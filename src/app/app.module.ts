import { BrowserModule } from '@angular/platform-browser';
import  { HttpClientModule  } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {  LoginModule } from './login/login.module'
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';


import { APP_ROUTES } from './app.routes';
import { ComponentsModule } from './components/components.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PageModule } from './pages/page.module';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IntegradosService } from './services/servicios/integrados.service';
import { USGService } from './services/usg/usg.service';

//DorpZone :D
import { NgxDropzoneModule } from 'ngx-dropzone';

import { NgStepperComponent } from 'angular-ng-stepper';
import { BuscarPacientePipe } from '../app/pipes/paciente/buscar-paciente.pipe';
import { ModalModule } from 'ng-modal-lib';
import { MatStepperModule } from '@angular/material/stepper';
import { FilterPipe } from './pipes/search/filter.pipe';

/* import { BucarPacienteLabPipe } from './pipes/buscarPacienteLab/bucar-paciente-lab.pipe'; */
// import { GenerosPipe } from './pipes/generos.pipe';




@NgModule({
  declarations: [
    AppComponent,
    BuscarPacientePipe,
    FilterPipe,
    // GenerosPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    PageModule,
    ComponentsModule,
    ReactiveFormsModule,
    LoginModule,
    NgxPaginationModule,
    ChartsModule,
    BrowserAnimationsModule,
    APP_ROUTES,
    ComponentsModule,
    NgxDropzoneModule,
    // ModalModule
  ],
  exports: [
    ReactiveFormsModule,
    NgStepperComponent,
    
  ],
  providers: [
    IntegradosService,
    ReactiveFormsModule,
    USGService,
    MatStepperModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
