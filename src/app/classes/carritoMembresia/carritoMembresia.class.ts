import StorageCarritoMembresia from './storageCarritoMembresia.class';
import CarritoMembresiaStorage from '../../interfaces/carrito.interface';

export default class CarritoMembresia {

    public carritoMembresia = {
        items:[],
        total:0
    };
    
    constructor() {
        this.getCarritoStorage();
    }


    getCarritoStorage(){
        const carritoStorage = new StorageCarritoMembresia();
       this.carritoMembresia = carritoStorage.obtenerCarritoMembresia();
       
       if( this.carritoMembresia == null ){

        this.carritoMembresia = {
            items:[],
            total:0
        };
       }
       return this.carritoMembresia;
    }

    guardarCarritoStorage (  carrito ){
        const carritoClass = new StorageCarritoMembresia();
         carritoClass.guardarCarritoStorage( carrito  );
    }

    eliminarCarritoStorage(){
        const carritoStorage = new StorageCarritoMembresia();
        const resultCarrito = carritoStorage.eliminarStorageCarritoMembresia();
        return resultCarrito;
    }

    agregarItem(ev, item){
    
        const estudio = {
            nombreEstudio: item.DESTINO,
            precio:"",
            name: item.name,
            idEstudio: item._id,
          }

          // validamos que sea un estudio o uun destino de la ambulancia

        if(  item.DESTINO == null || item.DESTINO == ""  ){
            estudio.nombreEstudio = item.ESTUDIO;
        }else{
            estudio.nombreEstudio = item.DESTINO;
        }

        if(  ev.path[1].classList.contains('precioMembresia') ){

            
            if(  item.PRECIO_MEMBRESIA_DIA === undefined ){
                estudio.precio = item.PRECIO_MEMBRESIA
            }else {
                estudio.precio = item.PRECIO_MEMBRESIA_DIA;
                
            }


        }else if(  ev.path[1].classList.contains('precioMembresiaRadondoDia') ){

            estudio.precio = item.PRECIO_MEMBRESIA_REDONDO_DIA;

        }else if( ev.path[1].classList.contains('precioMembresiaNoche') ){
            estudio.precio = item.PRECIO_MEMBRESIA_NOCHE;
        }else if( ev.path[1].classList.contains('precioMembresiaRedondoNoche') ){
            estudio.precio = item.PRECIO_MEMBRESIA_REDONDO_NOCHE;
        }else if( ev.path[1].classList.contains('precioMembresiaUrgencia') ){
            estudio.precio = item.PRECIO_MEMBRESIA_URGENCIA;
        }else if( ev.path[1].classList.contains('precioMembresiaHospitalizacion') ){
            estudio.precio = item.PRECIO_MEMBRESIA_HOSPITALIZACION;
        }else if( ev.path[1].classList.contains('precioMembresiaHospitalizacionUrgencia') ){
            estudio.precio = item.PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA;
        }
               
        this.sumarTotal(  estudio.precio );
        console.log( estudio );
        this.carritoMembresia.items.push( estudio );
        this.guardarCarritoStorage( this.carritoMembresia  );
    }


    sumarTotal(  precio ){


        let precioReplace = precio.replace('$', '');
        let precioSinComa  = precioReplace.replace(',', '');
        let precioNumber = parseFloat( precioSinComa );
        this.carritoMembresia.total = this.carritoMembresia.total + precioNumber;


    }

    eliminarItem( estudio ){

        this.carritoMembresia.items.forEach(  (item, index) => {
            // Agregar algun otro caso que se pueda dar

            if ( item.idEstudio  === estudio.idEstudio && item.precio === estudio.precio ) {
                this.carritoMembresia.items.splice( index, 1 )
                this.restarTotal(  item.precio );
            }

          });


             this.guardarCarritoStorage (  this.carritoMembresia );
    }

    restarTotal( precio  ){

        
        let precioTrim  =  precio.replace('$', '');
        let precioSinComa = precioTrim.replace(',', '');
        // aca le quito la coma si es que trae
        let precioMembresiaNumber = parseFloat( precioSinComa );
        this.carritoMembresia.total =  this.carritoMembresia.total - precioMembresiaNumber;
         return this.carritoMembresia;

    }


}