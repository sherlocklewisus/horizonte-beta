export interface FichaInfo {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:String;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;

} 

export interface FichaInfoCensur {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:String;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;
    receptor: String;
    tipoDeSangre: String;
    disponente: String;
    religion:String;
    fechaRegistro:Date,
    consultas: String,
}

export interface FichaInfoCensurDoctor {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:String;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;
    receptor: String;
    tipoDeSangre: String;
    disponente: String;
    religion: String;
}
export interface FichaInfoCensurLab {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:String;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;
    receptor: String;
    tipoDeSangre: String;
    disponente: String;
    religion: String;
}

export interface FichaInfoReaccionesDerivadas {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:String;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;
    receptor: String;
    disponente: String;
    religion: String;
}

export interface LaboratorioDisp {
    fecha: ""
    laboratorios: ""
    obtenidos: []
    pedido: []
    proceso: ""
    resultados: {},
    _id: ""
}

