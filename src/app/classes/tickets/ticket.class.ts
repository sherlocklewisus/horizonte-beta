import { numeroALetras  } from '../../functions/generalFunctions';
import jsPDF from 'jspdf';
import { getDataStorage, getrolefunction } from 'src/app/functions/storage.funcion';



export default class Tickets {
    public IVaCDEl16=0;
    constructor() {       
    }
    public IVaDEl16 = 0;
    public positionYPDF = 55;
    public totalLetra = "";
    public atendio = "";

    calcularIva( carrito, membresia  ){
      if(membresia){
        this.IVaDEl16 = ((carrito.totalCon  * 16 ) / 100);
      }else {
        this.IVaDEl16 = ((carrito.totalSin  * 16 ) / 100);
      }
    }

    calcularIvaPedido( pedido){
      this.IVaDEl16 = ((pedido.totalCompra  * 16 ) / 100);
    }

    printTicket( paciente, pedido:any, infoVenta:any, folio ){
      let restanteCredito= 0;
      let restanteDebito= 0;
      let x=0;
      let y=0;

      this.atendio = getDataStorage().nombre;
  
      const doc: any = new jsPDF();
  
      doc.setFontSize(8);
      // REvisar estas funciones
      doc.text( 2 , 15,`No ticke: ${folio}         Fecha: ${infoVenta.fecha}`);
      doc.text(2, 20,`RFC: HGS111116J76                  Teléfono: 735-35-77-564`);
      //  doc.text(2, 25,`Atendió: ${infoVenta.vendedor} `);
      doc.text( 20, 25, `Gracias ${ paciente.nombrePaciente } ${paciente.apellidoPaterno} ${paciente.apellidoMaterno}` );
      doc.text(2, 30, `------------------------------------------------------------------------------------------------------`)
      doc.text(2,35,`           Estudio                                   Costo                `);
      doc.text(2, 40, `------------------------------------------------------------------------------------------------------`)
    
      pedido.estudios.forEach(  item => {
        if( paciente.membresiaActiva ){
          doc.text(3, this.positionYPDF,`${item.nombreEstudio}`);
          doc.text(5,  this.positionYPDF+5,`$ ${item.precioCon}.00 MX` );
          doc.text(3, this.positionYPDF += 15, ``)
        }else {
          doc.text(3, this.positionYPDF,`${item.nombreEstudio}`);
          doc.text(5,  this.positionYPDF+5,`$ ${item.precioSin}.00 MX` );
          doc.text(3, this.positionYPDF += 15, ``)
          this.positionYPDF += 5;
        }
      });

      this.calcularIvaPedido(pedido);
      doc.text(15, this.positionYPDF+=10, `Anticipo:                               $ ${pedido.montoAnticipo}.00 MX` );
      doc.text(15, this.positionYPDF+=10, `El total sin I.V.A es:              $ ${infoVenta.totalCompra - pedido.montoAnticipo - this.IVaDEl16} MX` );
      doc.text( 15, this.positionYPDF+=5,  `El I.V.A es de:                       $ ${this.IVaDEl16} MX`  );
      /* if(infoVenta.efectivo){
        infoVenta.totalCompra = infoVenta.totalCompra - infoVenta.mon;
     }
     if(infoVenta.transferencia){
        infoVenta.totalCompra = infoVenta.totalCompra - infoVenta.montoTranferencia;
     } */
      if(infoVenta.tarjetCredito){
        y=Math.round((infoVenta.totalCompra * 1.9)/100)
        restanteCredito =(infoVenta.totalCompra+(infoVenta.totalCompra * 1.9)/100)-infoVenta.montoTarjteCredito
        doc.text( 15, this.positionYPDF+=5,  `Iva tarjeta de credito:              $ ${Math.round((infoVenta.totalCompra * 1.9)/100)}.00 MX`  );
      }else{
        restanteCredito = infoVenta.totalCompra
      }
      if(infoVenta.tarjetaDebito){
        x=Math.round((restanteCredito * 2.5)/100)
        restanteDebito=Math.round(((restanteCredito * 2.5)/100)-infoVenta.montoTarjetaDebito)
        doc.text( 15, this.positionYPDF+=5,  `Iva tarjeta de credito:              $ ${Math.round((restanteCredito * 2.5)/100)}.00 MX`  );
      }else{
        restanteDebito=infoVenta.totalCompra
      }
      this.totalLetra = numeroALetras( infoVenta.totalCompra+x+y-pedido.montoAnticipo, 'Pesos mexicanos' );
      doc.text( 15, this.positionYPDF+=5,  `El Total con I.V.A:                  $ ${infoVenta.totalCompra+x+y-pedido.montoAnticipo} MX`  );
      doc.text(5, this.positionYPDF+=6, `Total con letra : `);
      doc.text(2, this.positionYPDF+=5, `${this.totalLetra}` );
      doc.text(25, this.positionYPDF+=10,  `Atendío: ${this.atendio}`);
      doc.text( 25, this.positionYPDF+=9, "Gracias por su Compra");
      doc.text( 8, this.positionYPDF +=5, "Recuerda usar cubrebocas   #QuédateEnCasa ");
      this.positionYPDF +=15;
      doc.save('TICKET');
      this.positionYPDF = 100;
    }

    printTicketSinMembresia( paciente, carrito:any, infoVenta:any, folio:any ){
        let restanteCredito= 0;
        let restanteDebito= 0;
        let x=0;
        let y=0;
        
        this.atendio = getDataStorage().nombre;
        
        const doc: any = new jsPDF();
        doc.setFontSize(8);
        // REvisar estas funciones
        doc.text( 2 , 15,`No ticket: ${folio}         Fecha: ${infoVenta.fecha}`);
        doc.text(2, 20,`RFC: HGS111116J76                  Teléfono: 735-35-77-564`);
        //  doc.text(2, 25,`Atendió: ${infoVenta.vendedor} `);
        doc.text( 20, 25, `Gracias ${ paciente.nombrePaciente } ${paciente.apellidoPaterno} ${paciente.apellidoMaterno}` );
        doc.text(2, 30, `------------------------------------------------------------------------------------------------------`)
        doc.text(2,35,`           Estudio                                   Costo                `);
        doc.text(2, 40, `------------------------------------------------------------------------------------------------------`)
      
        carrito['items'].forEach(  item => {
          if( paciente.membresiaActiva ){
            doc.text(3, this.positionYPDF,`${item.nombreEstudio}`);
            doc.text(5,  this.positionYPDF+5,`$ ${item.precioCon}.00 MX` );
            doc.text(3, this.positionYPDF += 15, ``)
          }else {
            doc.text(3, this.positionYPDF,`${item.nombreEstudio}`);
            doc.text(5,  this.positionYPDF+5,`$ ${item.precioSin}.00 MX` );
            doc.text(3, this.positionYPDF += 15, ``)
            this.positionYPDF += 5;
          }
        });

        this.calcularIva(  carrito, paciente.membresiaActiva );
     
        if( paciente.membresiaActiva ){
          doc.text(15, this.positionYPDF+=10, `El total sin I.V.A es:               $ ${carrito.totalCon - this.IVaDEl16} MX` );

        }else {
          doc.text(15, this.positionYPDF+=10, `El total sin I.V.A es:                 $ ${carrito.totalSin - this.IVaDEl16} MX` );

        }
  
        doc.text( 15, this.positionYPDF+=5,  `El I.V.A es de:                       $ ${this.IVaDEl16} MX`  );

        if(infoVenta.efectivo){
            infoVenta.totalCompra = infoVenta.totalCompra - infoVenta.montoEfectivo;
        }
        if(infoVenta.transferencia){
            infoVenta.totalCompra = infoVenta.totalCompra - infoVenta.montoTranferencia;
        }
        if(infoVenta.tarjetCredito){
          y=Math.round((infoVenta.totalCompra * 1.9)/100)
          restanteCredito =(infoVenta.totalCompra+(infoVenta.totalCompra * 1.9)/100)-infoVenta.montoTarjteCredito
          doc.text( 15, this.positionYPDF+=5,  `Iva tarjeta de credito:              $ ${Math.round((infoVenta.totalCompra * 1.9)/100)}.00 MX`  );
        }else{
          restanteCredito = infoVenta.totalCompra
        }
        if(infoVenta.tarjetaDebito){
          x=Math.round((restanteCredito * 2.5)/100)
          restanteDebito=Math.round(((restanteCredito * 2.5)/100)-infoVenta.montoTarjetaDebito)
          doc.text( 15, this.positionYPDF+=5,  `Iva tarjeta de credito:              $ ${Math.round((restanteCredito * 2.5)/100)}.00 MX`  );
        }else{
          restanteDebito=infoVenta.totalCompra
        }
  
        if( paciente.membresiaActiva  ){
          doc.text( 15, this.positionYPDF+=5,  `El total con I.V.A:                  $ ${carrito.totalCon+x+y}.00 MX`  );
        }else {
          doc.text( 15, this.positionYPDF+=5,  `El total con I.V.A:                  $ ${carrito.totalSin+x+y}.00 MX`  );
        }
        if( paciente.membresiaActiva ){
          this.totalLetra = numeroALetras( carrito.totalCon+x+y, 'Pesos mexicanos' );
        }else{
          this.totalLetra = numeroALetras( carrito.totalSin+x+y, 'Pesos mexicanos' );
        }
        doc.text(5, this.positionYPDF+=6, `Total con letra : `);
        doc.text(2, this.positionYPDF+=5, `${this.totalLetra}` );
        doc.text(25, this.positionYPDF+=10,  `Atendío: ${this.atendio}`);
        doc.text( 25, this.positionYPDF+=9, "Gracias por su Compra");
        doc.text( 8, this.positionYPDF +=5, "Recuerda usar cubrebocas   #QuédateEnCasa ");
        this.positionYPDF +=15;
        doc.save('TICKET');
        this.positionYPDF = 100;
    }

    calcularIvaCotizacion( pedido){
      this.IVaCDEl16 = ((pedido  * 16 ) / 100);
    }
    // sin membresia 
    imprimirCotizacion( carrito ){

      

      const totalLetra = numeroALetras( carrito.totalSin, 'Pesos mexicanos' );
    
      const doc: any = new jsPDF('p', 'mm', [ 300, 500]);
      doc.setFontSize(8);
        // REvisar estas funciones
        doc.text(2, 20,`RFC: HGS111116J76                  Teléfono: 735-35-77-564`);
        //  doc.text(2, 25,`Atendió: ${infoVenta.vendedor} `);
        doc.text( 20, 25, `Cotización horizonte` );
        doc.text(2, 30, `------------------------------------------------------------------------------------------------------`)
        doc.text(2,35,`           Estudio                                   Costo                `);
        doc.text(2, 40, `------------------------------------------------------------------------------------------------------`)
      
        this.calcularIvaCotizacion(carrito.totalSin);
    
      //  doc.addImage( ImagenTicketHorizonter, 'JPEG', 30, 5, 70, 50 );
      doc.setFontSize(10);
      carrito.items.forEach(  item => {
        doc.text(3, this.positionYPDF,`${item.nombreEstudio}`);
        doc.text(5,  this.positionYPDF+5,`$ ${item.precioSin}.00 MX` );
        doc.text(3, this.positionYPDF += 15, ``)
        this.positionYPDF += 5;
      });
      // this.calcularIva();
      doc.text(15, this.positionYPDF+=10, `El total sin I.V.A es:               $ ${carrito.totalSin - this.IVaCDEl16}.00 MX` );
      doc.text( 15, this.positionYPDF+=5,  `El I.V.A es de:                       $ ${this.IVaCDEl16}.00 MX`  );
      doc.text( 15, this.positionYPDF+=5,  `El total con I.V.A:                   $ ${carrito.totalSin}.00 MX`  );
      doc.text(5, this.positionYPDF+=6, `Total con letra : `);
      doc.text(2, this.positionYPDF+=5, `${totalLetra}` );
      doc.text(50, this.positionYPDF+=10,  `Atendío: ${this.atendio}`);
      doc.text(50, this.positionYPDF+=12, "Gracias por su Compra");
      doc.save('COTIZACION');
      this.positionYPDF = 100;
    }
}