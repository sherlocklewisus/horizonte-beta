import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiometriaLabDisponenteComponent } from './biometria-lab-disponente.component';

describe('BiometriaLabDisponenteComponent', () => {
  let component: BiometriaLabDisponenteComponent;
  let fixture: ComponentFixture<BiometriaLabDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiometriaLabDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiometriaLabDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
