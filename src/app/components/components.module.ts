import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';

import { CardComponent } from './card/card.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChatComponent } from './chat/chat/chat.component';
import { FormularioServiciosComponent } from './servicios/formulario-servicios/formulario-servicios.component';
import { FormularioAmbulanciaComponent } from './servicios/formulario-ambulancia/formulario-ambulancia.component';
import { EndoscopiaComponent } from './servicios/serv/endoscopia/endoscopia.component';
import { AmbulanciaComponent } from './servicios/serv/ambulancia/ambulancia.component';
import { PatologiaXComponent } from './servicios/serv/patologia-x/patologia-x.component';
import { LabUltaTomoResComponent } from './servicios/serv/lab-ulta-tomo-res/lab-ulta-tomo-res.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatStepperModule, MatStepper } from '@angular/material/stepper';
import { StepperComponent } from './registro/stepper/stepper.component';
import { TablasConsultasComponent } from './tablas/tablas-consultas/tablas-consultas.component';
import { TabHisroiaClinicaComponent } from './tabs/tab-hisroia-clinica/tab-hisroia-clinica.component';
import { FichaIdentificacionComponent } from './ficha-identificacion/ficha-identificacion.component';

import { HeaderRecetaComponent } from './header-receta/header-receta.component';
import { GraficasPediatriaComponent } from './graficas-pediatria/graficas-pediatria.component';
import { GraficasNinosComponent } from './graficas-ninos/graficas-ninos.component';
import { HistoriaClinicaComponent } from './hisotriaClinica/historia-clinica/historia-clinica.component';
import { PaquetesComponent } from './paquetes/paquetes.component';
import { RiesgoDeCaidaComponent } from './ficha-identificacion/riesgoCaida/riesgo-de-caida/riesgo-de-caida.component';
import { PrenatalComponent } from './paquetes/paciente-paquete/prenatal/prenatal.component';
import { NeonatalComponent } from './paquetes/paciente-paquete/neonatal/neonatal.component';
import { PediatricoComponent } from './paquetes/paciente-paquete/pediatrico/pediatrico.component';
import { MembresiaComponent } from './paquetes/paciente-paquete/membresia/membresia.component';
import { MedicoLaboralComponent } from './paquetes/paciente-paquete/medico-laboral/medico-laboral.component';
import { PrenatalRiesgoComponent } from './paquetes/paciente-paquete/prenatal-riesgo/prenatal-riesgo.component';
import { VidaPlenaComponent } from './paquetes/paciente-paquete/vida-plena/vida-plena.component';
import { FichaInfoUserComponent } from './ficha-identificacion/ficha-info-user/ficha-info-user.component';
import { GraficasSvComponent } from './graficas-sv/graficas-sv.component';
import { PagoServiciosConComponent } from './pagos/pagosCon/pago-servicios-con/pago-servicios-con.component';

import { NavBarComponent } from './nav-bar/nav-bar.component';
import { TablaServiceComponent } from './servicios/serv/tabla-service/tabla-service.component';
import { StepperSocioComponent } from './registro-socio/stepper-socio/stepper-socio.component';
import { StepperSocioEditarComponent } from './registro-socio/stepper-socio-editar/stepper-socio-editar.component';
import { FichaInfoSocioComponent } from './ficha-identificacion/ficha-info-socio/ficha-info-socio.component';
import { UtilidadSocioServiciosComponent } from './utilidad-socio/utilidad-socio-servicios/utilidad-socio-servicios.component';
import { UtilidadSocioAmbulanciaComponent } from './utilidad-socio/utilidad-socio-ambulancia/utilidad-socio-ambulancia.component';
import { StepperSocioEditarAmbulanciaComponent } from './registro-socio/stepper-socio-editar-ambulancia/stepper-socio-editar-ambulancia.component';
import { PagosPaquetesComponent } from './paquetes/pagos-paquetes/pagos-paquetes.component';
import { PagosCensurComponent } from './pagos/pagos-censur/pagos-censur.component';
/* import { FichaInfoCensurComponent } from './ficha-identificacion/ficha-info-censur/ficha-info-censur.component'; */
import { RegistrarDisponenteComponent } from './registrar-disponente/registrar-disponente.component';
/* import { FichaEnfermeriaDisponenteComponent } from './ficha-enfermeria-disponente/ficha-enfermeria-disponente.component'; */
/* import { FichaInfoFlebotomiaComponent } from './ficha-identificacion/ficha-info-flebotomia/ficha-info-flebotomia.component'; */
/* import { FichaDoctorComponent } from './ficha-doctor/ficha-doctor.component'; */
import { IndicadoresGeograficosComponent } from './historia-clinica-disp/indicadores-geograficos/indicadores-geograficos.component';
import { AntecedentesComponent } from './historia-clinica-disp/antecedentes/antecedentes.component';
import { AntecedentesPersonalesComponent } from './historia-clinica-disp/antecedentes-personales/antecedentes-personales.component';
import { AntecedentesPatologicosComponent } from './historia-clinica-disp/antecedentes-patologicos/antecedentes-patologicos.component';
import { AntecedentesGinecoComponent } from './historia-clinica-disp/antecedentes-gineco/antecedentes-gineco.component';
import { PracticasRiesgoComponent } from './historia-clinica-disp/practicas-riesgo/practicas-riesgo.component';
import { ParejasSexualesComponent } from './historia-clinica-disp/parejas-sexuales/parejas-sexuales.component';
import { UltimoAnioComponent } from './historia-clinica-disp/ultimo-anio/ultimo-anio.component';
import { MedicamentosComponent } from './historia-clinica-disp/medicamentos/medicamentos.component';
import { ExploracionFisicaComponent } from './historia-clinica-disp/exploracion-fisica/exploracion-fisica.component';
import { DiagnosticoDisponenteComponent } from './historia-clinica-disp/diagnostico-disponente/diagnostico-disponente.component';
import { FichaReaccionesDerivadasComponent } from './ficha-reacciones-derivadas/ficha-reacciones-derivadas.component';
import { BiometriaLabDisponenteComponent } from './biometria-lab-disponente/biometria-lab-disponente.component';
import { ReaccionVasovagalComponent } from './reacciones-derivadas-disp/reaccion-vasovagal/reaccion-vasovagal.component';
import { ReaccionVenopuncionComponent } from './reacciones-derivadas-disp/reaccion-venopuncion/reaccion-venopuncion.component';
import { ReaccionToxicidadComponent } from './reacciones-derivadas-disp/reaccion-toxicidad/reaccion-toxicidad.component';
import { ReaccionAdversaComponent } from './reacciones-derivadas-disp/reaccion-adversa/reaccion-adversa.component';
/* import { FichaLaboratorioDisponenteComponent } from './ficha-laboratorio-disponente/ficha-laboratorio-disponente.component'; */
import { FichaIndentificaciosproductosAlmacenComponent } from './ficha-identificacion/almacen/ficha-indentificaciosproductos-almacen/ficha-indentificaciosproductos-almacen.component';
import { LaboratorioDispComponent } from './historia-clinica-disp/laboratorio-disp/laboratorio-disp.component';
import { SppinerComponent } from './ui/sppiner/sppiner.component';
import { FichasIdentificacionComponent } from './ficha-identificacion/ficha-info-bancoSangre/fichas-identificacion/fichas-identificacion.component';
import { TitulosComponent } from './ficha-identificacion/titulos/titulos.component';
// import { FormularioCreadorComponent } from './servicios/creador-servicios/formulario-creador/formulario-creador.component';



@NgModule({
  declarations: [
    DashboardComponent,
    CardComponent,
    NavBarComponent,
    ChatComponent,
    FormularioServiciosComponent,
    FormularioAmbulanciaComponent,
    EndoscopiaComponent,
    AmbulanciaComponent,
    PatologiaXComponent,
    LabUltaTomoResComponent,
    StepperComponent,
    TablasConsultasComponent,
    TabHisroiaClinicaComponent,
    FichaIdentificacionComponent,
    GraficasPediatriaComponent,
    GraficasNinosComponent,
    HeaderRecetaComponent,
    HistoriaClinicaComponent,
    PaquetesComponent,
    RiesgoDeCaidaComponent,
    PrenatalComponent,
    NeonatalComponent,
    PediatricoComponent,
    MembresiaComponent,
    MedicoLaboralComponent,
    PrenatalRiesgoComponent,
    VidaPlenaComponent,
    FichaInfoUserComponent,
    GraficasSvComponent,
    LabUltaTomoResComponent,
    PagoServiciosConComponent,
    // FormularioCreadorComponent,
    TablaServiceComponent,
    StepperSocioComponent,
    StepperSocioEditarComponent,
    FichaInfoSocioComponent,
    UtilidadSocioServiciosComponent,
    UtilidadSocioAmbulanciaComponent,
    StepperSocioEditarAmbulanciaComponent,
    PagosPaquetesComponent,
    PagosCensurComponent,
    RegistrarDisponenteComponent,
    IndicadoresGeograficosComponent,
    AntecedentesComponent,
    AntecedentesPersonalesComponent,
    AntecedentesPatologicosComponent,
    AntecedentesGinecoComponent,
    PracticasRiesgoComponent,
    ParejasSexualesComponent,
    UltimoAnioComponent,
    MedicamentosComponent,
    ExploracionFisicaComponent,
    DiagnosticoDisponenteComponent,
    FichaReaccionesDerivadasComponent,
    BiometriaLabDisponenteComponent,
    ReaccionVasovagalComponent,
    ReaccionVenopuncionComponent,
    ReaccionToxicidadComponent,
    ReaccionAdversaComponent,
    FichaIndentificaciosproductosAlmacenComponent,
    LaboratorioDispComponent,
    SppinerComponent,
    FichasIdentificacionComponent,
    TitulosComponent
    // PagoServiciosSinComponent,

  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA
   ],
  exports: [
    CardComponent,
    NavBarComponent,
    ChatComponent,
    FormularioServiciosComponent,
    FormularioAmbulanciaComponent,
    EndoscopiaComponent,
    AmbulanciaComponent,
    PatologiaXComponent,
    LabUltaTomoResComponent,
    ReactiveFormsModule,
    StepperComponent,
    TablasConsultasComponent,
    TabHisroiaClinicaComponent,
    FichaIdentificacionComponent,
    GraficasNinosComponent,
    GraficasPediatriaComponent,
    GraficasSvComponent,
    HeaderRecetaComponent,
    HistoriaClinicaComponent,
    PaquetesComponent,
    RiesgoDeCaidaComponent,
    PrenatalComponent,
    NeonatalComponent,
    PediatricoComponent,
    MembresiaComponent,
    MedicoLaboralComponent,
    PrenatalRiesgoComponent,
    VidaPlenaComponent,
    FichaIdentificacionComponent,
    AmbulanciaComponent,
    EndoscopiaComponent,
    PagoServiciosConComponent,
    FichaInfoUserComponent,
    TablaServiceComponent,
    StepperSocioComponent,
    StepperSocioEditarComponent,
    FichaInfoSocioComponent,
    UtilidadSocioServiciosComponent,
    UtilidadSocioAmbulanciaComponent,
    StepperSocioEditarAmbulanciaComponent,
    PagosPaquetesComponent,
    PagosCensurComponent,
    RegistrarDisponenteComponent,
    IndicadoresGeograficosComponent,
    AntecedentesComponent,
    AntecedentesPersonalesComponent,
    AntecedentesPatologicosComponent,
    AntecedentesGinecoComponent,
    PracticasRiesgoComponent,
    ParejasSexualesComponent,
    UltimoAnioComponent,
    MedicamentosComponent,
    ExploracionFisicaComponent,
    DiagnosticoDisponenteComponent,
    FichaReaccionesDerivadasComponent,
    BiometriaLabDisponenteComponent,
    ReaccionVasovagalComponent,
    ReaccionVenopuncionComponent,
    ReaccionToxicidadComponent,
    ReaccionAdversaComponent,
    FichaIndentificaciosproductosAlmacenComponent,
    LaboratorioDispComponent,
    SppinerComponent,
    FichasIdentificacionComponent,
    TitulosComponent
    // FormularioCreadorComponent


  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    MatStepperModule,
    ChartsModule,
    // HeaderRecetaComponent
  ],
  providers:[]
})
export class ComponentsModule { }
