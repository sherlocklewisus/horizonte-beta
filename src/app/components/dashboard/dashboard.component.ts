import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.eliminarCarrito();
    
    this.eliminarSede();
    this.borrarLocal();
  }

  eliminarCarrito(){
    localStorage.removeItem('carrito');
  }

  eliminarSede(){
    localStorage.removeItem('sede');
  }

  

  borrarLocal(){
    localStorage.removeItem('sedePaciente')
    localStorage.removeItem('carrito')
    localStorage.removeItem('idPedidoSede')
    localStorage.removeItem('idPedidoXray')
    localStorage.removeItem('idPedidoUltra')
    localStorage.removeItem('pedidoUsg')
    localStorage.removeItem('ultrasonido')
    localStorage.removeItem('fechaUsg')
    localStorage.removeItem('fechaXray')
    localStorage.removeItem('xray')
    localStorage.removeItem('receptor')
    localStorage.removeItem('censur-carrito')
    localStorage.removeItem('idCensur')
  }

}
