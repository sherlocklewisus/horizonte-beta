import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaIndentificaciosproductosAlmacenComponent } from './ficha-indentificaciosproductos-almacen.component';

describe('FichaIndentificaciosproductosAlmacenComponent', () => {
  let component: FichaIndentificaciosproductosAlmacenComponent;
  let fixture: ComponentFixture<FichaIndentificaciosproductosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichaIndentificaciosproductosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaIndentificaciosproductosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
