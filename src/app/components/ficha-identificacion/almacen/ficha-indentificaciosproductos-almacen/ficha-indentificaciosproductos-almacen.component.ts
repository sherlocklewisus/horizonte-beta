import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ficha-indentificaciosproductos-almacen',
  templateUrl: './ficha-indentificaciosproductos-almacen.component.html',
  styleUrls: ['./ficha-indentificaciosproductos-almacen.component.css']
})
export class FichaIndentificaciosproductosAlmacenComponent implements OnInit {

  @Input('dataFiProducto') dataFiProducto = {
    nombre:'',
    costo:"",
    estado:"",
    descripcion:"",
    tipo_producto:"",
    idProducto:"",
    proveedor: []
  }
  constructor() { }

  ngOnInit(): void {
  }

}
