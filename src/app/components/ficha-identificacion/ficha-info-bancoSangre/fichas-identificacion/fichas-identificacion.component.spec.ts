import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichasIdentificacionComponent } from './fichas-identificacion.component';

describe('FichasIdentificacionComponent', () => {
  let component: FichasIdentificacionComponent;
  let fixture: ComponentFixture<FichasIdentificacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichasIdentificacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichasIdentificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
