import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ficha-info-flebotomia',
  templateUrl: './ficha-info-flebotomia.component.html',
  styleUrls: ['./ficha-info-flebotomia.component.css']
})
export class FichaInfoFlebotomiaComponent implements OnInit {

  @Input() pacienteFI = {
    nombrePaciente:'',
    apellidoPaterno: '',
    apellidoMaterno:'',
    edad:0,
    genero:"",
    telefono:"",
    fechaNacimientoPaciente:"",
    curp:"",
    _id:""
  }


  @Output() dataDonanteEmit = new EventEmitter();
  
  dataDonante = {

    tipo_de_sangreado:"",
    tipo_de_donador:"",
    idbancosangre:"",
    notas:""
  
  }

  constructor() { }

  ngOnInit(): void {
  }

  emmitDataDocumentos() {

    if( this.dataDonante.tipo_de_sangreado.length == 0 ){

        alert("Ingresa el tipo de sangrado");

    }else if( this.dataDonante.tipo_de_donador.length == 0 ){
      // console.log( this.dataDonante );
      alert("Ingresa el tipo de donador");

    }
    else{
      this.dataDonanteEmit.emit( this.dataDonante );

    }
  }

}
