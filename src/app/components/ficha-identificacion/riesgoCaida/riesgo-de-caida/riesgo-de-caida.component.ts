import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import swal from 'sweetalert/dist/sweetalert.min.js';

@Component({
  selector: 'app-riesgo-de-caida',
  templateUrl: './riesgo-de-caida.component.html',
  styleUrls: ['./riesgo-de-caida.component.css']
})
export class RiesgoDeCaidaComponent implements OnInit {

  @Output() totalRiesgoCaida = new EventEmitter<number>();

  constructor() { }

  public riesgo={
    valores:""
  }
  public rango={
    uno:"",
    dos:"",
    tres:"",
    cuatro:"",
    cinco:"",
    seis:"",
    siete:"",
    ocho:"",
    nueve:"",
    diez:"",
    once:"",
    doce:""
  }

  public suma=0;

  public evaluacion = false

  ngOnInit(){
  }

  habilitarRiesgo(){
    if (this.riesgo.valores=="si"){
          this.evaluacion=true;
    }
  }

  addRiesgoCaida(){
    this.totalRiesgoCaida.emit( this.suma );
  }

  monstrarRangoRiesgo(){

    if(this.rango.uno=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.dos=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.tres=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.cuatro=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.cinco=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.seis=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.siete=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.ocho=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.nueve=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.diez=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.once=="si"){
      this.suma= this.suma +1;
    }
    if(this.rango.doce=="si"){
      this.suma= this.suma +1;
    }

if(this.suma==0){ 
  
  swal ( 
          "SIN PROBLEMAS",
           "NO EXISTE RIESGO DE CAIDA", 
           "success",{ button:"Entendido"}
            );

            this.addRiesgoCaida();

            this.limpiarRadios();

    this.evaluacion= false;
    //this.riesgo.valores=""
    this.suma=0

          }
if(this.suma==1 || this.suma==2){
  swal ( 
      "ADVERTENCIA",
       "POSIBLE RIESGO DE CAIDA, SE ENCUENTRA EN UN RANGO DE 1 Y 2", 
       "warning",{ button:"Entendido"}
        );
        this.addRiesgoCaida();
    this.limpiarRadios()
    this.evaluacion= false;
    //this.riesgo.valores=""
    this.suma=0
    
}
if(this.suma >2){
   swal ( 
           "PELIGRO",
           "RIESGO DE CAIDA CON RANGO MAYOR A 2", 
            "error",{ button:"Entendido"}
         );
         this.addRiesgoCaida();
         this.limpiarRadios()
    this.evaluacion= false;
    //this.riesgo.valores=""
 this.suma=0
                  
}
    
 
} // fin metodo

   limpiarRadios(){
     this.rango.uno="",
     this.rango.dos="",
     this.rango.tres="",
     this.rango.cuatro="",
     this.rango.cinco="",
     this.rango.seis="",
     this.rango.siete="",
     this.rango.ocho="",
     this.rango.nueve="",
     this.rango.diez="",
     this.rango.once="",
     this.rango.doce=""
   }


}
