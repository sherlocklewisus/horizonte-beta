import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaReaccionesDerivadasComponent } from './ficha-reacciones-derivadas.component';

describe('FichaReaccionesDerivadasComponent', () => {
  let component: FichaReaccionesDerivadasComponent;
  let fixture: ComponentFixture<FichaReaccionesDerivadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichaReaccionesDerivadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaReaccionesDerivadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
