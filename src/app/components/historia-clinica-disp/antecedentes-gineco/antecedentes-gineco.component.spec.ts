import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesGinecoComponent } from './antecedentes-gineco.component';

describe('AntecedentesGinecoComponent', () => {
  let component: AntecedentesGinecoComponent;
  let fixture: ComponentFixture<AntecedentesGinecoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecedentesGinecoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesGinecoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
