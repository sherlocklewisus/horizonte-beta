import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosticoDisponenteComponent } from './diagnostico-disponente.component';

describe('DiagnosticoDisponenteComponent', () => {
  let component: DiagnosticoDisponenteComponent;
  let fixture: ComponentFixture<DiagnosticoDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiagnosticoDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosticoDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
