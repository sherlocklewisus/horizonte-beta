import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadoresGeograficosComponent } from './indicadores-geograficos.component';

describe('IndicadoresGeograficosComponent', () => {
  let component: IndicadoresGeograficosComponent;
  let fixture: ComponentFixture<IndicadoresGeograficosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndicadoresGeograficosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadoresGeograficosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
