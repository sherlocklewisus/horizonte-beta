import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaboratorioDispComponent } from './laboratorio-disp.component';

describe('LaboratorioDispComponent', () => {
  let component: LaboratorioDispComponent;
  let fixture: ComponentFixture<LaboratorioDispComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaboratorioDispComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LaboratorioDispComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
