import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParejasSexualesComponent } from './parejas-sexuales.component';

describe('ParejasSexualesComponent', () => {
  let component: ParejasSexualesComponent;
  let fixture: ComponentFixture<ParejasSexualesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParejasSexualesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParejasSexualesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
