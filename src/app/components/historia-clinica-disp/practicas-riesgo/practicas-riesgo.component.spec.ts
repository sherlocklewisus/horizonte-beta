import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticasRiesgoComponent } from './practicas-riesgo.component';

describe('PracticasRiesgoComponent', () => {
  let component: PracticasRiesgoComponent;
  let fixture: ComponentFixture<PracticasRiesgoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PracticasRiesgoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticasRiesgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
