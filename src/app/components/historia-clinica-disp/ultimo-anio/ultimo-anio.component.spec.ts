import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimoAnioComponent } from './ultimo-anio.component';

describe('UltimoAnioComponent', () => {
  let component: UltimoAnioComponent;
  let fixture: ComponentFixture<UltimoAnioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UltimoAnioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimoAnioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
