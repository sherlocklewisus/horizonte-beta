import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosCensurComponent } from './pagos-censur.component';

describe('PagosCensurComponent', () => {
  let component: PagosCensurComponent;
  let fixture: ComponentFixture<PagosCensurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagosCensurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosCensurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
