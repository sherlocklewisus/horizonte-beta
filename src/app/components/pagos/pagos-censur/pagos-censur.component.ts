import { Component, OnInit } from '@angular/core';
import  CarritoCensur  from '../../../classes/carrito-censur/censur-carrito';

@Component({
  selector: 'app-pagos-censur',
  templateUrl: './pagos-censur.component.html',
  styleUrls: ['./pagos-censur.component.css']
})
export class PagosCensurComponent implements OnInit {

  public carrito = {
    total: 0,
    items: []
  };

  public paciente = {
    nombrePaciente:'',
    apellidoPaterno:'',
    apellidoMaterno: '',
    edad:0,
    genero:'',
    direccion:{
      callePaciente:""
    },
    id:'',
    membresiaActiva:false,
    _id:"",
  }

  constructor() { }

  ngOnInit(): void {
    this.obtenerCarritoStorage();
    this.obtenerReceptor();
  }

  obtenerCarritoStorage(){
    const storageCarrito = new CarritoCensur();
    this.carrito = storageCarrito.obtenerSotorageCarrito();
  }

  obtenerReceptor(){
    this.paciente = JSON.parse(localStorage.getItem('receptor'))
    console.log(this.paciente);
    
  }
}
