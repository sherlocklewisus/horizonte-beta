import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { EmptyError } from 'rxjs';
import Carrito from 'src/app/classes/carrito/carrito.class';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import Dates from 'src/app/classes/dates/date.class';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
import Tickets from 'src/app/classes/tickets/ticket.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import { eliminarTodoPedido, getDataStorage } from 'src/app/functions/storage.funcion';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import { USGService } from 'src/app/services/usg/usg.service';
import { XRAYService } from 'src/app/services/Rayos-X/xray.service';
import swal from 'sweetalert';
import { element } from 'protractor';



@Component({
  selector: 'pago-servicios-con',
  templateUrl: './pago-servicios-con.component.html',
  styleUrls: ['./pago-servicios-con.component.css']
})
export class PagoServiciosConComponent implements OnInit {

  public fecha = "";
  public paciente = {
    nombrePaciente:'',
    apellidoPaterno:'',
    apellidoMaterno: '',
    edad:0,
    genero:'',
    direccion:{
      callePaciente:""
    },
    id:'',
    membresiaActiva:false,
    _id:"",
  }
  public IVaDEl16 = 0;
  public positionYPDF = 100;
  public infoVenta = {  
    paciente:"",
    nombrePaciente:"",
    vendedor:"",
    fecha:"",
    hora:"",
    estudios:[],
    efectivo:false,
    doctorQueSolicito:"",
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    montoEfectivo:0,
    montoTarjteCredito:0,
    montoTarjetaDebito:0,
    montoTranferencia: 0,
    sede:"",
    totalCompra:0,
    prioridad:"",
    compraConMembresia:true
  }

  public btnValidacion=true;
  public totalSinMembresia = 0;
  public totalConMembresia = 0;

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };

  public totalConIva=0;
  public totalCarritoMembresia = 0;
  public pedidosLaboratorios = { 
    estudios:[],
    idPaciente:"",
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado",
    estado:""
  }

  public pedidosUltrasonido = {
    idPaciente:"", 
    estudios:[],
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado"
  }

  public pedidosRayox = {
    idPaciente:"", 
    estudios:[],
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado"
  }

  constructor(private _pagoServicios: PagoServiciosService,
              private _router: Router,
              private _ultrasonidoService: USGService,
              private _xrayService : XRAYService){}

  ngOnInit(): void {
    this.obtenerPacienteStorage();
    this.obtenerCarrito();
    this.obtenerTotalCarrito();
  }

  obtenerTotalCarrito(){
    this.totalSinMembresia = this.carrito.totalSin;
    this.totalCarritoMembresia= this.carrito.totalCon;
  }

  setDatesVenta(){
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    this.infoVenta.paciente = this.paciente._id;
    if(this.infoVenta.paciente == undefined){
      this.infoVenta.paciente = this.paciente.id;
    }
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.estudios = this.carrito.items;
    if(this.paciente.membresiaActiva){
      this.infoVenta.totalCompra = this.carrito.totalCon;
    }else{
      this.infoVenta.totalCompra = this.carrito.totalSin;
    }
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = CEDE;
    this.pedidosLaboratorios.idPaciente = this.paciente._id;
    console.log(this.pedidosLaboratorios.idPaciente);
    
    if(this.pedidosLaboratorios.idPaciente == undefined){
      this.pedidosLaboratorios.idPaciente = this.paciente.id;
    }
    // configuracion de los pedidos de ultrasonido
    this.pedidosUltrasonido.idPaciente = this.paciente._id;
    if(this.pedidosUltrasonido.idPaciente == undefined){
      this.pedidosUltrasonido.idPaciente = this.paciente.id;
    }
    this.pedidosUltrasonido.fecha = datesPedidoLab.getDate();
    this.pedidosUltrasonido.sede = CEDE;
    // configuracion de los pedidos de Rayos x
    this.pedidosRayox.idPaciente = this.paciente._id;
    if(this.pedidosRayox.idPaciente == undefined){
      this.pedidosRayox.idPaciente = this.paciente.id;
    }
    this.pedidosRayox.fecha = datesPedidoLab.getDate();
    this.pedidosRayox.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosRayox.sede = CEDE;
  }

  obtenerPacienteStorage(){
    const pacienteStorage = new PacienteStorage();
    this.paciente = pacienteStorage.verPacienteConMembresia();
    console.log( this.paciente );
  }

  obtenerCarrito(){
    let carritoSinMembresia = new Carrito();
    this.carrito = carritoSinMembresia.obtenerSotorageCarrito();
    this.totalSinMembresia = this.carrito.totalSin;
    this.totalCarritoMembresia= this.carrito.totalCon;
  }

  validarBoton(valor){    
    if( valor == 0  ){
      this.btnValidacion = false;
    }
  }

  eliminarCarritoSinMembresia( item  ){
    // console.log(item );
    let carritoMembresia = new Carrito();
    carritoMembresia.eliminar( item );
    this.obtenerCarrito();
  }


  calcularNuevoTotalEfectivo(){ 
    if(this.paciente.membresiaActiva){
      if(this.carrito.totalCon < this.infoVenta.montoEfectivo){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalCarritoMembresia = this.carrito.totalCon - this.infoVenta.montoEfectivo;      
        this.infoVenta.compraConMembresia = false;  
        this.validarBoton(this.totalCarritoMembresia);
      }
    }else{
      if(this.carrito.totalSin < this.infoVenta.montoEfectivo){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalSinMembresia = this.carrito.totalSin - this.infoVenta.montoEfectivo;      
        this.infoVenta.compraConMembresia = false;  
        this.validarBoton(this.totalSinMembresia);
      }
    }
  }

  calcuarMontoTarjetaDebito(){
   /* console.log(this.infoVenta.montoTarjetaDebito);
   console.log(this.totalSinMembresia); */
    if(this.paciente.membresiaActiva){
      if(this.infoVenta.montoTarjetaDebito > this.totalCarritoMembresia){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalCarritoMembresia = this.totalCarritoMembresia  - this.infoVenta.montoTarjetaDebito;
        this.validarBoton(this.totalCarritoMembresia);
      }
    }else{
      if(this.infoVenta.montoTarjetaDebito > this.totalSinMembresia){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalSinMembresia = this.totalSinMembresia  - this.infoVenta.montoTarjetaDebito;
        this.validarBoton(this.totalSinMembresia);
      }
    }
  }
 
  calcularMontoTarjetaCredito(){
    if(this.paciente.membresiaActiva){
      if(this.infoVenta.montoTarjteCredito > this.totalCarritoMembresia){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalCarritoMembresia = this.totalCarritoMembresia  - this.infoVenta.montoTarjteCredito;
        this.validarBoton(this.totalCarritoMembresia);
      } 
    }else{
      if(this.infoVenta.montoTarjteCredito > this.totalSinMembresia){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalSinMembresia = this.totalSinMembresia  - this.infoVenta.montoTarjteCredito;
        this.validarBoton(this.totalSinMembresia);
      } 
    }
  }

  calcularMontoTransferencia(){
    if(this.paciente.membresiaActiva){
      if(this.infoVenta.montoTranferencia > this.totalCarritoMembresia){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalCarritoMembresia = this.totalCarritoMembresia - this.infoVenta.montoTranferencia;
        this.validarBoton(this.totalCarritoMembresia);
      } 
    }else{
      if(this.infoVenta.montoTranferencia > this.totalSinMembresia){
        swal("Monto mayor",{icon:"warning"});
      }else{
        this.totalSinMembresia = this.totalSinMembresia - this.infoVenta.montoTranferencia;
        this.validarBoton(this.totalSinMembresia);
      } 
    } 
  }

  generarTicket(folio){
    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.paciente, this.carrito ,  this.infoVenta , folio);
  }

  agregarIva(){
     // agregamos la comision
    if(this.paciente.membresiaActiva){
      let iva = 0.0;
      if( this.infoVenta.tarjetCredito  ){
        iva = 1.9;
      }
      
      if( this.infoVenta.tarjetaDebito ){
        iva = 2.5;
      }
      let totalIva = (( this.totalCarritoMembresia * iva ) / 100);
      console.log("totalIva: "+totalIva)
      this.totalConIva =  this.totalCarritoMembresia + totalIva;
      console.log("total con iva G: "+this.totalConIva)
      this.totalCarritoMembresia =  Math.round(this.totalConIva);
    }else{
      let iva = 0.0;
      if( this.infoVenta.tarjetCredito  ){
        iva = 1.9;
      }else if( this.infoVenta.tarjetaDebito ){
        iva = 2.5;
      }
      let totalIva = (( this.totalSinMembresia * iva ) / 100);
      this.totalConIva =  this.totalSinMembresia + totalIva;
      this.totalSinMembresia =  Math.round(this.totalConIva);
    } 
  }

  enviarPedido(){
    //this.infoVenta.estudios = this.carrito.items;
    /* this.infoVenta.paciente = this.paciente.id; */
    this.setDatesVenta();
    console.log(this.paciente);
    
    console.log(this.infoVenta);
    
    this._pagoServicios.agregarPedido( this.infoVenta ).subscribe( (data) => {
      
      console.log(  data )
      // console.log( data );
      if(  data['ok'] ){
        console.log(data);
        
        this.generarTicket(data['folio']);
        // se crea la tabla de las ventas 
        swal("Venta generada exitosamente",{icon:"success"});
        // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        // seteamos las fechas
        this.setDatesPedidos();
        this.carrito.items.forEach((elemets: items) => {
          if( elemets.name  === 'laboratorio' ){
            elemets.estado = "En espera"
            this.pedidosLaboratorios.estudios.push( elemets )
          }
          if(  elemets.name == "MEMBRESIA"  ){
            this._router.navigateByUrl('/pacientes');
          }            
          if( elemets.name == "ultrasonido"  ){
            this.pedidosUltrasonido.estudios.push( elemets )
          }
          if (elemets.name == "xray"){
            this.pedidosRayox.estudios.push(elemets)
          }
        });
        
        if( this.pedidosLaboratorios.estudios.length > 0 ){
            // this.setDatesPedidos();
            this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios )
                                .subscribe( data => console.log( data ));
        }
        if( this.pedidosUltrasonido.estudios.length > 0   ){
            this._ultrasonidoService.postPedidosUltra( this.pedidosUltrasonido )
                                    .subscribe( data => console.log( data ));
        }
        if ( this.pedidosRayox.estudios.length > 0){
            this._xrayService.postPedidosXray(this.pedidosRayox)
                              .subscribe(data => console.log(data));
        }
        
        eliminarStorage();
        const eliminarPaciente = new PacienteStorage();
        eliminarPaciente.removePacienteStorage();  
        this._router.navigateByUrl('/');   
      }
    });
  }

  eliminar(i){
    const eliminarItem = new Carrito;
    eliminarItem.eliminar(i);
    this.obtenerCarrito();
    this.obtenerTotalCarrito();
  }

  calcularIva(){
    this.IVaDEl16 = ((this.carrito.totalSin  * 16 ) / 100);
    return this.IVaDEl16;
  }

}

interface items {
  nombreEstudio : string
  precioCon: string
  precioSin: string
  idEstudio: string
  name: string
  estado: string
}