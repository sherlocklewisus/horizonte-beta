import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import swal from 'sweetalert';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import Dates from 'src/app/classes/dates/date.class';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import Tickets from 'src/app/classes/tickets/ticket.class';

@Component({
  selector: 'app-prenatal-riesgo',
  templateUrl: './prenatal-riesgo.component.html',
  styleUrls: ['./prenatal-riesgo.component.css']
})
export class PrenatalRiesgoComponent implements OnInit {

  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  concepto:any[] = []
  medicos:any[] = []
  paquete:any[] = []
  citas:any[] = []
  examenes:any[] = []
  public egoCount = 0;
  public ego2Count = 0;
  public obsCount = 0;
  public ultCount=0;
  public labCount=0;
  public GOCount=0;
  public GSCount=0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true
  
    }
  
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
  fecha: string;


  constructor(public _router: ActivatedRoute, public _paquete:PaquetesService, public _route:Router, private _pagoServicios: PagoServiciosService) { }

  ngOnInit(): void {
    this.obtenerMedicos();
    this.obtenerPaquete();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete = data['paquetes']
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.verCosnultashechas();
    });
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "7 Consultas programadas con el Especialista" ){
        // console.log("Se lo lee");
        this.egoCount += 1;
        let cardEspecialista = document.querySelector('#cardEspecialista');
        let badgeEspecialista = document.querySelector('#badgeEspecialista');
        this.addClassesCss( cardEspecialista, badgeEspecialista);
        //this.hospitalizacion = element ;
      } 
      if( element.consulta === '3 consultas de Nutrición'){
        this.ego2Count += 1;
        let cardNutricion = document.querySelector('#cardNutricion');
        let badgeNutricion = document.querySelector('#badgeNutricion');
        this.addClassesCss( cardNutricion, badgeNutricion);
        //this.consultaDespuesParto = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "8 ultrasonidos Obstétricos" ){
        this.ultCount += 1;
        let cardUltrasonidoObs = document.querySelector('#UltrasonidoObsCard');
        let badgeUltrasonidoObs = document.querySelector('#badegeUltrasonidoObs');
        this.addClassesCss( cardUltrasonidoObs, badgeUltrasonidoObs);
      }
      if( examen.consulta === "1 higado y Via Biliar"  ){
        let cardHigado = document.querySelector('#HigadoCard');
        let badgeHigado = document.querySelector("#badgeHigado");
        this.addClassesCss( cardHigado, badgeHigado );
      }
      if(examen.consulta === "4 Obstétricos"){
        this.obsCount += 1;
        let cardObstetricos = document.querySelector('#obstetricosCard');
        let badgeObstetricos = document.querySelector("#badegeObstetricos");
        this.addClassesCss( cardObstetricos, badgeObstetricos );
      }
      if(examen.consulta === "1 Genetico (11-13.6 SDG)"){
        let cardGenetico = document.querySelector('#GeneticoCard');
        let badgeGenetico = document.querySelector('#badgeGenetico');
        this.addClassesCss( cardGenetico, badgeGenetico);
      }
      if( examen.consulta === "1 Estructural (  18- 24 SDG)"){
        let cardEstructural = document.querySelector("#EstructuralCard");
        let badgeEstructural = document.querySelector("#badgeEstructural");
        this.addClassesCss(cardEstructural, badgeEstructural)
      }
      if( examen.consulta === "1 Perfil biofisico (Ultimo trimestre)"){
        let cardBiofisico = document.querySelector("#BiofisicoCard");
        let badgeBiofisico = document.querySelector("#badgeBiofisico");
        this.addClassesCss(cardBiofisico, badgeBiofisico)
      }
      if( examen.consulta === "17 Exámenes de laboratorio"){
        this.labCount += 1;
        let cardLab = document.querySelector("#LabCard");
        let badgeLab = document.querySelector("#badgeLab");
        this.addClassesCss(cardLab, badgeLab)
      }
      if( examen.consulta === "1 Biometría Hemática Completa "){
        let cardBHC = document.querySelector("#BHCCard");
        let badgeBHC = document.querySelector("#badgeBHC");
        this.addClassesCss(cardBHC, badgeBHC)
      }
      if( examen.consulta === "1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)"){
        let cardQS = document.querySelector("#QSCard");
        let badgeQS = document.querySelector("#badgeQS");
        this.addClassesCss(cardQS, badgeQS)
      }
      if( examen.consulta === "3 Exámenes Generales de Orina"){
        this.GOCount += 1;
        let cardEGO = document.querySelector("#EGOCard");
        let badgeEGO = document.querySelector("#badgeEGO");
        this.addClassesCss(cardEGO, badgeEGO)
      }
      if( examen.consulta === "1 V.D.R.L."){
        let cardVDRL = document.querySelector("#VDRLCard");
        let badgeVDRL = document.querySelector("#badgeVDRL");
        this.addClassesCss(cardVDRL, badgeVDRL)
      }
      if( examen.consulta === "1 V.I.H."){
        let cardVIH = document.querySelector("#VIHCard");
        let badgeVIH = document.querySelector("#badgeVIH");
        this.addClassesCss(cardVIH, badgeVIH)
      }
      if( examen.consulta === "2 Grupos Sanguíneos (Ambos padres)"){
        this.GSCount += 1;
        let cardGSA = document.querySelector("#GSACard");
        let badgeGSA = document.querySelector("#badgeGSA");
        this.addClassesCss(cardGSA, badgeGSA)
      }
      if( examen.consulta === "1 Tiempos de Coagulación"){
        let cardTC = document.querySelector("#TCCard");
        let badgeTC = document.querySelector("#badgeTC");
        this.addClassesCss(cardTC, badgeTC)
      }
      if( examen.consulta === "1 Curva de tolerancia a la glucosa"){
        let cardCTG = document.querySelector("#CTGCard");
        let badgeCTG = document.querySelector("#badgeCTG");
        this.addClassesCss(cardCTG, badgeCTG)
      }
      if( examen.consulta === "1 Pruebas de funcionamiento hepático"){
        let cardPFH = document.querySelector("#PFHCard");
        let badgePFH = document.querySelector("#badgePFH");
        this.addClassesCss(cardPFH, badgePFH)
      }
      if( examen.consulta === "1 Depuración de proteínas en orina de 24 horas"){
        let cardDPO = document.querySelector("#DPOCard");
        let badgeDPO = document.querySelector("#badgeDPO");
        this.addClassesCss(cardDPO, badgeDPO)
      }
      if( examen.consulta === "1 Triple "){
        let cardTriple = document.querySelector("#TripleCard");
        let badgeTriple = document.querySelector("#badgeTriple");
        this.addClassesCss(cardTriple, badgeTriple)
      }
      if( examen.consulta === "1 Perfil tiroideo"){
        let cardPT = document.querySelector("#PTCard");
        let badgePT = document.querySelector("#badgePT");
        this.addClassesCss(cardPT, badgePT)
      }
      if( examen.consulta === "1 Urocultivo"){
        let cardUrocultivo = document.querySelector("#UrocultivoCard");
        let badgeUrocultivo = document.querySelector("#badgeUrocultivo");
        this.addClassesCss(cardUrocultivo, badgeUrocultivo)
      }
      if( examen.consulta === "1 Exudado vaginal"){
        let cardEV = document.querySelector("#EVCard");
        let badgeEV = document.querySelector("#badgeEV");
        this.addClassesCss(cardEV, badgeEV)
      }
    });
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
          swal( `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}` );
        }else{
          this.citas.forEach((cita:consultas, index)=>{
            if(cita.consulta === examen){
              swal( `Tipo:  ${this.citas[index].consulta}\n  Fecha: ${this.citas[index].fecha} \n Hora: ${this.citas[index].hora}\n   Médico ${this.citas[index].medico}\n  Atendió: ${this.citas[index].firma}` );
            }
          })
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    if( badge.id == "badgeEspecialista"){
      badge.innerHTML = this.egoCount.toString(); 
    }else if(badge.id == "badgeNutricion"){
      badge.innerHTML = this.ego2Count.toString();
    }else if(badge.id == "badegeUltrasonidoObs"){
      badge.innerHTML = this.ultCount.toString();
    }else if(badge.id == "badegeObstetricos"){
      badge.innerHTML = this.obsCount.toString();
    }else if(badge.id == "badgeLab"){
      badge.innerHTML = this.labCount.toString();
    }else if(badge.id == "badgeEGO"){
      badge.innerHTML = this.GOCount.toString();
    }else if(badge.id == "badgeGSA"){
      badge.innerHTML = this.GSCount.toString();
    }
    else {
      badge.innerHTML = "1";
    }
    badge.classList.add('badge-primary');
  }

  // Nueva programacion de insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      case 'laboratorio':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;
      
      case 'ultrasonido':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.ultrasonidos){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }
        this.consultas.consulta = ''
        
      break;

      default:
        break;
    }
  }

  agregarConsulta(){
    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      this.consultas.fecha = this.fechaConsulta;
      this.consultas.hora = this.horaIngreso;
      console.log(this.consultas)
      if(this.consultas.tipo == 'laboratorio' || this.consultas.tipo == 'ultrasonido'){
        this.consultas.tipo='examenesLab'
        //servicios
        this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
      }
      this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
        (data)=>{
          //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
          this.mostrarConsultas();
          this._pagoServicios.agregarPedido( this.infoVenta )
          .subscribe( (data) => {
            // console.log( data );
            if(  data['ok'] ){
              
              this.generarTicket(data['folio']);
                // se crea la tabla de las ventas 
                swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
              }
            });
          })
          //citas
          /* this.generarTicket(); */
        this.setDatesVenta(this.consultas.medico);          
      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
    }
  }

  //metodo 
  setDatesVenta(medico){
    const item = {
      nombreEstudio: "CONSULTA DE MEDICO GENERAL",
      precioCon:0,
      precioSin:0,
      _id:'5fd3ebca08ccbb0017712f0d'
    }
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    this.infoVenta.paciente = this.pacienteInfo.id;
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.carrito.items.push(item);
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;
        
  }
  
  pagar_consulta(nombre,medico){
    const dates = new Dates(); 
    console.log(nombre);
    //19
    switch (nombre) {
      case '1 Biometría Hemática Completa ':
        console.log('entros');
        
            this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            this.infoVenta.paciente = this.pacienteInfo.id;
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            this.carrito.items.push(this.item);
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
      case '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)':
        this.item.nombreEstudio = "QUIMICA SANGUINEA 6 ";
        this.item._id = '5fd2a4171cb0ea0017f57ddb';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '3 Exámenes Generales de Orina':
        this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
        this.item._id = '5fd28c2f1cb0ea0017f57c58';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 V.D.R.L.':
        this.item.nombreEstudio = "V.D.R.L.";
        this.item._id = '5fd29b611cb0ea0017f57d51';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 V.I.H.':
        this.item.nombreEstudio = "H.I.V. (PRUEBA PRESUNTIVA)";
        this.item._id = '5fd293501cb0ea0017f57caa';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '2 Grupos Sanguíneos (Ambos padres)':
        this.item.nombreEstudio = "GRUPO Y FACTOR RH";
        this.item._id = '5fd292b31cb0ea0017f57c9e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Tiempos de Coagulación':
        this.item.nombreEstudio = "TIEMPO DE COAGULACION  ";
        this.item._id = '5fd29a991cb0ea0017f57d41';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            this.generarTicket(data['folio']);

              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Curva de tolerancia a la glucosa':
        this.item.nombreEstudio = "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS";
        this.item._id = '5fd2a0051cb0ea0017f57d7a';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Pruebas de funcionamiento hepático':
        this.item.nombreEstudio = "PRUEBAS DE FUNCION HEPATICO (BT,BD,BI,TGO,TGP,ALK)";
        this.item._id = '55fd2992b1cb0ea0017f57d1e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Depuración de proteínas en orina de 24 horas':
        this.item.nombreEstudio = "PROTEINAS EN ORINA DE 24 HRS.";
        this.item._id = '5fd2984d1cb0ea0017f57d0f';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Triple ':
        this.item.nombreEstudio = "TRIPLE MARCADO CON INTERPRETACION GRAFICA";
        this.item._id = '5fd2a4821cb0ea0017f57de2';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Perfil tiroideo':
        this.item.nombreEstudio = "PERFIL TIROIDEO I";
        this.item._id = '5fd2a3771cb0ea0017f57dcf';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Urocultivo':
        this.item.nombreEstudio = "CULTIVO DE ORINA (UROCULTIVO)";
        this.item._id = '5fd28a2b1cb0ea0017f57c32';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Exudado vaginal':
        this.item.nombreEstudio = "CULTIVO CERVICO VAGINAL";
        this.item._id = '5fd289731cb0ea0017f57c24';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Perfil biofisico (Ultimo trimestre)':
        this.item.nombreEstudio = "ULTRASONIDO PERFIL BIOFISICO";
        this.item._id = '602edde98a29f000156dbc2d';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Estructural (  18- 24 SDG)':
        this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL SEGUNDO TRIMESTRE ";
        this.item._id = '5fd280c41cb0ea0017f57b91';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Genetico (11-13.6 SDG)':
        this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL PRIMER TRISMESTRE";
        this.item._id = '5fd2810b1cb0ea0017f57b96';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '4 Obstétricos':
        this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO CONVENCIONAL";
        this.item._id = '5fd280031cb0ea0017f57b87';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 higado y Via Biliar':
        this.item.nombreEstudio = "ULTRASONIDO DE HIGADO Y VIAS BILIARES";
        this.item._id = '5fd27ce51cb0ea0017f57b5a';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      default:
        break;
    }
  }
  mostrarConsultas(){
    this.obtenerPaquete();
  }

  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio);
  
  }
}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
