import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import swal from 'sweetalert';
import { FichaInfo } from 'src/app/classes/ficha-info-paciente';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import Dates from 'src/app/classes/dates/date.class';
import Tickets from 'src/app/classes/tickets/ticket.class';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';


@Component({
  selector: 'app-prenatal',
  templateUrl: './prenatal.component.html',
  styleUrls: ['./prenatal.component.css']
})
export class PrenatalComponent implements OnInit {

    @Input() id: String;
    consultas:any = { tipo: '', consulta: '', fecha: '', hora:'' , medico: '', firma: '' }
    item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
    medicos:any[] = []
    paquete:any[] = []
    concepto:any[] = []
    citas:any[] = []
    examenes:any[] = []
    totalpagos:number
    pagos:any = { semanaGesta:'', semanapago: '', pago: ''}
    tabularpagos:any = []
    public egoCount = 0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteFicha: FichaInfo;
    
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };

    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true
  
    }
  
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
    
    fecha: string;

  constructor(public _router: ActivatedRoute, public _paquete:PaquetesService, public _route:Router, private _pagoServicios: PagoServiciosService) {
    // this.obtenerPaquete();
  } 
  
  ngOnInit(): void {

    this.obtenerPaquete();
    this.obtenerMedicos();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {  
      // obtenemos la tabla intermedia del control de las visitas 5e683cf41c9d4400005e1727
      console.log(data);
      
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete = data['paquetes']
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.tabularpagos = this.paquete[0].pagos;
      console.log( this.pacienteInfo );
      console.log( this.tabularpagos );
      this.totalpagos = 0
      //console.log(this.pagos); 
      this.tabularpagos.forEach(element => {
        this.totalpagos=element.pago+this.totalpagos
      });
      // this.pacienteInfo = this.pacienteFicha;
      this.verCosnultashechas();
    });
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    // esta funcion se encarga de ver cuales de los paquetes ya estan hechas y las colorea
    let cardHospitalizacion = document.querySelector('#hospitalizacion');
    let badegHospitalizacion = document.querySelector('#badgeHospitalzacion');
    // consultas despues del parto
    let cardConsultas = document.querySelector('#consultaDespuesDelParto');
    let badgeConsultas = document.querySelector('#badgeParto');
    // incia el codifo que valida las consultas 
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Hospitalicación en habitación estándar" ){
        // console.log("Se lo lee");
        cardHospitalizacion.classList.add('border-primary');
        badegHospitalizacion.innerHTML = "1";
        badegHospitalizacion.classList.add('badge-primary');
        this.hospitalizacion = element ;
      } 
      if( element.consulta === 'Consulta despues del parto o Cesárea con Medicina General' ){
        cardConsultas.classList.add('border-primary');
        badgeConsultas.innerHTML = '1';
        badgeConsultas.classList.add('badge-primary');
        this.consultaDespuesParto = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "1 Biometría hemática completa" ){
        let cardBiometria = document.querySelector('#cardBiometria');
        let badgeBiometrie = document.querySelector('#badgeBiometria');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if( examen.consulta === "1 Quimica sanguinea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Trigliceridos )"  ){
        let cardQuimica = document.querySelector('#quimicaSanguineaCard');
        let badgeQuimica = document.querySelector("#badgeQuimica");
        this.addClassesCss( cardQuimica, badgeQuimica );
      }
      if( examen.consulta === "2 Examen General de Orina"  ){
        this.egoCount += 1;
        let egoCard = document.querySelector('#egoCard');
        let badgeEgo = document.querySelector("#badegeEgo1");
        this.addClassesCss( egoCard, badgeEgo );

      }
      if(examen.consulta === "1 V.D.R.L"){
        let cardVrl = document.querySelector('#VrlCard');
        let badgeVrl = document.querySelector('#badgeVrl');
        this.addClassesCss( cardVrl, badgeVrl );
      }
      if( examen.consulta === "1 Grupo Sanguineo"  ){
        let grupoSanguineo = document.querySelector("#grupoSanguineoCard");
        let badgeSanguineo = document.querySelector("#badgeGrupoSanguineo");
        this.addClassesCss(grupoSanguineo, badgeSanguineo)
      }
      if( examen.consulta === "1 V.I.H." ){
        let vihcard = document.querySelector('#vihcard');
        let vihbadge = document.querySelector('#vihbadge');
        this.addClassesCss( vihcard, vihbadge );
      }
      if(  examen.consulta === "1 Tiempos de Coagulación" ){
        let tiemposCoagulacionCard = document.querySelector("#tiemposCoagulacionCard");
        let  tiemposCoagulacionBadge = document.querySelector('#tiemposCoagulacionBadge');
        this.addClassesCss( tiemposCoagulacionCard, tiemposCoagulacionBadge );
      }
      if(  examen.consulta === "1 Curva de tolerancia a la Glucosa"  ){
        let curvaDetoleranciaCard = document.querySelector("#curvaDetoleranciaCard");
        let curvaDetoleranciaBadge = document.querySelector("#curvaDetoleranciaBadge");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge );
      }
    });
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
          swal( `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}` );
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    if( badge.id == "badegeEgo1"  ){
      badge.innerHTML = this.egoCount.toString(); 
    }else {
      badge.innerHTML = "1";
    }
    badge.classList.add('badge-primary');
  } 

  mostrarHospitalizacion(){
    console.log( this.hospitalizacion );
    swal( `Tipo:  ${this.hospitalizacion.consulta}\n  Fecha: ${this.hospitalizacion.fecha} \n Hora: ${this.hospitalizacion.hora}\n   Médico ${this.hospitalizacion.medico}\n  Atendió: ${this.hospitalizacion.firma}` );
  }

  cambiarVisita( selectCon ){
    let nombreConsultaSelect = selectCon.value ;
    if(this.consultas.tipo  === 'examenesLab' ){
      // let log = console.log;
      // checar si nos se han consumido los laboratorios
      this.examenes.forEach((examen:consultas) =>  {
          if( nombreConsultaSelect == examen.consulta && examen.consulta == nombreConsultaSelect ){
            swal("Este elemento ya ha sido usado",{icon:"error"})
            /* alert('Este elemento ya ha sido usado'); */
            // console.log("Biometria hematica");
            this.btnAgregaConsultas = true;
          }
      });
    }
    
    this.citas.forEach( element => {
      if( nombreConsultaSelect ===  element.consulta && element.consulta === "Hospitalicación en habitación estándar"  ){
        swal("Este elemento ya ha sido usado",{icon:"error"})
        this.btnAgregaConsultas = true;
      }else if(nombreConsultaSelect ===  element.consulta && element.consulta === "Consulta despues del parto o Cesárea con Medicina General"){
        this.btnAgregaConsultas = true;
        swal("Este elemento ya ha sido usado",{icon:"error"})
      }
      this.btnAgregaConsultas = false;
      if(  nombreConsultaSelect  != "Hospitalicación en habitación estándar" && nombreConsultaSelect  != "Consulta despues del parto o Cesárea con Medicina General" && nombreConsultaSelect != "1 Biometría hemática completa" && nombreConsultaSelect != "Quimica sanguinea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Trigliceridos )"  && nombreConsultaSelect != "1 V.D.R.L" && nombreConsultaSelect != "1 V.I.H." && nombreConsultaSelect != "1 Grupo Sanguineo" && nombreConsultaSelect != "1 Tiempos de Coagulación"  ){
        this.btnAgregaConsultas = false;        
      }
      if( this.egoCount == 2  ){
        this.btnAgregaConsultas = false;
      }
    }) 
  }

  //insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      default:
        break;
    }
  }

  agregarConsulta(){
    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      this.consultas.fecha = this.fechaConsulta;
      this.consultas.hora = this.horaIngreso;
      console.log(this.consultas)
      this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
        (data)=>{
          //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
          this.mostrarConsultas();
          this._pagoServicios.agregarPedido( this.infoVenta )
          .subscribe( (data) => {
            // console.log( data );
            if(  data['ok'] ){
              
              this.generarTicket(data['folio']);
                // se crea la tabla de las ventas 
                swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                // seteamos las fechas 
                  eliminarStorage();
                  
                  
                  const eliminarPaciente = new PacienteStorage();
                  eliminarPaciente.removePacienteStorage();  
              }
          });
        })

        //citas
        this.setDatesVenta(this.consultas.medico);          
        /* this.generarTicket(); */

        this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
    }
  }

  //metodo 
  setDatesVenta(medico){
    const item = {
      nombreEstudio: "CONSULTA DE MEDICO GENERAL",
      precioCon:0,
      precioSin:0,
      _id:'5fd3ebca08ccbb0017712f0d'
    }
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    this.infoVenta.paciente = this.pacienteInfo.id;
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.carrito.items.push(item);
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  agregarPago(){
    console.log(this.pagos)
    if(this.pagos.tipo == '' || this.pagos.pago == '' || this.pagos.semanaGesta == '' || this.pagos.semanapago == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      this._paquete.agregarConsulta(this.pagos,'pagos',this.id).subscribe((data)=>{
        console.log(data)
        swal('Pago Agregada', 'se agrego el pago', 'success')
        this.ngOnInit();
      })
      this.pagos = {semanaGesta:'', semanapago: '', pago: ''}
    }
  }

  pagar_consulta(nombre, medico){
    const dates = new Dates();
    switch (nombre) {
      case '1 Biometría hemática completa':
            this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            this.infoVenta.paciente = this.pacienteInfo.id;
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            this.carrito.items.push(this.item);
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
      break;
      case '1 Quimica sanguinea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Trigliceridos )':
        this.item.nombreEstudio = "QUIMICA SANGUINEA 6 ";
        this.item._id = '5fd2a4171cb0ea0017f57ddb';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas(); 
      break;
      case '2 Examen General de Orina':
        this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
        this.item._id = '5fd28c2f1cb0ea0017f57c58';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();  
      break;
      case '1 V.D.R.L':
        this.item.nombreEstudio = "V.D.R.L.";
        this.item._id = '5fd29b611cb0ea0017f57d51';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Grupo Sanguineo':
        this.item.nombreEstudio = "GRUPO Y FACTOR RH";
        this.item._id = '5fd292b31cb0ea0017f57c9e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 V.I.H.':
        this.item.nombreEstudio = "H.I.V. (PRUEBA PRESUNTIVA)";
        this.item._id = '5fd293501cb0ea0017f57caa';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Tiempos de Coagulación':
        this.item.nombreEstudio = "TIEMPO DE COAGULACION  ";
        this.item._id = '5fd29a991cb0ea0017f57d41';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Curva de tolerancia a la Glucosa':
        this.item.nombreEstudio = "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS";
        this.item._id = '5fd2a0051cb0ea0017f57d7a';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      default:
        break;
    }
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }
  
  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio );
  
  }
  mostrarConsultasDespuesParto(){
    swal( `Tipo:  ${this.consultaDespuesParto.consulta}\n  Fecha: ${this.consultaDespuesParto.fecha} \n Hora: ${this.consultaDespuesParto.hora}\n   Médico ${this.consultaDespuesParto.medico}\n  Atendió: ${this.consultaDespuesParto.firma}` );
  }

  mostrarDatos(consulta, medico, servicio){
    if(servicio == '1') swal('Citas incluidas\n','Tipo de consulta:'+consulta+'\nMedico: '+medico,'')
    if(servicio == '2') swal('Examenes de Laboratorio\n','Tipo de laboratorio:'+consulta+'\nMedico: '+medico,'')
  }

  printComponent(cmpName) {
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload();
}

}


class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}

  
class paqueteDB{ 
CitasIncluidas:  []
consultasGinecologia: Number
contenidoPaquete: []
costoTotal: 14500
examenesLaboratorio:  []
tabuladorPagos: []
icon: String;
nombrePaquete: String;
nomenclatura: String;
nomenclaturaPaciente: []
_id: String;
}