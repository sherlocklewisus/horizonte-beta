import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import swal from 'sweetalert';
import { PagoServiciosService } from 'src/app/services/pagos/pago-servicios.service';
import Dates from 'src/app/classes/dates/date.class';
import { getDataStorage } from 'src/app/functions/storage.funcion';
import { CEDE } from 'src/app/classes/cedes/cedes.class';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import Tickets from 'src/app/classes/tickets/ticket.class';

@Component({
  selector: 'app-vida-plena',
  templateUrl: './vida-plena.component.html',
  styleUrls: ['./vida-plena.component.css']
})
export class VidaPlenaComponent implements OnInit {
  
  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  concepto:any[] = []
  medicos:any[] = []
  paquete:any[] = []
  citas:any[] = []
  examenes:any[] = []
  public egoCount = 0;
  public ego2Count = 0;
  public obsCount = 0;
  public ultCount=0;
  public labCount=0;
  public GOCount=0;
  public GSCount=0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true
  
    }
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
  fecha: string;
  constructor(public _router: ActivatedRoute, public _paquete:PaquetesService, public _route:Router,private _pagoServicios: PagoServiciosService) { }

  ngOnInit(): void {
    this.obtenerMedicos();
    this.obtenerPaquete();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete = data['paquetes']
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.verCosnultashechas();
    });
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "8 Consultas programadas con Especialista MEDICINA INTERNA" ){
        // console.log("Se lo lee");
        this.egoCount += 1;
        let cardCPEMI = document.querySelector('#cardCPEMI');
        let badgeCPEMI = document.querySelector('#badgeCPEMI');
        this.addClassesCss( cardCPEMI, badgeCPEMI);
        //this.hospitalizacion = element ;
      } 
      if( element.consulta === 'Cita abierta a urgencias desde la contratación del servicio por un año con Medicina General.'){
        let cardCA = document.querySelector('#cardCA');
        let badgeCA = document.querySelector('#badgeCA');
        this.addClassesCss( cardCA, badgeCA);
        //this.consultaDespuesParto = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "1 Biometría Hemática Completa " ){
        let cardBHC = document.querySelector('#BHCCard');
        let badgeBHC = document.querySelector('#badegeBHC');
        this.addClassesCss( cardBHC, badgeBHC);
      }
      if( examen.consulta === "1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)"  ){
        let cardQS = document.querySelector('#QSCard');
        let badgeQS = document.querySelector("#badgeQS");
        this.addClassesCss( cardQS, badgeQS );
      }
      if(examen.consulta === "1 Pruebas de Funcionamiento Hepático"){
        let cardPFH = document.querySelector('#PFHCard');
        let badgePFH = document.querySelector("#badegePFH");
        this.addClassesCss( cardPFH, badgePFH );
      }
      if(examen.consulta === "1 Exámenes Generales de Orina"){
        let cardEGO = document.querySelector('#EGOCard');
        let badgeEGO = document.querySelector('#badgeEGO');
        this.addClassesCss( cardEGO, badgeEGO);
      }
      if( examen.consulta === "1 Tele de Tórax."){
        let cardTT = document.querySelector("#TTCard");
        let badgeTT = document.querySelector("#badgeTT");
        this.addClassesCss(cardTT, badgeTT)
      }
      if( examen.consulta === "1 Electrocardiograma"){
        let cardElectroC = document.querySelector("#ElectroCCard");
        let badgeElectroC = document.querySelector("#badgeElectroC");
        this.addClassesCss(cardElectroC, badgeElectroC)
      }
    });
   //   //  termina el codigo que valida los examenes 
  }


  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
          swal( `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}` );
        }else{
          this.citas.forEach((cita:consultas, index)=>{
            if(cita.consulta === examen){
              swal( `Tipo:  ${this.citas[index].consulta}\n  Fecha: ${this.citas[index].fecha} \n Hora: ${this.citas[index].hora}\n   Médico ${this.citas[index].medico}\n  Atendió: ${this.citas[index].firma}` );
            }
          })
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    badge.innerHTML = "1";
    badge.classList.add('badge-primary');
  }

  // Nueva programacion de insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;

      case 'laboratorio':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }  
        this.consultas.consulta = ''
      break;
      
      case 'ultrasonido':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.ultrasonidos){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }
        this.consultas.consulta = ''
        
      break;

      case '4':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.extras){
            this.concepto.push(item2)
          }
          console.log(this.concepto)
        }
        this.consultas.consulta = ''
        
      break;

      default:
        break;
    }
  }

  agregarConsulta(){
    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
      swal('Error!', 'Porfavor ingrese los datos que le piden', 'error')
    }else{
      this.consultas.fecha = this.fechaConsulta;
      this.consultas.hora = this.horaIngreso;
      console.log(this.consultas)
      if(this.consultas.tipo == 'laboratorio' || this.consultas.tipo == 'ultrasonido'){
        this.consultas.tipo='examenesLab'
        
        //servicios
        this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
      }
      this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
        (data)=>{
          //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
          this.mostrarConsultas();
          this._pagoServicios.agregarPedido( this.infoVenta )
          .subscribe( (data) => {
            // console.log( data );
            if(  data['ok'] ){
              
              this.generarTicket(data['folio']);
                // se crea la tabla de las ventas 
                swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                // seteamos las fechas 
                  eliminarStorage();
                  
                  
                  const eliminarPaciente = new PacienteStorage();
                  eliminarPaciente.removePacienteStorage();  
              }
          });
        })
        //citas
        this.setDatesVenta(this.consultas.medico);          
          /* this.generarTicket();
 */
      this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
    }
  }

  //metodo 
  setDatesVenta(medico){
    const item = {
      nombreEstudio: "CONSULTA DE MEDICO GENERAL",
      precioCon:0,
      precioSin:0,
      _id:'5fd3ebca08ccbb0017712f0d'
    }
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    this.infoVenta.paciente = this.pacienteInfo.id;
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.carrito.items.push(item);
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }

  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio);
  
  }
  
  pagar_consulta(nombre,medico){
    const dates = new Dates();
    switch (nombre) {
      case "1 Biometría Hemática Completa ":
        this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            this.infoVenta.paciente = this.pacienteInfo.id;
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = CEDE;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            this.carrito.items.push(this.item);
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
      case '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)':
        this.item.nombreEstudio = "QUIMICA SANGUINEA 6 ";
        this.item._id = '5fd2a4171cb0ea0017f57ddb';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            // se crea la tabla de las ventas 
            this.generarTicket(data['folio']);
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Pruebas de Funcionamiento Hepático':
        this.item.nombreEstudio = "PRUEBAS DE FUNCION HEPATICO (BT,BD,BI,TGO,TGP,ALK)";
        this.item._id = '5fd2992b1cb0ea0017f57d1e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Exámenes Generales de Orina':
        this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
        this.item._id = '5fd28c2f1cb0ea0017f57c58';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Tele de Tórax.':
        this.item.nombreEstudio = "RADIOGRAFÍA TELE DE TÓRAX";
        this.item._id = '5fd4f80e8adaca00176bc5b8';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      case '1 Electrocardiograma':
        this.item.nombreEstudio = "ELECTROCARDIOGRAMA";
        this.item._id = '5fd7c35961c8830017d478b3';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        this.infoVenta.paciente = this.pacienteInfo.id;
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = CEDE;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        this.carrito.items.push(this.item);
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')

              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
      break;
      default:
        break;
    }
  }
}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
