import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionAdversaComponent } from './reaccion-adversa.component';

describe('ReaccionAdversaComponent', () => {
  let component: ReaccionAdversaComponent;
  let fixture: ComponentFixture<ReaccionAdversaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionAdversaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionAdversaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
