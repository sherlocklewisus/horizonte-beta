import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reaccion-adversa',
  templateUrl: './reaccion-adversa.component.html',
  styleUrls: ['./reaccion-adversa.component.css']
})
export class ReaccionAdversaComponent implements OnInit {

  @Output() tiempodepresentacion = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit(): void {
  }

  formularioTiempoPresentacion(form:NgForm){
    console.log(form.form.value);
    
    this.tiempodepresentacion.emit(form.form.value);
  }

}
