import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionToxicidadComponent } from './reaccion-toxicidad.component';

describe('ReaccionToxicidadComponent', () => {
  let component: ReaccionToxicidadComponent;
  let fixture: ComponentFixture<ReaccionToxicidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionToxicidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionToxicidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
