import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionVasovagalComponent } from './reaccion-vasovagal.component';

describe('ReaccionVasovagalComponent', () => {
  let component: ReaccionVasovagalComponent;
  let fixture: ComponentFixture<ReaccionVasovagalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionVasovagalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionVasovagalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
