import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionVenopuncionComponent } from './reaccion-venopuncion.component';

describe('ReaccionVenopuncionComponent', () => {
  let component: ReaccionVenopuncionComponent;
  let fixture: ComponentFixture<ReaccionVenopuncionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionVenopuncionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionVenopuncionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
