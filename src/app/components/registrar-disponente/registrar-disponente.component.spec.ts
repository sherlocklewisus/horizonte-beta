import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarDisponenteComponent } from './registrar-disponente.component';

describe('RegistrarDisponenteComponent', () => {
  let component: RegistrarDisponenteComponent;
  let fixture: ComponentFixture<RegistrarDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
