import { Component, Input, OnInit } from '@angular/core';
import { LocationServicesService } from 'src/app/services/otrosService/location-services.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import {  MatStepper  } from '@angular/material/stepper';
import {  Router } from '@angular/router';
import { splitAtColon } from '@angular/compiler/src/util';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert'

@Component({
  selector: 'app-registrar-disponente',
  templateUrl: './registrar-disponente.component.html',
  styleUrls: ['./registrar-disponente.component.css']
})
export class RegistrarDisponenteComponent implements OnInit {

  public estados: [];
  public paises:[];
  public municipios:[];
  public estado: string;
  public paquetes: [];
  public validateBtn = false;
  public edadPaciente: HTMLInputElement;

  constructor(public _locationService: LocationServicesService,
              public _pacienteService: PacientesService,
              private _router: Router) { }

  ngOnInit(): void {
    this.obtenerEstados();
    this.obtenerPaises();
  }

  datosFiscales(form: NgForm){
    console.log(form);
    
    var inputValue = document.getElementById('cpRazonSocial') as HTMLInputElement;
    var inputValue2 = document.getElementById('estadoRazonSocial') as HTMLInputElement;
    var inputValue3 = document.getElementById('municipioRazonSocial') as HTMLInputElement;
    var inputValue4 = document.getElementById('razonSocial1Calle') as HTMLInputElement;
    var inputValue5 = document.getElementById('correoRazonSocial') as HTMLInputElement;
    var inputValue6 = document.getElementById('calleRazonSocial') as HTMLInputElement;
    console.log(form.value.municipio);
    
    inputValue.value = form.value.codigoPostal;
    inputValue2.value = form.value.estadoPaciente;
    inputValue3.value = form.value.municipio;
    inputValue4.value = form.value.referencia1;
    inputValue5.value = form.value.correoPaciente;
    inputValue6.value = form.value.referencia2;
  }

  obtenerPaises(){
    this._locationService.getCountries().subscribe((data:any) => {  
        this.paises = data;
    });
  }

  obtenerEstados(){
    this._locationService.getEstado().subscribe((data:any) => {
        this.estados = data.response['estado']
    });
  }

  ObtenerMunicipio(estado){
    this._locationService.getMunicipios(estado).subscribe((data:any) => {
        this.municipios = data['response']['municipios']
    });
  }

  buscarMunicipios() {
    this.ObtenerMunicipio( this.estado )
  }

  validarSexo( sexo: string ){
    if(  sexo.length < 4 || sexo == "" ){
      return false;
    }else{
      return true;
    }
  }

  validarEdad(edad:number){
    if( edad > 110  ){
      return false;
    }else{
      return true;
    }
  }

  generarEdad(edadForm: HTMLInputElement ) {
    this.edadPaciente = document.querySelector('#edad');
    let fecha = edadForm.value;
    let splitFecha = fecha.split('-');
    var fechaActual = new Date();
    var anio = fechaActual.getFullYear();
    let edadNormal = anio - parseFloat( splitFecha[0]  );
    let edades = edadNormal.toString();
    this.edadPaciente.value = edades;
  }

  validarCurp( curp: string  ){
    if(  curp.length == 18  ){
      return true;
    }else{
      return false;
    }
  }

  validarString(nombre: string){
    if(nombre == ""  || nombre.length <3 ){
        return false;
    }
    return true;
  }

  message(msj){
    swal(msj,{icon:"warning"})
  }

  validacones(form){
    if( this.validarString(form.nombrePaciente) ){
    }else {
      this.message('Completa el nombre')
      return false;
    }

    if(this.validarString(form.apellidoPaterno)){
    }else {
      this.message('Completa el apellido')
      return false;
    }

    if( this.validarString(form.apellidoMaterno) ){
    }else{
      this.message('Completa el apellido')
      return false;
    }

    if( this.validarCurp( form.curp  )   ){
    }else{
      this.message('Ingresa un curp valido')
      return false;
    }

    if(this.validarEdad(form.edad)  ){
    }else{
      this.message('Ingrea una edad valida')
      return false;
    }

    if(this.validarSexo(form.genero)){
    }else{
      this.message('Ingresa el sexo del paciente');
      return false;
    }
    return true;
  }

  enviar( form: NgForm  ){
    let resultado =this.validacones( form.value );
    form.value.edad = this.edadPaciente.value;
    form.value.disponente = "DISPONENTE";
    if(resultado){
      this.validateBtn = true;
      this._pacienteService.setPacientes(form.value ,'TLYC01').subscribe((data) => {
        if(  data['ok'] ){
          swal("Disponente registrado",{icon:"success"})
          this._router.navigateByUrl('/bitacora/disponentes')
        }
      });
    }else {
      return;
    }
  }
}
