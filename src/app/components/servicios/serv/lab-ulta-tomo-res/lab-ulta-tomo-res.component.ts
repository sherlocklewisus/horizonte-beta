import { Component, OnInit } from '@angular/core';
import { IntegradosService } from '../../../../services/servicios/integrados.service';
import { Router, ActivatedRoute} from '@angular/router';
import { gaurdarCotizacion } from '../../../../functions/storage.funcion';
import { getCarritoStorage } from '../../../../functions/pacienteIntegrados';
import {  getrolefunction  } from '../../../../functions/storage.funcion';
import swal from 'sweetalert';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
import Carrito  from '../../../../classes/carrito/carrito.class';

@Component({
  selector: 'app-lab-ulta-tomo-res',
  templateUrl: './lab-ulta-tomo-res.component.html',
  styleUrls: ['./lab-ulta-tomo-res.component.css']
})
export class LabUltaTomoResComponent implements OnInit {

  
  // data de los servicios
  public serviceSi: any [] = [];
  public ambulanciaSI: any [] = [];
  public totalAmbulancia: string;
  public termino: string;
  public pagina = 0;
  public showTableAmbulanacia = true;
  public showAmbulanacia = false;
  public searching = false;
  public servicios:string;
  public roleuser = "";
  public membresia: boolean;
  public mostrar=true;
  public paciente :any;

  public todosLosServicios = {
    ambulancia: [],
    endoscopia: [],
    laboratorios: [],
    message: '',
    ok: false,
    otrosServicio: [],
    patologia: [],
    rayosX: [],
    tomografia: [],
    ultrasonido: []
  };

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };

  public buscar = {
    estudio:''
  };

  constructor(private _service: IntegradosService, private _router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.servicios = this.activatedRoute.snapshot.paramMap.get('servicio');
    //console.log(this.servicios)
    this.obtenerCarrito();
    this.obtenerMembresia();
    this.getRoleUser();
  }


  obtenerCarrito () {
   
    const carrito = new Carrito();
    this.carrito = carrito.obtenerSotorageCarrito();
  
  }


  obtenerMembresia(){

    const usuarioMembresia = new PacienteStorage();
    this.paciente = usuarioMembresia.verPacienteConMembresia();

    if( this.paciente == null || this.paciente.membresiaHabilitada == false ) {
    
      this.verDatos();

    }else{ 
      this.membresia = usuarioMembresia.verMembresiaPaciente(); 
      console.log(this.membresia);
      this.verDatos();  
    }
  

  }

  getRoleUser(){
    this.roleuser = getrolefunction();
    console.log( this.roleuser );
  }

  busquedaGeneral(){
    if(this.buscar.estudio.length >3){
      this._service.getAllDepartments(this.buscar)
      .subscribe((res:any) => {
        // console.log( res );
        if(res.data[0]!=0){
          this.ambulanciaSI = res.data[0];
          this.showAmbulanacia= true;
          this.showTableAmbulanacia = false;
        }else if(res.data[1] != 0){
          this.serviceSi = res.data[1];
          this.searching = true;
        }            
      })
    }
    if(this.buscar.estudio == ''){
      this.searching = false;
      this.mostrar=true;
      this.showAmbulanacia= false;
      this.showTableAmbulanacia = true;
      this.obtenerCarrito();
      this.verDatos();
    }
  }

  
  agregarCarrito( event, item: any ) {
    const carrito = new Carrito();
    this.carrito =  carrito.agregarItem( event, item );
   
}

eliminar( item ) {

  const carrito = new Carrito();
  carrito.eliminar( item );
  this.obtenerCarrito();

}

  verDatos(){
    this._service.getObtener(this.servicios).subscribe(
      (res: any) => {
          console.log( res );
          this.serviceSi = res.data;
          console.log(this.serviceSi);
          this.totalAmbulancia = res.data.results;
      },
      err => {
        console.log(<any>err);
      }
    );   
  }

  alertcomparasion( ev, precioPublico, precioMembresia, item2:any ){
    let precioSinTrim  =  precioPublico.replace('$', '');
    let precioSinComaPublico = precioSinTrim.replace(',', '');
    let precioMemTrim  =  precioMembresia.replace('$', '');
    let precioMemComaMembresia = precioMemTrim.replace(',', '');

    swal("Con membresia ahorras:"+(precioSinComaPublico - precioMemComaMembresia),{icon:"success"});
  }

  showAlert(){
    swal({title: "Estas seguro de contratar a este destino?",
    text: "El servicio de ambulancia solo será requerido para dicho destino, no puede haber cambios",
    icon: "warning",
    buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "OK",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }
    },
    dangerMode: true,
  })
  .then((value) => {
    console.log( value );
    if (value) {
      swal("Vamos a llenar el papeleo!", {
        icon: "success",
      });
      this._router.navigateByUrl('/hoja-fram');
    } else if( value == null ) {
      swal("Tranquilo, Puedes intentar contratar algun otro destino!", {
        icon: "error",
      });
    }});

  }

  editarServicio(id){
    swal({title: "Estas seguro de Editar este servicio?",
    text: "Una vez que se haya editado el servicio, no se podrá recuperar",
    icon: "warning",
    buttons: [true, true],
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      this._router.navigateByUrl('formularioeditarservice/' +id)
    } else if (willDelete == null){
      swal("Tranquilo, el servicio sigue estando ahí..", {
        icon: "error",
      });
      this._router.navigateByUrl('serviciosInt/'+this.servicios);
    }});
  }
  
  delete(id) {
    swal(
      {title:"¿Estas seguro de eliminar este servicio?",text:"Una vez eliminado el servicio, no se podra recuperar",icon:"warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
      },
      dangerMode: true,}
    ).then((willDelete) => {
      if (willDelete) {
        this._service.deleteService(id).subscribe(
          response => {
            swal("El servicio se ha eliminado correctamente",{icon:"success"});
            this.verDatos();
            this._router.navigateByUrl('serviciosInt/'+this.servicios);
          },
          error => {
            console.log(error);
          }
        );
      } else if (willDelete == null){
        swal("Tranquilo, el destino sigue estando ahí..", {
          icon: "error",
        });
      }}); 
  }

}
