import { TestBed } from '@angular/core/testing';

import { IslogedGuard } from './isloged.guard';

describe('IslogedGuard', () => {
  let guard: IslogedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IslogedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
