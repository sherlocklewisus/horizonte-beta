import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import  { getDataStorage  } from '../../functions/storage.funcion'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IslogedGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      

      if( getDataStorage() == null ){
        return false;
       }else {
         return true;
       }


    return true;
  }
  
}
