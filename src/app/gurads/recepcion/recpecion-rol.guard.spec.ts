import { TestBed } from '@angular/core/testing';

import { RecpecionRolGuard } from './recpecion-rol.guard';

describe('RecpecionRolGuard', () => {
  let guard: RecpecionRolGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RecpecionRolGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
