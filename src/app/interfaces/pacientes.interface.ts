
export default class Pacientes {
    constructor(
     public  nombre: String,
     public  apellidoPaterno: String,
     public  apellidoMaterno: String,
     public  estadoPaciente: String,
    
     public  fechaNacimiento: String,
     public  telefono: String,
     public  edad: String,
     public  genero: String,
      public curp:String,
      public callePaciente:String,
      
     ) {
  
    }
    
  }
  