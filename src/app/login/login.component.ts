import {  LoginService  } from '../services/login.service';
import { Component, OnInit } from '@angular/core';
import {  Personal } from '../interfaces/usuario.interface'
import { Router  } from '@angular/router';
import {  NgForm  } from '@angular/forms';
// import * as swal from 'sweetalert';
import swal from 'sweetAlert';
import { CEDE } from '../classes/cedes/cedes.class';
import { WsLoginService } from '../services/sockets/chat/ws-login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  private cede = '';

  constructor(
    private wsloginService: WsLoginService,
    public _loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if( this.cede === null || this.cede == "" ){ 
      this.setCede();
    }
    this.verSede();
    
  }
  
  
  // validaciones de las sedes en localstorage
  setCede(){
    
    this.wsloginService.adios();
    localStorage.setItem('cede', JSON.stringify( CEDE ) );

  }

  verSede(){
    this.wsloginService.adios();
    this.cede = JSON.parse( localStorage.getItem('cede') );
    // console.log(  this.cede);
  }

  // esta funcio se llama para hacer el login con los datos del usuario

  ingresar( forma: NgForm ) {
    
    // console.log( forma )
    this._loginService.logout();
    this.wsloginService.adios();
    if( forma.invalid ){
      swal('Llena todos los campos','','error');
      return false;
    }

   const nombrePersonal = forma.value.nombre.trim();
   const passwordPersonal = forma.value.password.trim();

    if( !nombrePersonal ){
      swal('Nombre no valido', 'Ingresalo de nuevo', 'error');
      forma.reset();
      return
    }
    else if(!passwordPersonal){
      swal('Contraseña no valida', 'Ingresalo de nuevo', 'error');
      forma.reset();
      return
    }

    let personal = new Personal( nombrePersonal, passwordPersonal );
    
    this._loginService.login( personal, this.cede )
    .subscribe( (correcto: any)=> {

      if(!correcto){
        alert('entra');
      }

      if( correcto ){
        console.log(correcto)
        this.router.navigate(['/dashboard'])
      } 
    },
      error => {
          if( error['ok']  == false){
            forma.reset();
            swal('El nombre o la contraseña son incorrectos', 'Ingresalos de nuevo', 'error');
          }
      }
     )
    
  

  }


}
