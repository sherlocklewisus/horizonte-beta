import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleMaterialesComponent } from './detalle-materiales.component';

describe('DetalleMaterialesComponent', () => {
  let component: DetalleMaterialesComponent;
  let fixture: ComponentFixture<DetalleMaterialesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleMaterialesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleMaterialesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
