import { Component, OnInit } from '@angular/core';
import { AlmacenService } from 'src/app/services/almacenBs/almacen.service';

@Component({
  selector: 'app-lista-materiales',
  templateUrl: './lista-materiales.component.html',
  styleUrls: ['./lista-materiales.component.css']
})
export class ListaMaterialesComponent implements OnInit {

  constructor( private servicioAlmace: AlmacenService) { }

  public servicios = [];

  ngOnInit(): void {
    this.obtenerServicios();
  }

  obtenerServicios(){
    this.servicioAlmace.getListadoMateriales().subscribe((data:any) => {
      this.servicios = data.data;
  
  })
}
}
