import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleProductosAlmacenComponent } from './detalle-productos-almacen.component';

describe('DetalleProductosAlmacenComponent', () => {
  let component: DetalleProductosAlmacenComponent;
  let fixture: ComponentFixture<DetalleProductosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleProductosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleProductosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
