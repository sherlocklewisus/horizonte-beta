import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProductosAlmacenComponent } from './editar-productos-almacen.component';

describe('EditarProductosAlmacenComponent', () => {
  let component: EditarProductosAlmacenComponent;
  let fixture: ComponentFixture<EditarProductosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarProductosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProductosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
