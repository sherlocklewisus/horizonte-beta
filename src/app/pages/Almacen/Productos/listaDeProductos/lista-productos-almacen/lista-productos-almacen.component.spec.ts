import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaProductosAlmacenComponent } from './lista-productos-almacen.component';

describe('ListaProductosAlmacenComponent', () => {
  let component: ListaProductosAlmacenComponent;
  let fixture: ComponentFixture<ListaProductosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaProductosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaProductosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
