import { Component, OnInit } from '@angular/core';
import { AlmacenService } from 'src/app/services/almacenBs/almacen.service';

@Component({
  selector: 'app-lista-productos-almacen',
  templateUrl: './lista-productos-almacen.component.html',
  styleUrls: ['./lista-productos-almacen.component.css']
})
export class ListaProductosAlmacenComponent implements OnInit {

  public productos = [];
  public idSelectedForDelete = "";
  public txtSearch = "";
  constructor( private _almacenService: AlmacenService ) { }

  ngOnInit(): void {
    this.obetenerProductos();
  }


  obetenerProductos() {
    this._almacenService.obtenerProductosAlmacenBitacora()
                .subscribe((data:any) => this.setProductosAlmacen(data.data));
  }

  setProductosAlmacen(data:any) {
    this.productos = data;
  }


  disabledProdcut( id: string){
    
    const messageAlertSection = document.querySelector('#section-message-alert');
    messageAlertSection.classList.remove('dispaly-none');
    messageAlertSection.classList.add('dispaly-block');
    this.idSelectedForDelete = id;

  }

  btnConfirmDelete() {
    const messageAlertSection = document.querySelector('#section-message-alert');

    this._almacenService.disabledAProduct( this.idSelectedForDelete )
    .subscribe((data:any) => {

      if(data.ok){
        setTimeout(() => {
          messageAlertSection.classList.remove('dispaly-block');
          messageAlertSection.classList.add('dispaly-none');
          this.idSelectedForDelete = "";
          this.obetenerProductos();
        }, 2000);
        
      }
    });
  }


  filterChangeByName() {

    const productsFilter =[];

    if( this.txtSearch.length >= 3 ) {
      
      
      this.productos.forEach( (producto) => {
          // console.log( producto);
        if(producto.nombre.includes(this.txtSearch.toUpperCase()) ){
          productsFilter.push( producto );
        }
     
      });

      this.productos = productsFilter;
    
    
    }else{
      this.obetenerProductos();
    }
  }

  cancelBtnAlertMessage() {
    const messageAlertSection = document.querySelector('#section-message-alert');
    messageAlertSection.classList.remove('dispaly-block');
    messageAlertSection.classList.add('dispaly-none')
  }

}
