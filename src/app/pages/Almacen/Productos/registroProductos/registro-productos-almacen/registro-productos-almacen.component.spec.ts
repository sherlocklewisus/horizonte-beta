import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroProductosAlmacenComponent } from './registro-productos-almacen.component';

describe('RegistroProductosAlmacenComponent', () => {
  let component: RegistroProductosAlmacenComponent;
  let fixture: ComponentFixture<RegistroProductosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroProductosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroProductosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
