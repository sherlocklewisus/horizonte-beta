import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder,  Validators, } from '@angular/forms';
import { Router } from '@angular/router';
import { AlmacenService } from 'src/app/services/almacenBs/almacen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-productos-almacen',
  templateUrl: './registro-productos-almacen.component.html',
  styleUrls: ['./registro-productos-almacen.component.css']
})
export class RegistroProductosAlmacenComponent implements OnInit {

  // inicializacion de la variables
  public btnRegistro = true;
  public newFormProductos: any;
  public proveedoresCensur = [];
  public countOfProducts = 0;
  public newIdProducto ="";

  constructor(
    private formBuilder: FormBuilder,
    private _alamcenService: AlmacenService,
    private _router: Router,
  ) { }
    // dependencias necesarias

  ngOnInit(): void {
    // mandamos a llamar el metodo que crea el formulario dinamico
    this.obtenerProveedoresCensur();
    this.creacionDeFormulario();
    this.getCountOfProducts();
  }

  getCountOfProducts() { 
    this._alamcenService.countProducts()
    .subscribe((data:any) => this.setTotalOfProducts(data.data));
  }

  setTotalOfProducts(data: number) {
    this.countOfProducts = data;
  }

  get valoresProveedor () {
    // obtenemos los controles que estan dentro del form Array
    return this.newFormProductos.get('proveedor') as FormArray;
  }


  agregarProveedor() {
    // metodo para crear el una nueva fila de controles
    const valoresProveedor = this.formBuilder.group({
      proveedor: '',
      costo: 0
    });

    this.valoresProveedor.push( valoresProveedor );
    // console.log("Agregar ejecutado")
  }

  obtenerProveedoresCensur() {
    this._alamcenService.obetenerProveedores()
      .subscribe( (data:any) => this.setProveedores(data.data));
  }

  setProveedores(data) {
    this.proveedoresCensur = data;
  }
  
  creacionDeFormulario() {
    
    // creacion del formualrio dinamico
    this.newFormProductos = this.formBuilder.group({
      nombre: ['', { validators: [Validators.required] }],
      tipo_producto: ['', { validators: [Validators.required] }],
      nombre_comercial: ['', { validators: [Validators.required] }],
      idProducto: [ this.newIdProducto , { validators: [] }],
      descripcion: ['', { validators: [Validators.required] }],
      proveedor: this.formBuilder.array([]),
      laboratorio: ['', { validators: [Validators.required] }],
    });

    this.agregarProveedor();
    // hacemos el push de los forms reactivos
  }

  validarFormulario() {
    // validmaos que el formualrio se haya llenado de forma correcta
    if(this.newFormProductos.valid) {
        this.btnRegistro = false;
        this.createAnId( this.newFormProductos.value );
      }
  }

  enviarFormualrio() {

    // validamos el formulario
    this.newFormProductos.value.idProducto = this.newIdProducto
    if( this.newFormProductos.valid ){
      // bloqueamos el boton 

      this.btnRegistro = true;
      //enviamos la peticion HTTP
      this._alamcenService.registroProductosAlmacen( this.newFormProductos.value )
      .subscribe( (data:any) => {
      
        // si la respuesta el true disparamos el alert 
        if(data.ok) {
          
          Swal.fire({
            icon: 'success',
            title: '',
            text: 'SE REGISTRO EXITOSAMENTE EL PRODUCTO',
          });
            // esperamos 2s para que redireccione al dash
          setTimeout(() => {
            
            this._router.navigateByUrl('/');
          }, 2000)
        }
      });

    }else{
      alert('Algo pasó');
     }

  }


  // funcion que se encraga de la creaicon de los ID de los producos
  createAnId(data: any){


    const { nombre_comercial, proveedor} = data;
    const nombreComercialCutted = nombre_comercial.slice( 0, 3 ).toUpperCase();
    const proveedorOne = proveedor[0].proveedor;
    const proveedorCutted = proveedorOne.slice( 0, 3 );

    const newIdOfProduct =  `${nombreComercialCutted}/${proveedorCutted}/${this.countOfProducts + 1}/${new Date().getMinutes()}/${new Date().getSeconds()}`;
    this.newIdProducto = newIdOfProduct;
  }

}
