import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockProductosAlamacenBSComponent } from './stock-productos-alamacen-bs.component';

describe('StockProductosAlamacenBSComponent', () => {
  let component: StockProductosAlamacenBSComponent;
  let fixture: ComponentFixture<StockProductosAlamacenBSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockProductosAlamacenBSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockProductosAlamacenBSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
