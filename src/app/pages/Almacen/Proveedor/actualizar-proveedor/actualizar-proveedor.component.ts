import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import Swal from 'sweetalert2';
@Component({
  selector: 'app-actualizar-proveedor',
  templateUrl: './actualizar-proveedor.component.html',
  styleUrls: ['./actualizar-proveedor.component.css']
})
export class ActualizarProveedorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  guardar( forma:NgForm){

    if (forma.invalid) {

      Object.values( forma.controls ).forEach( control =>{
      control.markAsTouched();
    });
      
    }else { 
      Swal.fire({
        title: 'EL PROOVEEDOR SE ACTUALIZO EXISTOSAMENTE',
        icon: 'success'
        }).then( forma => {
          forma.value
        }) 
    } 
  }
}
