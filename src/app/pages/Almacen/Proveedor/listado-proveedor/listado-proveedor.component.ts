import { Component, OnInit } from '@angular/core';
import { ProveedorService } from '../servicio-proveedor/proveedor.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listado-proveedor',
  templateUrl: './listado-proveedor.component.html',
  styleUrls: ['./listado-proveedor.component.css']
})
export class ListadoProveedorComponent implements OnInit {

  public proveedores = [];
  constructor( private ProveedorService: ProveedorService) { }

  ngOnInit(): void { 
    this.obtenerProveedores();
  }


  obtenerProveedores(){
    this.ProveedorService.getProveedor().subscribe((data:any) => {
      this.proveedores = data.data;
    })
  }

  borrarProveedor(){
    Swal.fire({
      title: '¿SEGURO QUE DESEAS ELIMINAR EL PROVEEDOR?',
      text: "NO SE PODRA REVERTIR LA ACCIÓN",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#13B42D',
      cancelButtonColor: '#FF0000',
      cancelButtonText: 'NO CANCELAR',
      confirmButtonText: 'SI, ACEPTAR'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'SE ELIMINO EL PROVEEDOR EXITOSAMENTE',
          '',
          'success'
        )
      }
    })
  }
}
