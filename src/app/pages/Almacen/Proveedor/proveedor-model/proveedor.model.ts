
export class proveedorModel{
    nombreEmpresa: string = "";
    idProveedor:string = "";
    razonSocial :string = "";
    domicilio:string = "";
    contacto:string = "";
    correo:string = "";
    rfcEmpresa:string = "";
    diasDistribucion :string = "";
    descripcion: string = "";
}