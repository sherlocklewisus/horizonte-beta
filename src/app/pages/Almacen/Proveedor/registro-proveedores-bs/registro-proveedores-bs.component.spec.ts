import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroProveedoresBsComponent } from './registro-proveedores-bs.component';

describe('RegistroProveedoresBsComponent', () => {
  let component: RegistroProveedoresBsComponent;
  let fixture: ComponentFixture<RegistroProveedoresBsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroProveedoresBsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroProveedoresBsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
