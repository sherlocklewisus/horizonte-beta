import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProveedorService } from '../servicio-proveedor/proveedor.service';
import { NgForm } from "@angular/forms";
import Swal from 'sweetalert2';
import { proveedorModel } from '../proveedor-model/proveedor.model'

@Component({
  selector: 'app-registro-proveedores-bs',
  templateUrl: './registro-proveedores-bs.component.html',
  styleUrls: ['./registro-proveedores-bs.component.css']
})
export class RegistroProveedoresBsComponent implements OnInit {

  public btnRegistrar = false;
  proveedor = new proveedorModel();
  constructor( 
    private proveedorService: ProveedorService,
    private _router: Router
    ) { }

  ngOnInit(): void {
  }

  guardar( forma:NgForm){

    if (forma.invalid) {
      Object.values( forma.controls ).forEach( control =>{
      control.markAsTouched();
    });
    Swal.fire({
      title: 'FAVOR DE LLENAR TODOS LOS CAMPOS',
      icon: 'error'
      }).then( forma => {
        forma.value
      }) 
      
    }else { 
     this.proveedorService.crearProveedor( this.proveedor ).
      subscribe( resp=>{
        this.btnRegistrar = true;

        this._router.navigateByUrl('/');
      })
      Swal.fire({
        title: 'EL PROOVEEDOR SE CREO EXISTOSAMENTE',
        icon: 'success'
        }).then( forma => {
          forma.value
        }) 
        this.generarIdProv();
    } 
  }

  generarIdProv(){
    const idprov = this.proveedor.rfcEmpresa;
    let date: Date = new Date();
    let dia, mes, anio, hora, min;
     hora = date.getHours();
     min = date.getUTCSeconds();
     dia = date.getUTCDay();
     mes = date.getMonth();
     anio = date.getUTCFullYear();
    
     const idGenerado = `${idprov}/${anio}/${mes}/${dia}/${hora}/${min}`;
    return idGenerado;
  }
}
