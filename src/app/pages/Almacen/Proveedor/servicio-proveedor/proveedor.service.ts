import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { proveedorModel } from "../proveedor-model/proveedor.model";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})

export class ProveedorService{

    private url = 'http://localhost:3200'

    constructor( private http: HttpClient) {  }

    //Postear los proveedores
    crearProveedor( proveedor: proveedorModel ){
      return this.http.post(`${ this.url }/registro/proveedor`, proveedor);
    }
    
    //Obtener los proveedores
    getProveedor(){
     return this.http.get(`${ this.url }/ver/proveedores`);
    }

    
    
}