import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearSeguimientosAlmacenComponent } from './crear-seguimientos-almacen.component';

describe('CrearSeguimientosAlmacenComponent', () => {
  let component: CrearSeguimientosAlmacenComponent;
  let fixture: ComponentFixture<CrearSeguimientosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearSeguimientosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearSeguimientosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
