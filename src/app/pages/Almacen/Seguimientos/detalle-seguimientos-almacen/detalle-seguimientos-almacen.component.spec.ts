import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSeguimientosAlmacenComponent } from './detalle-seguimientos-almacen.component';

describe('DetalleSeguimientosAlmacenComponent', () => {
  let component: DetalleSeguimientosAlmacenComponent;
  let fixture: ComponentFixture<DetalleSeguimientosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleSeguimientosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSeguimientosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
