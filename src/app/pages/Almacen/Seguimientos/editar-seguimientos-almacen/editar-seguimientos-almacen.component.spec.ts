import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarSeguimientosAlmacenComponent } from './editar-seguimientos-almacen.component';

describe('EditarSeguimientosAlmacenComponent', () => {
  let component: EditarSeguimientosAlmacenComponent;
  let fixture: ComponentFixture<EditarSeguimientosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarSeguimientosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarSeguimientosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
