import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoSeguimientosAlmacenComponent } from './listado-seguimientos-almacen.component';

describe('ListadoSeguimientosAlmacenComponent', () => {
  let component: ListadoSeguimientosAlmacenComponent;
  let fixture: ComponentFixture<ListadoSeguimientosAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoSeguimientosAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoSeguimientosAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
