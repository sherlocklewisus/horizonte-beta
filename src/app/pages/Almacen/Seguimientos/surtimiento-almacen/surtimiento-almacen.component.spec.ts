import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurtimientoAlmacenComponent } from './surtimiento-almacen.component';

describe('SurtimientoAlmacenComponent', () => {
  let component: SurtimientoAlmacenComponent;
  let fixture: ComponentFixture<SurtimientoAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurtimientoAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurtimientoAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
