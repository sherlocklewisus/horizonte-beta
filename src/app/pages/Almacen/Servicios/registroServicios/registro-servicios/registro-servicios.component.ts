import { Component, OnInit } from '@angular/core';
import {  MatStepper  } from '@angular/material/stepper';
import { NgForm } from '@angular/forms';
import { AlmacenService } from 'src/app/services/almacenBs/almacen.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-registro-servicios',
  templateUrl: './registro-servicios.component.html',
  styleUrls: ['./registro-servicios.component.css']
})
export class RegistroServiciosComponent implements OnInit {

  public schedules = [];
  public counterAllService  =  0;
  public txtSearchInput = "";
  public productsAndServiceDB = [];
  public productsAndServiceJson = [];

  constructor(
    private _almacenService: AlmacenService
  ) { }

  ngOnInit(): void {
  }

  // buscamos en la DB los productos y servicio en la DB
  buscarProductosYMateriales(){
    this.productsAndServiceDB = [];
    const searchTxtJson = {
      name: this.txtSearchInput
    }

    if( this.txtSearchInput.length >= 3 ) {
      
      this._almacenService.obtenerLosProductosYMateriales(  searchTxtJson )
          .subscribe((data:any) =>  this.setProductsAndService(data.data));
    }else {
      this.productsAndServiceDB = [];
    } 

  }

//funcion que se encarga de agregar y validar que no haya producto y materiales repetidos en el json
  addANewMaterial(value:any) {
    

    // Si es la primera vez que se agrega un producto o material que se agregue al array
      if(this.productsAndServiceJson.length == 0){

        this.productsAndServiceJson.push(value)
      
      }else{
        let encontrado;
       

        encontrado = this.productsAndServiceJson.findIndex(elelemt => elelemt == value);
        if (encontrado >= 0) {
          alert("No lo puedes agregar otra vez");
          
        } else {

          this.productsAndServiceJson.push(value);
        }
  
      }

    this.productsAndServiceDB = [];
    this.txtSearchInput = "";
  
  }

  //setea los productos y materiales con los que cuenta el servicio
  setProductsAndService(data:any) {
    this.productsAndServiceDB = data;
  }
 
  //Elimina del array el elemento que se pinta en el html
  deleteProductFromJson( index:number) {
    this.productsAndServiceJson.splice(index, 1);
  }

  // funcion que agrega al json la cantidad
  addACantidad(fcantidad, producto){

    const newProduct = {
      producto,
      cantidad: parseFloat(fcantidad.value),
    }

    this.productsAndServiceJson.forEach((element, index) => {
      
      if(element == newProduct.producto){
        this.productsAndServiceJson.splice(index, 1);
        this.productsAndServiceJson.push(newProduct);
      }
    })

  }

  // hacemos el post de los servicios
  onSubmit(f){
    
    if(!f.valid) {      

      Swal.fire({
        icon: 'error',
        title: '',
        text: 'INGRESA TODOS LOS CAMPOS',
      });

    return
    
    }else {

      const { value } = f;
      value.horario = this.schedules;
      
      if(value.horario == []) {
   
        alert('Ingresa los dias del servicio');
      
      }else{
   
        this.generateAnId( value );

      this._almacenService.nuevoServicioCensur( f.value )
        .subscribe( (data:any) => {
          
          if(data.ok) {
          
            Swal.fire({
              icon: 'success',
              title: '',
              text: 'SE REGISTRO EXITOSAMENTE EL SERVICIO',
            });
          
          }else {
            Swal.fire({
              icon: 'error',
              title: '',
              text: 'INTENTALO MÁS TARDE',
            });

          }
        });
   
      }

      }
  }


  //agregamos un nuevo horario a los servicios 
  addSchedule(  horario : String ) {  

    this.getCounterAllSerivices();

    if( this.schedules.length == 0 ) {

      this.schedules.push( horario );
      
    }else {
      

        for( let i = 0; i <= this.schedules.length; i++ ) {


          if( this.schedules.length >= 1 && horario === "todosLosDias" ) {
           
            alert('No se puede agregar');
            return;
          }

          if( this.schedules[i] === "todosLosDias" ) {
            alert('No puedes agregar mas días');
            return
  
          }else if( horario != this.schedules[i] ) {
  
            this.schedules.push(horario);
            return; 
        }


        }
    
    }
  }

//obtenemos el count de los serivicios registrados para sacar el ID consecutivo
  getCounterAllSerivices ( ){
    this._almacenService.getCounterAllDocumentsSerivices()
    .subscribe((data:any) => {
      // seteamos los servicios 
      if(data.ok) {
        this.counterAllService = data.data;
      }else{
        alert("Intenta mas tarde");
        this.counterAllService = 0
      }
    })
  }

  // generamos el ID de los servicios 
  generateAnId(value:any){

    const { nombre, tipoServicio, categoria, profesionalARealizar  } = value;

    const nombreCutted = nombre.slice( 0, 3 ).toUpperCase();
    const tipoServicioCutted = tipoServicio.slice(0,3).toUpperCase();
    const categoriaCutted = categoria.slice(0,3).toUpperCase();
    const profesionalARealizarCutted = profesionalARealizar.slice(0,3).toUpperCase();
  
    const idService = `${ nombreCutted}/${tipoServicioCutted}/${categoriaCutted}/${profesionalARealizarCutted}/${this.counterAllService}/${new Date().getMinutes()}/${new Date().getSeconds()} ` 

    value.idServicio = idService;
    
  } 

}