import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraConsultaDisponentesComponent } from './bitacora-consulta-disponentes.component';

describe('BitacoraConsultaDisponentesComponent', () => {
  let component: BitacoraConsultaDisponentesComponent;
  let fixture: ComponentFixture<BitacoraConsultaDisponentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraConsultaDisponentesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraConsultaDisponentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
