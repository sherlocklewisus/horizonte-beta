import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaEvolucionDisponenteComponent } from './hoja-evolucion-disponente.component';

describe('HojaEvolucionDisponenteComponent', () => {
  let component: HojaEvolucionDisponenteComponent;
  let fixture: ComponentFixture<HojaEvolucionDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaEvolucionDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaEvolucionDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
