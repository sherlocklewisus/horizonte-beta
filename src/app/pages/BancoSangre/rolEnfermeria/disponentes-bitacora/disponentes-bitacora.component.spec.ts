import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisponentesBitacoraComponent } from './disponentes-bitacora.component';

describe('DisponentesBitacoraComponent', () => {
  let component: DisponentesBitacoraComponent;
  let fixture: ComponentFixture<DisponentesBitacoraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisponentesBitacoraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisponentesBitacoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
