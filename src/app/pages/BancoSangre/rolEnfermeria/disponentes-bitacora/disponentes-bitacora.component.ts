import { Component, OnInit } from '@angular/core';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';

@Component({
  selector: 'app-disponentes-bitacora',
  templateUrl: './disponentes-bitacora.component.html',
  styleUrls: ['./disponentes-bitacora.component.css']
})
export class DisponentesBitacoraComponent implements OnInit {

  public pacientes:any [] = [];

  constructor(private _banco:BancoService) { }

  ngOnInit(): void {
    this.getBitacora();
  }

  getBitacora(){
    this._banco.getEnfermeria().subscribe((resp:any)=>{
      this.pacientes = resp['data'];   
      /* console.log(this.pacientes); */
    })
  }
}
