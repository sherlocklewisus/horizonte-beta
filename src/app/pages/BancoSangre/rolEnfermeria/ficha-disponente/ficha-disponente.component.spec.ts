import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaDisponenteComponent } from './ficha-disponente.component';

describe('FichaDisponenteComponent', () => {
  let component: FichaDisponenteComponent;
  let fixture: ComponentFixture<FichaDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichaDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
