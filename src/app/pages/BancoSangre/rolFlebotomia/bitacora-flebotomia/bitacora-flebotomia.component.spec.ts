import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraFlebotomiaComponent } from './bitacora-flebotomia.component';

describe('BitacoraFlebotomiaComponent', () => {
  let component: BitacoraFlebotomiaComponent;
  let fixture: ComponentFixture<BitacoraFlebotomiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraFlebotomiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraFlebotomiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
