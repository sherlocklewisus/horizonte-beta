import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosDonanteFlebotomiaComponent } from './documentos-donante-flebotomia.component';

describe('DocumentosDonanteFlebotomiaComponent', () => {
  let component: DocumentosDonanteFlebotomiaComponent;
  let fixture: ComponentFixture<DocumentosDonanteFlebotomiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentosDonanteFlebotomiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosDonanteFlebotomiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
