import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionesDerivadasComponent } from './reacciones-derivadas.component';

describe('ReaccionesDerivadasComponent', () => {
  let component: ReaccionesDerivadasComponent;
  let fixture: ComponentFixture<ReaccionesDerivadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionesDerivadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionesDerivadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
