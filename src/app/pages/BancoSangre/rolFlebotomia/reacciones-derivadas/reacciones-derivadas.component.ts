import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';

@Component({
  selector: 'app-reacciones-derivadas',
  templateUrl: './reacciones-derivadas.component.html',
  styleUrls: ['./reacciones-derivadas.component.css']
})
export class ReaccionesDerivadasComponent implements OnInit {

  public idCensur;
  public paciente={
    apellidoMaterno: "",
    apellidoPaterno: "",
    edad: 0,
    nombrePaciente: "",
    _id: "",
    disponente: "",
    fechaNacimientoPaciente: "",
    estadoPaciente: "",
    callePaciente: "",
    telefono: 0,
  }
  
  constructor(private _banco: BancoService,
              private activatedRoute:ActivatedRoute){}

  ngOnInit(): void {
    this.idCensur = this.activatedRoute.snapshot.paramMap.get('id');
    /* this.getDisponente(this.idCensur); */
  }

  reaccionesVasovagalleve(event){
    console.log(event);
    
  }

  reaccionesVasovagalmoderada(event){
    console.log(event);
    
  }

  reaccionesVasovagalsevera(event){
    console.log(event);
    
  }

  getDisponente(id){
    this._banco.getDisponente(id).subscribe((resp:any)=>{
      this.paciente = resp.data.paciente
    })
  }
}
