import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'formsede',
  templateUrl: './agregar-sede.component.html',
  styleUrls: ['./agregar-sede.component.css']
})
export class AgregarSedeComponent implements OnInit {

  formsede: FormGroup
  submitted = false;

  constructor(private formBuilder: FormBuilder) {
    this.formsede= this.formBuilder.group({

      clinica: ['', [Validators.required]],
      razon :['', [Validators.required]],
      rfc:['', [Validators.required]],
      responsable_sa:['', [Validators.required]],
      contacto:['', [Validators.required]],
      genero:['', [Validators.required]],
      fecha:['', [Validators.required]],
      telefono:['', [Validators.required]],
      usuario:['', [Validators.required]],
      contrasena:['', [Validators.required]],
      correo:['', [Validators.required]],
      responsable_si:['', [Validators.required]],
      calle:['', [Validators.required]],
      no_exterior:['', [Validators.required]],
      postal:['', [Validators.required]],
      pais:['', [Validators.required]],
      estado:['', [Validators.required]],
      municipio:['', [Validators.required]],
      ref_1:['', [Validators.required]],
      ref_2:['', [Validators.required]],
      razon_em:['', [Validators.required]],
      rfc_em:['', [Validators.required]],
      postal_em:['', [Validators.required]],
      estado_em:['', [Validators.required]],
      municipio_em:['', [Validators.required]],
      calle_em:['', [Validators.required]],
      no_exterior_em:['', [Validators.required]],
      correo_em:['', [Validators.required]]




    });
  }

  get f() {
    
    return this.formsede.controls 
  
  }

  onsavesede() {

    this.submitted = true;

    if (this.formsede.valid) {
      alert("Datos validados")
    }else{
      console.log("Datos incorrectos")
    }
  }

  ngOnInit(): void {
  }


}
