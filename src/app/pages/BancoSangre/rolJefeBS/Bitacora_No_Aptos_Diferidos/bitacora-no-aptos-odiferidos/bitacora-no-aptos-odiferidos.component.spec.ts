import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraNoAptosODiferidosComponent } from './bitacora-no-aptos-odiferidos.component';

describe('BitacoraNoAptosODiferidosComponent', () => {
  let component: BitacoraNoAptosODiferidosComponent;
  let fixture: ComponentFixture<BitacoraNoAptosODiferidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraNoAptosODiferidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraNoAptosODiferidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
