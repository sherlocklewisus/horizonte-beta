import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FISedeComponent } from './fisede.component';

describe('FISedeComponent', () => {
  let component: FISedeComponent;
  let fixture: ComponentFixture<FISedeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FISedeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FISedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
