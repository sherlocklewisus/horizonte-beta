import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaServicioReceptorBSComponent } from './hoja-servicio-receptor.component';

describe('HojaServicioReceptorBSComponent', () => {
  let component: HojaServicioReceptorBSComponent;
  let fixture: ComponentFixture<HojaServicioReceptorBSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaServicioReceptorBSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaServicioReceptorBSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
