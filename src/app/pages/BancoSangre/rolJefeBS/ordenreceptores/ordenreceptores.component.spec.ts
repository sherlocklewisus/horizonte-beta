import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenreceptoresComponent } from './ordenreceptores.component';

describe('OrdenreceptoresComponent', () => {
  let component: OrdenreceptoresComponent;
  let fixture: ComponentFixture<OrdenreceptoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenreceptoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdenreceptoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
