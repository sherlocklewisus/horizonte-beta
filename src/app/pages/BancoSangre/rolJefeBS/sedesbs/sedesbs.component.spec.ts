import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SedesbsComponent } from './sedesbs.component';

describe('SedesbsComponent', () => {
  let component: SedesbsComponent;
  let fixture: ComponentFixture<SedesbsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SedesbsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SedesbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
