import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraLaboratorioComponent } from './bitacora-laboratorio.component';

describe('BitacoraLaboratorioComponent', () => {
  let component: BitacoraLaboratorioComponent;
  let fixture: ComponentFixture<BitacoraLaboratorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraLaboratorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraLaboratorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
