import { Component, OnInit } from '@angular/core';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';

@Component({
  selector: 'app-bitacora-laboratorio',
  templateUrl: './bitacora-laboratorio.component.html',
  styleUrls: ['./bitacora-laboratorio.component.css']
})
export class BitacoraLaboratorioComponent implements OnInit {

  public bitacoraPacientes = [{
    paciente: {
      nombrePaciente: '',
    }
  }]
  constructor(public _banco:BancoService) { }

  ngOnInit(): void {
    this.bitacora();
    ///bitacora/laboratorio implementar este metodo
  }

  bitacora(){
    this._banco.bitacoraLaboratorio()
    .subscribe((resp:any)=> {
      if(resp.ok){
        this.setDataBitacora(resp.data);
      }
    })
  }

  setDataBitacora(data) {
    this.bitacoraPacientes = data;
    /* console.log( this.bitacoraPacientes ); */
  }

}
