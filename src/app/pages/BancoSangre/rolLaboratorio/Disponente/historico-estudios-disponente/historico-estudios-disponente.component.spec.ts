import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoEstudiosDisponenteComponent } from './historico-estudios-disponente.component';

describe('HistoricoEstudiosDisponenteComponent', () => {
  let component: HistoricoEstudiosDisponenteComponent;
  let fixture: ComponentFixture<HistoricoEstudiosDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricoEstudiosDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoEstudiosDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
