import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaServiciosLabDisponenteComponent } from './hoja-servicios-lab-disponente.component';

describe('HojaServiciosLabDisponenteComponent', () => {
  let component: HojaServiciosLabDisponenteComponent;
  let fixture: ComponentFixture<HojaServiciosLabDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaServiciosLabDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaServiciosLabDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
