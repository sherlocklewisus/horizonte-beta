import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaServiciosComponent } from './hoja-servicios.component';

describe('HojaServiciosComponent', () => {
  let component: HojaServiciosComponent;
  let fixture: ComponentFixture<HojaServiciosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaServiciosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaServiciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
