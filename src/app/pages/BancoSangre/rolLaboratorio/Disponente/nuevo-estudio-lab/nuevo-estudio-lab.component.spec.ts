import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoEstudioLabComponent } from './nuevo-estudio-lab.component';

describe('NuevoEstudioLabComponent', () => {
  let component: NuevoEstudioLabComponent;
  let fixture: ComponentFixture<NuevoEstudioLabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoEstudioLabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoEstudioLabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
