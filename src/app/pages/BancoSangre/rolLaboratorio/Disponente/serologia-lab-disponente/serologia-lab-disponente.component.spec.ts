import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerologiaLabDisponenteComponent } from './serologia-lab-disponente.component';

describe('SerologiaLabDisponenteComponent', () => {
  let component: SerologiaLabDisponenteComponent;
  let fixture: ComponentFixture<SerologiaLabDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SerologiaLabDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SerologiaLabDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
