import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';
import { serologia_disp } from '../../../../../functions/bancoSangre';
import Swal from 'sweetalert2'
import * as moment from 'moment';
@Component({
  selector: 'app-serologia-lab-disponente',
  templateUrl: './serologia-lab-disponente.component.html',
  styleUrls: ['./serologia-lab-disponente.component.css']
})
export class SerologiaLabDisponenteComponent implements OnInit {

    public idBanco = "";
    
    public paciente={
      nombrePaciente: '',
      apellidoPaterno: '',
      apellidoMaterno: '',
      curp: '',
      edad: '',
      genero: '',
      _id:'',
      callePaciente: '',
      fechaNacimientoPaciente:'',
      estadoPaciente: '',
      paisPaciente: '',
      telefono: '',
      receptor: '',
      tipoDeSangre: '',
      disponente: '',
      religion:''
    } 

    resultadoCulero: FormGroup;
        
    ngNop = 'NO REACTIVO';
    ngSip = 'REACTIVO';
    resultado0 = 'NO REACTIVO';
    resultado1 = 'NO REACTIVO';
    resultado2 = 'NO REACTIVO';
    resultado3 = 'NO REACTIVO';
    resultado4 = 'NO REACTIVO';
    resultado5 = 'NO REACTIVO';
    resultado6 = 'NO REACTIVO';

    resultado: string[] = ['NO REACTIVO', 'NO REACTIVO', 'NO REACTIVO', 'NO REACTIVO','NO REACTIVO','NO REACTIVO','NO REACTIVO'];

    public metodos=[];
    public elementos=[]; 
    public sero = {
      idlaboratorio:'',
      metodo:[],
      quimico:'',
      tecnico:'',
      cedula:'',
      proceso:'',
      obtenidos:[],
      productos:[],
      idbancodesangre:''
    }
    public donaciones=0;
    public donacionesPaciente=0;
    public respuesta={
      diferido: [{
        fechafinal: "",
        fechainicio: "",
        motivo: "",
        tiempoDiferir: "",
      }],
      diferir: true,
      proceso: "",
      respuestas: 0
    }
     
  constructor(
    private _route: ActivatedRoute, 
    private route: Router, 
    private _banco: BancoService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    // 
    this.idBanco = this._route.snapshot.paramMap.get('id');
    this.getFichadeIdentificacion(this.idBanco)
    this.dataSerologia();

    // Form Culero :v
  //   this.resultadoCulero = this.formBuilder.group({
  //     resultado0: ['NO REACTIVO'],
  //     resultado1: ['NO REACTIVO'],
  //     resultado2: ['NO REACTIVO'],
  //     resultado3: ['NO REACTIVO'],
  //     resultado4: ['NO REACTIVO'],
  //     resultado5: ['NO REACTIVO'],
  //     resultado6: ['NO REACTIVO'],
  // });


  }

  getFichadeIdentificacion ( id ){
    this._banco.getHojaDeServicioLab(id).subscribe((resp:any)=>{
      /* console.log(resp); */
      /* this.paciente = resp.data.paciente */
      this.setPaciente(resp.data.paciente);
      this.getDonaciones(resp.data.paciente._id);
    })
  }

  setPaciente(data){
    for (const key in this.paciente) {
      if (data[key] == undefined) {
        this.paciente[key] = ''
      }else{
        this.paciente[key] = data[key]
      }
    }
  }

  dataSerologia(){
    this._banco.getEstudiosbyIdlLab('61383eeb7a528c514406d4ef').subscribe((resp:any)=>{
      this.setSerologia(resp['data']);
    })
  }

  setSerologia(resp){
    this.elementos = resp.valoresDeReferencia
    this.elementos.forEach(element => {
      this.metodos.push(element.metodo)
    })
  }

  getDonaciones(id){
    let idPaciente = {
      idPaciente:id
    }
    this._banco.numerodeDonadores().subscribe((resp:any)=>{
      this.setNumeroDonadores(resp['data']);
    })
    this._banco.numeroDonacionesPaciente(idPaciente).subscribe((resp:any)=>{
      this.setNumeroDonacionesPaciente(resp['data'])
    })
  }

  setNumeroDonadores(numero){
    this.donaciones = numero
  }
  setNumeroDonacionesPaciente(numero){
    this.donacionesPaciente=numero
  }

  validarSerologia(form: NgForm){
    let fecha;
    fecha = moment().format('l'); 
    let regreso = serologia_disp(fecha, form.value);
    this.respuesta = regreso;
    
  }

  //loading
  pintLoadingScreen() {
    const sectionSpinner = document.querySelector('#sppiner-section');
    sectionSpinner.classList.add('display-block');
    console.log("toy funcionando weeeee");
    
  }

  removeLoadingScreen() {
    const sectionSpinner = document.querySelector('#sppiner-section');
    sectionSpinner.classList.remove('display-block');
    console.log('dejo de funcionar');
    
  }

  enviar(resp){
    this.pintLoadingScreen()
    if(resp.diferir){
      let fecha = new Date(resp.diferido[0].fechafinal);
      let dif ={
          idbanco:this.idBanco,
          proceso:resp.proceso, 
          estatus:'DIFERIDO',
          motivo:resp.diferido[0].motivo,
          rechazo:resp.diferido[0].tiempoDiferir,
          fechaRechazo:fecha
      }
      this._banco.diferirDisponente(dif).subscribe((resp:any)=>{
       /* console.log(resp); */
       this.removeLoadingScreen()
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'EL DISPONENTE SE DIFERIO CORRECTAMENTE',
        })
        this.route.navigate([ `/dashboard`]);
      })
    }else{
      this.setSerologiaData(resp.sero);
    }
  }

  setSerologiaData(obtenido){
    let id = {
        idbancosangre:this.idBanco,
        proceso:'finalizado'
      }
    let clave = ''
    this.sero.idbancodesangre = this.idBanco
    this.sero.idlaboratorio='61383eeb7a528c514406d4ef'
    this.sero.cedula='xxxxxxxxxxxx'
    this.sero.metodo=this.metodos
    this.sero.quimico='xxxxxxxxxxxx'
    this.sero.tecnico='xxxxxxxxxxxx'
    this.sero.proceso='FINALIZADO'
    this.sero.obtenidos.push(obtenido)
    clave = this.paciente.curp.concat('/',this.donaciones.toString(),'/',this.donacionesPaciente.toString())
    this.sero.productos = [
      {
            productosFI: '61611aad8598bf035496e74f',
            fecha_caducidad:  this.fechas_caducidad(35),
            lote:  '1',
            proveedor: '615e6f5c47e774260875cbbe',
            factura: '0001',
            lote_unitario : clave.concat('/','CE') ,
            costoReal: '350',
            precioVenta:'2500'
      },
      {
            productosFI: '61611abf8598bf035496e750',
            fecha_caducidad:  this.fechas_caducidad(720),
            lote:  '1',
            proveedor: '615e6f5c47e774260875cbbe',
            factura: '0001',
            lote_unitario : clave.concat('/','CP') ,
            costoReal: '350',
            precioVenta:'2500'
      },
      {
            productosFI: '61611ac88598bf035496e751',
            fecha_caducidad:  this.fechas_caducidad(1080),
            lote:  '1',
            proveedor: '615e6f5c47e774260875cbbe',
            factura: '0001',
            lote_unitario : clave.concat('/','PL') ,
            costoReal: '350',
            precioVenta:'2500'
      }
    ]
    /* console.log(this.sero.productos) */
     this._banco.datosSerologia(this.sero).subscribe((resp:any)=>{
     /* console.log(resp); */
      if (resp.ok) {
              this._banco.cambiarProceso(id).subscribe((resp:any)=>{
             // console.log(resp);
             this.removeLoadingScreen();
              Swal.fire({
                icon: 'success',
                title: '',
                text: 'RECUERDA MANTENER LOS PRODUCTOS EN REFRIGERACIÓN',
              })
              this.route.navigate([ `/dashboard`]);
            })

      }
    })     
  }

  fechas_caducidad( duracion ){
    const fecha = new Date()
    const agregarTiempo = duracion * 86400
    
    const fecha_final = fecha.setSeconds(agregarTiempo)
    const piv_fecha = new Date(fecha_final)
    /* console.log(piv_fecha) */
    return piv_fecha
  }
}
