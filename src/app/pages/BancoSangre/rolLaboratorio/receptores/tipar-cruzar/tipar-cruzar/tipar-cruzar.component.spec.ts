import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiparCruzarComponent } from './tipar-cruzar.component';

describe('TiparCruzarComponent', () => {
  let component: TiparCruzarComponent;
  let fixture: ComponentFixture<TiparCruzarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TiparCruzarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TiparCruzarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
