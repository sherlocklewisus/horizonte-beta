import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';
import swal from 'sweetalert'
@Component({
  selector: 'app-tipar-cruzar',
  templateUrl: './tipar-cruzar.component.html',
  styleUrls: ['./tipar-cruzar.component.css']
})
export class TiparCruzarComponent implements OnInit {

  public paciente = {
    nombrePaciente: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    curp: "",
    edad: 0,
    genero: "",
    _id:"",
    callePaciente: "",
    fechaNacimientoPaciente:"",
    estadoPaciente: "",
    paisPaciente: "",
    telefono: "",
    receptor:"",
    tipoDeSangre:"",
  }
  public gruposanguineo = {
    tiposangre: '',
    sede: '', 
    idbancodesangre:''
  }
  public receptor = {
    idbancosangre:''
  }

  public idCensur;
  public pedido = {
    estudios:[{
      cantidad: 0,
      idProducto: '',
      precio: 0,
      producto: ''
    }],
    fecha: '',
    pedido: '',
    sede_banco: '',
    status: '',
    totalCompra: 0,
    vendedor: '',
    _id: ''
  }

  public concentrados = [];
  public productos = [];
  public donantes = [];
  public donantesTipoSangre = []
  public habilitar = false;
  constructor(private _bancoSangre: BancoService,
              private _router:Router) { }

  ngOnInit(): void {
    this.obtenerReceptor();
    this.getIdCensur();
  }

  obtenerReceptor(){
    this.paciente = JSON.parse(localStorage.getItem('receptor'));
  }

  getIdCensur(){
    this.idCensur = localStorage.getItem('idCensur'); 
    this.getPedidoReceptor(this.idCensur);
    this.gruposanguineo.idbancodesangre = this.idCensur
  }

  getPedidoReceptor(id){
    this.receptor.idbancosangre = id;
    this._bancoSangre.getPedidoReceptor(this.receptor).subscribe((resp:any) =>{
      this.setPedido(resp.data);
      console.log( "data pedido",resp.data) 
    })
  }

  setPedido(pedido){
    this.pedido = pedido.pedido;
    pedido.pedido.estudios.forEach(element => {
      /* console.log(element.cantidad) */
      for (let i = 0; i < element.cantidad; i++) {
        /* console.log(element) */
        this.productos.push(element)      
      }
      
      console.log("productos",this.productos) 
      /*       if(element.producto == "CONCENTRADOS ERITROCITARIOS"){
        this.habilitar= true;
        this.concentrados.push(element)
      }else{
        this.productos.push(element)
      } */
    });
    console.log(this.productos);
    this.productos.forEach(element => {
      this.donantesTipoSangre.push('')
    })
  }

  guardar(){
    /* console.log(this.pedido._id); */
    swal("Se agregaron los productos",{icon:"success"})
    this._router.navigateByUrl('/hojaservicios/receptor/'+this.pedido.pedido)
  }

  disponentes(grupo: any, index:any){
    this.gruposanguineo.tiposangre = grupo
    this.gruposanguineo.sede = 'TLYC01'
    this._bancoSangre.postgruposanguineo(this.gruposanguineo).subscribe((resp:any) =>{
      this.donantes = resp.data
      //console.log(this.donantes)
      /* this.donantesTipoSangre=[]; */
      this.getDonadores(this.donantes, index)
    })
    /* console.log(grupo) */
  }

  getDonadores(donadores: any, index:any){
    const array2 = []
    const arreglo = []
    console.log(donadores);
    
    donadores.forEach(element => {
        array2.push(element)
    });

    for (let i = 0; i < array2.length; i++) {
      for (let x = 0; x < array2[i].length; x++) {
        const dona = array2[i][x]
        const element = array2[i][x].paciente
        arreglo.push(element)
      }
    }
    console.log(index);
    console.log(this.donantesTipoSangre[0]);
    
    this.donantesTipoSangre[index]=arreglo
    /* console.log(dona) */
    console.log(this.donantesTipoSangre);
    
  }
}
