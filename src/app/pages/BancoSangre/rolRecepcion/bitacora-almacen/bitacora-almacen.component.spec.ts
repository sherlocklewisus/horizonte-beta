import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraAlmacenComponent } from './bitacora-almacen.component';

describe('BitacoraAlmacenComponent', () => {
  let component: BitacoraAlmacenComponent;
  let fixture: ComponentFixture<BitacoraAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
