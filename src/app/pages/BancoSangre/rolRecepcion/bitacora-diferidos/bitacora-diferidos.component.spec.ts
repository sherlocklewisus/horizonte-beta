import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraDiferidosComponent } from './bitacora-diferidos.component';

describe('BitacoraDiferidosComponent', () => {
  let component: BitacoraDiferidosComponent;
  let fixture: ComponentFixture<BitacoraDiferidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraDiferidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraDiferidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
