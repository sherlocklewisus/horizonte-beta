import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoexclusionComponent } from './autoexclusion.component';

describe('AutoexclusionComponent', () => {
  let component: AutoexclusionComponent;
  let fixture: ComponentFixture<AutoexclusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoexclusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoexclusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
