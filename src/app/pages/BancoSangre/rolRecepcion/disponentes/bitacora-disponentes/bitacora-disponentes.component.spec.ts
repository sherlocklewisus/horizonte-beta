import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraDisponentesComponent } from './bitacora-disponentes.component';

describe('BitacoraDisponentesComponent', () => {
  let component: BitacoraDisponentesComponent;
  let fixture: ComponentFixture<BitacoraDisponentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraDisponentesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraDisponentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
