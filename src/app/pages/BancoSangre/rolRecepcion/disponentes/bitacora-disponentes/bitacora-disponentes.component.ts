import { Component, OnInit } from '@angular/core';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { Subject } from 'rxjs';
import swal from 'sweetalert'


@Component({
  selector: 'app-bitacora-disponentes',
  templateUrl: './bitacora-disponentes.component.html',
  styleUrls: ['./bitacora-disponentes.component.css']
})
export class BitacoraDisponentesComponent implements OnInit {

  public pacientes2:any[] = [];
  public totalpaciente:string;
  public pagina = 0;
  public  filtropacientes = '';
  public filterPost = '';

  private unsubcribe$ = new Subject<void>();

  constructor(private _pacienteService: PacientesService) { }

  ngOnInit(): void {
    this.obtenerPacientes();
    this.focusSearchPatients();
  }

  obtenerPacientes() {
    this._pacienteService.getPacientesAll()
    .subscribe( (data: any) => {
      if( data['message']   === 'No hay pacientes' ) {
        swal("Aun no hay pacientes",{icon:"warning"})
        return;
      }
       this.pacientes2 = data.data;
       this.totalpaciente = data.data.results;
    });
  }

  typeSearchPatients(){
    if( this.filtropacientes.length > 3 ){
      this._pacienteService.getPacientePorNombre( this.filtropacientes )
      .subscribe(  data => {
        this.pacientes2 = data['data'];
      });
    }else if(this.filtropacientes == ''){
      this.obtenerPacientes();
      this.focusSearchPatients();
    }
  }

  focusSearchPatients(){
    const input: HTMLInputElement = document.querySelector('#busquedaPaciente');
    input.select();
  }


  paginaAnterior(){
    this.pagina = this.pagina - 8;
    this._pacienteService.getPacientes( this.pagina)
    .subscribe( (data: any) => {
      this.pacientes2 = data.users;
    })
  }


  siguentePagina(){
    this.pagina += 8;
    this._pacienteService.getPacientes( this.pagina)
    .subscribe((data: any) => {
      this.pacientes2= data.users;
    })
  }

  ngOnDestroy(){
    this.unsubcribe$.next();
    this.unsubcribe$.complete();
  }
}
