import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroDisponentesComponent } from './registro-disponentes.component';

describe('RegistroDisponentesComponent', () => {
  let component: RegistroDisponentesComponent;
  let fixture: ComponentFixture<RegistroDisponentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroDisponentesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroDisponentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
