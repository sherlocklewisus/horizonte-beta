import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerDisponenteComponent } from './ver-disponente.component';

describe('VerDisponenteComponent', () => {
  let component: VerDisponenteComponent;
  let fixture: ComponentFixture<VerDisponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerDisponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerDisponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
