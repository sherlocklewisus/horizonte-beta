import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

@Component({
  selector: 'app-ver-disponente',
  templateUrl: './ver-disponente.component.html',
  styleUrls: ['./ver-disponente.component.css']
})
export class VerDisponenteComponent implements OnInit {

  public id;
  
  public paciente = {
      nombrePaciente:"",
      apellidoPaterno:"",
      apellidoMaterno: "",
      curp:"",
      telefono:0,
      consultas: 0,
      _id:"",
      fechaRegistro:Date,
      genero:"",
      direccion: {
        estadoPaciente:"",
        callePaciente:"",
        paisPaciente:"",
        cp:""
      },
      contactoEmergencia1Nombre:"",
      contactoEmergencia1Edad:"",
      correo: "",
      cp:"",
      edad:"",
      paquetes : [],
      membresiaActiva:false,
      receptor: "",
      tipoDeSangre: "",
      disponente: "",
  }
  public paquetes: any [] = [];
  public lugar = false;
  public paciente2 = {
    nombrePaciente: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    curp: '',
    edad: '',
    genero: '',
    _id:'',
    callePaciente: '',
    fechaNacimientoPaciente:'',
    estadoPaciente: '',
    paisPaciente: '',
    telefono: '',
    receptor: '',
    tipoDeSangre: '',
    disponente: '',
    fechaRegistro:Date,
    consultas: 0,
  }

  constructor(private activatedRoute: ActivatedRoute,
              private _router:Router,
              private _pacienteService: PacientesService,
              private _banco:BancoService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.obtenerPaciente();
    this.paquete(this.id);
  }

  obtenerPaciente(){
    this._pacienteService.getPacienteBtID(this.id).subscribe( (data:any) => {
      this.paciente = data.paciente;
      this.setPaciente2(data.paciente);
      this.guardarLocalStorage();
    });
  }

  setPaciente2(data){
    for (const key in this.paciente2) {
      if (data[key] == undefined) {
        this.paciente2[key] = ''
      }else{
        this.paciente2[key] = data[key]
      }
    }
  }

  guardarLocalStorage(){
    if(  localStorage.getItem('paciente') ){
      localStorage.removeItem('paciente');
    }
    if(  this.paciente.paquetes.length >= 1 || this.paciente.membresiaActiva == true ){
        this.paciente.membresiaActiva = true;
    }
    localStorage.setItem('paciente', JSON.stringify(this.paciente));
  }

  paquete(id){
    this._pacienteService.getPaquetePaciente(id).subscribe(
      (data: any) => {
          this.paquetes = data['data'];
      },
      err => {
        console.log(<any>err);
    });  
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    if(servicio == 'Donaciones'){
      let resp ={
        idpaciente: this.paciente._id,
        disponente:servicio
      }
      this.lugar = true;
      this._banco.agregarProducto(resp).subscribe((resp:any)=>{
      })
      
    }else{
      this._router.navigate([ `/serviciosInt/${servicio}`]);
    }
  }

  actualizarLugar(lugar){
    let resp ={
      idpaciente: this.paciente._id,
      lugarorigen:lugar
    }
    this._banco.agregarLugar(resp).subscribe((resp:any)=>{
      this._router.navigate([ `/autoexclusion/${this.paciente._id}`]);
    })    
  }

  pasar(){
    this._router.navigate([ `/historico/disponente/bs/${this.id}`]);
  }

}
