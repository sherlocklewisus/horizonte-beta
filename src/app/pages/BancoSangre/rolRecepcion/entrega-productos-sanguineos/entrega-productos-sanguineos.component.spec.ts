import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregaProductosSanguineosComponent } from './entrega-productos-sanguineos.component';

describe('EntregaProductosSanguineosComponent', () => {
  let component: EntregaProductosSanguineosComponent;
  let fixture: ComponentFixture<EntregaProductosSanguineosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntregaProductosSanguineosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregaProductosSanguineosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
