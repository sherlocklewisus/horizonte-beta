import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoDisponenteBSComponent } from './historico-disponente-bs.component';

describe('HistoricoDisponenteBSComponent', () => {
  let component: HistoricoDisponenteBSComponent;
  let fixture: ComponentFixture<HistoricoDisponenteBSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricoDisponenteBSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoDisponenteBSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
