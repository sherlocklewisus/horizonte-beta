import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';

@Component({
  selector: 'app-historico-disponente-bs',
  templateUrl: './historico-disponente-bs.component.html',
  styleUrls: ['./historico-disponente-bs.component.css']
})
export class HistoricoDisponenteBSComponent implements OnInit {

  public paciente = {
    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
    direccion: {
      estadoPaciente:"",
      callePaciente:"",
      paisPaciente:"",
      cp:""
    },
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    correo: "",
    cp:"",
    edad:"",
    paquetes : [],
    membresiaActiva:false,
    receptor: "",
    tipoDeSangre: "",
    disponente: "",
  }

  public idCensur = '';

  constructor(public _route:ActivatedRoute,
              public _banco:BancoService) { }

  ngOnInit(): void {
    this.idCensur = this._route.snapshot.paramMap.get('id')
    this.obtenerHistorico();
  }

  obtenerHistorico(){
    this._banco.getHistoricoLab(this.idCensur).subscribe((resp:any)=>{
      /* console.log(resp);
       */
    })
  }

  

}
