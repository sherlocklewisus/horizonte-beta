import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosReceptorComponent } from './documentos-receptor.component';

describe('DocumentosReceptorComponent', () => {
  let component: DocumentosReceptorComponent;
  let fixture: ComponentFixture<DocumentosReceptorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentosReceptorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosReceptorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
