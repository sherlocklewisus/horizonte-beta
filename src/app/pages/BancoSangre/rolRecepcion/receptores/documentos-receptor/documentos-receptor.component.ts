import { Component, OnInit } from '@angular/core';
import { BancoService } from 'src/app/services/bancodeSangre/banco.service';

@Component({
  selector: 'app-documentos-receptor',
  templateUrl: './documentos-receptor.component.html',
  styleUrls: ['./documentos-receptor.component.css']
})
export class DocumentosReceptorComponent implements OnInit {

  public document ={
    lastModified: 1623721377330,
    name: "",
    size: 167511,
    type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    webkitRelativePath: ""
  }
  public paciente = {
    nombrePaciente: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    curp: "",
    edad: 0,
    genero: "",
    _id:"",
    callePaciente: "",
    fechaNacimientoPaciente:"",
    estadoPaciente: "",
    paisPaciente: "",
    telefono: "",
    receptor:"",
  }

  public receptor ={
    idbancosangre: ''
  }

  public documentosReceptor = [];

  constructor(private _bancoSangre: BancoService) { }

  ngOnInit(): void {
    this.obtenerReceptor();
    this.getPedidoReceptor();
  }

  //LOADING
  pintLoadingScreen() {
    const sectionSpinner = document.querySelector('#sppiner-section');
    sectionSpinner.classList.add('display-block');
    console.log("toy funcionando weeeee");
    
  }

  removeLoadingScreen() {
    const sectionSpinner = document.querySelector('#sppiner-section');
    sectionSpinner.classList.remove('display-block');
    console.log('dejo de funcionar');
    
  }


  obtenerReceptor(){
    this.paciente = JSON.parse(localStorage.getItem('receptor'));
  }

  getPedidoReceptor(){
    this.receptor.idbancosangre = localStorage.getItem('idCensur');
    this._bancoSangre.getPedidoReceptor(this.receptor).subscribe((resp:any) =>{
      this.setPedido(resp.data);
    })
  }
  
  setPedido(pedido){
    pedido.receptor.documentosreceptor.forEach(element => {
      /* console.log(element.path); */
      let cadena = ''
      cadena=element.path.slice(8);;
      this.documentosReceptor.push(cadena);
    });
    /* console.log(this.documentosReceptor); */
    
  }

  // DROPZONE
  files: File[] = [];

  onSelect(event) {
    this.files.push(...event.addedFiles);
    /* console.log(event); */
    
  }
  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

}
