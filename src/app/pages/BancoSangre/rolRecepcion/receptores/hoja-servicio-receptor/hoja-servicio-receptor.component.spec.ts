import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaServicioReceptorComponent } from './hoja-servicio-receptor.component';

describe('HojaServicioReceptorComponent', () => {
  let component: HojaServicioReceptorComponent;
  let fixture: ComponentFixture<HojaServicioReceptorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaServicioReceptorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaServicioReceptorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
