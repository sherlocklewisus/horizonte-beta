import { Component, OnInit } from '@angular/core';
import { BancoService } from '../../../../services/bancodeSangre/banco.service';
import { preRegistro} from '../../../../functions/bancoSangre'
import * as moment from 'moment';

@Component({
  selector: 'app-receptores',
  templateUrl: './receptores.component.html',
  styleUrls: ['./receptores.component.css']
})
export class ReceptoresComponent implements OnInit {
  public  filtropacientes = '';
  public pedidos=[];

  /* public form = {
    talla:1.70,
    peso:49,
    imc:18,
    temp:36,
    sistolica:175,
    diastolica:95,
    multiples:'',
    recientes:''
  } */

  /* public form = {
    nacionalidad:'NO',
    viajes:'NO',
    enfermos_hepatitis:'NO',
    vih_deteccion:'NO',
    hepatitisb_deteccion:'NO',
    hepatitisc_deteccion:'NO',
    enfermedad_de_transmision:'NO',
    odinofagia:'NO',
    alcohol_consumido:'NO',
    medicamento_regular:'NO',
    consume_etretinato:'NO',
    tratamiento_dental:'NO',
    cirugia_mayor:'NO',
    cirugia_menor:'NO',
    alergias:'NO',
    inmunizaciones:'NO',
    cardiopatias:'NO',
    efermedades_renales:'NO',
    coagulopatias:'NO',
    cancer:'SI',
    neoplasia_hematologica:'NO',
    anemia:'NO',
    infecciones_bacteo:'NO',
    lepra:'NO',
    paludismo:'NO',
    brucelosis:'NO',
    diabetes:'NO',
    hipertension_arte:'NO',
    tuberculosis:'NO',
    epilepsia:'NO',
    hepatitis:'NO',
    icteria:'NO',
    transtormental:'NO',
    toxoplasmosis:'NO',
    transplantes:'NO',
    covid:'NO',
    otros_patologicos:'NO',
    ultima_relacion:'NO',
    gestas:'NO',
    partos:'NO',
    cesareas:'NO',
    abortos:'NO',
    ultimo_parto:'NO',
    ultimo_aborto:'NO',
    isoinmunizacioes:'NO',
    globulina:'NO',
    transfusiones_prev:'NO',
    pago_por_donar_sangre:'NO',
    uso_drogas:'NO',
    heterosecual_promiscuo:'NO',
    homoxesual:'NO',
    bisexual:'NO',
    pago_por_relaciones:'NO',
    contacto_con_hemofilicos:'NO',
    instituciones_pentales:'NO',
    acupunturas:'NO',
    tatuajes:'NO',
    enfermedades_itc:'NO',
    parejas_sexuales:'NO',
    ultimo_año_de_parejas:'NO',
    ultimos_cinco_años_de_parejas:'NO',
    tos_disnea:'NO',
    perdida_de_peso:'NO',
    diarrea_cronica:'NO',
    diaforesis_cronica:'NO',
    diaforesis_profusa:'NO',
    astenia:'NO',
    adenomegalias:'NO',
    herpes_mucocutaneo:'NO',
    fiebre_continua:'NO',
    odinofagia_diez_dias:'NO',
    sindrome_diarreico:'NO',
    isotretinoina_ultimo_mes:'NO',
    covid_ultimo_mes:'NO',
    medicamentos_ultima_semana:'NO',
    infecciones_agudas_ultima_semana:'NO',
    fiebre_ultimos_dos_dias:'NO',
    ejercicio_intenso_ultimos_dos_dias:'NO',
    ayuno_ultimos_dos_dias:'NO',
    vigilia_ultimos_dos_dias:'NO',
    ingesta_de_alcohol_dos_dias:'NO',
    medicamento_acitretina:'NO',
    medicamento_tamoxifeno:'NO',
    medicamento_dutasterida:'NO',
    medicamento_finasterida:'NO',
    medicamento_isotretinoina:'NO',
    medicamento_tertraciclina:'NO',
    medicamento_tretinoina:'NO',
    medicamento_talidoida:'NO',
    medicamento_acido_acetil_salicilico:'NO',
    medicamento_clopidogrel:'NO',
    medicamento_diflunisal:'NO',
    medicamento_fenilbutazona:'NO',
    medicamento_meloxicam:'SI',
    medicamento_nabumetona:'NO',
    medicamento_naproxeno:'NO',
    medicamento_sulindaco:'NO',
    medicamento_tenoxicam:'NO',
    medicamento_aceclofenaco:'NO',
    medicamento_acetamicin:'NO',
    medicamento_acido_mefenamico:'NO',
    medicamento_diclofenaco:'NO',
    medicamento_dexibuprofen:'NO',
    medicamento_flubiprofeno:'NO',
    medicamento_ibuprofeno:'NO',
    medicamento_indometacina:'NO',
    medicamento_ketoprofeno:'NO',
    medicamento_ketorolaco:'NO',
  } */

  /* public form = {
    preguntauno:'SI',
    preguntados:'NO',
    preguntatres:'NO',
    preguntacuatro:'NO',
    preguntacinco:'NO',
    preguntaseis:'SI',
    preguntasiete:'NO',
    preguntaocho:'NO',
    preguntanueve:'NO',
    preguntadiez:'NO',
    preguntaonce:'NO',
    preguntadoce:'SI',
  } */
  public fecha;

  public resultado:any;

  constructor(private _banco: BancoService) { }

  ngOnInit(): void {
    this.getOrdenes();
    /* this.validaciones(); */
  }

  typeSearchPatients(){}

  getOrdenes(){
    this._banco.getOrdenPedido().subscribe((resp:any) =>{
      this.setOrdenes(resp.data)
    })
  }

  setOrdenes(resp){
    resp.forEach(element => {
      if(element.pedido != undefined){
        this.pedidos.push(element);
      }      
    });
  }

  /* validaciones(){
    this.fecha = moment().format('l'); 
    let regreso = historiaClinica(this.fecha, this.form);
    console.log(regreso);
    
  } */
}
