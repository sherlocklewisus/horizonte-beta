import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePedidosSedesComponent } from './detalle-pedidos-sedes.component';

describe('DetallePedidosSedesComponent', () => {
  let component: DetallePedidosSedesComponent;
  let fixture: ComponentFixture<DetallePedidosSedesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallePedidosSedesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePedidosSedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
