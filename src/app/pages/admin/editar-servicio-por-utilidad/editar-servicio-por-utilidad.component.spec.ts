import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarServicioPorUtilidadComponent } from './editar-servicio-por-utilidad.component';

describe('EditarServicioPorUtilidadComponent', () => {
  let component: EditarServicioPorUtilidadComponent;
  let fixture: ComponentFixture<EditarServicioPorUtilidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarServicioPorUtilidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarServicioPorUtilidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
