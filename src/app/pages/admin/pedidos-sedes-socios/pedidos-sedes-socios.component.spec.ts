import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosSedesSociosComponent } from './pedidos-sedes-socios.component';

describe('PedidosSedesSociosComponent', () => {
  let component: PedidosSedesSociosComponent;
  let fixture: ComponentFixture<PedidosSedesSociosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidosSedesSociosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosSedesSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
