import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioCanalComponent } from './socio-canal.component';

describe('SocioCanalComponent', () => {
  let component: SocioCanalComponent;
  let fixture: ComponentFixture<SocioCanalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocioCanalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioCanalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
