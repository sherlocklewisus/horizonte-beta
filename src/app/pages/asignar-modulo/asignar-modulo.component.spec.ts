import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarModuloComponent } from './asignar-modulo.component';

describe('AsignarModuloComponent', () => {
  let component: AsignarModuloComponent;
  let fixture: ComponentFixture<AsignarModuloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignarModuloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarModuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
