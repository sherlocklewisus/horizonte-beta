import { Component, OnInit } from '@angular/core';
import { WsLoginService } from 'src/app/services/sockets/chat/ws-login.service';
import {  ConsultaService } from '../../../../services/consultas/consulta/consulta.service';

import * as moment from 'moment';
import jsPDF from 'jspdf';
import autoTable, { Column } from 'jspdf-autotable';

@Component({
  selector: 'app-bitacora-consulta-gral',
  templateUrl: './bitacora-consulta-gral.component.html',
  styleUrls: ['./bitacora-consulta-gral.component.css']
})
export class BitacoraConsultaGralComponent implements OnInit {

  public consultas:[] =[];
  public fecha:String;

  public nombreDoctror="";

  constructor(
    private _consultaService: ConsultaService,
    private _wsLognService: WsLoginService
  ) { }

  ngOnInit(): void {
    this.fecha = moment().format('L');
    this.obtenerConsultasMedico();
    

    this._wsLognService.escucharBitacoraDoctor()
    .subscribe(arg => {
      if(arg){
        this.obtenerConsultasMedico();
      }
    });
    
  }


  obtenerNombreDoctor(){
    this.nombreDoctror = JSON.parse( localStorage.getItem('usuario') ).nombre
    // console.log( this.nombreDoctror );
  }


  obtenerConsultasMedico(){
    this.obtenerNombreDoctor();
    this._consultaService.verConsultasMedico(  this.nombreDoctror )
    .subscribe( (data) => {
        console.log(data);
        this.consultas = data['data'].reverse();
        console.log(this.consultas);
        
    } )
  }


  cambiarEstado(id: string) {

    let estado = {
      status:'Medico'
    }

    this._consultaService.cambiarEstadoConsulta(id, estado)
    .subscribe(data => console.log(data));
    
  }


  imprimirBitacora(){
// 'p', 'mm', 'a4'
    const doc:any = new jsPDF('l', 'cm', 'legal');

    doc.autoTable({orientation:'p', html: '#bitacora'});

    doc.save( `Bitácora medicina general${this.fecha}.pdf`);
  }


}
