import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaEvolucionHistoricoComponent } from './hoja-evolucion-historico.component';

describe('HojaEvolucionHistoricoComponent', () => {
  let component: HojaEvolucionHistoricoComponent;
  let fixture: ComponentFixture<HojaEvolucionHistoricoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaEvolucionHistoricoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaEvolucionHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
