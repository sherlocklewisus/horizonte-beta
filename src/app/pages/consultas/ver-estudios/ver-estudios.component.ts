
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert';

import { RecetasService } from 'src/app/services/recetas/recetas.service';
import { getCarritoStorage } from '../../../functions/pacienteIntegrados';
import { gaurdarCotizacion } from '../../../functions/storage.funcion';
import CarritoMembresia from 'src/app/classes/carritoMembresia/carritoMembresia.class';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
import Carrito from 'src/app/classes/carrito/carrito.class';

@Component({
  selector: 'app-ver-estudios',
  templateUrl: './ver-estudios.component.html',
  styleUrls: ['./ver-estudios.component.css']
})
export class VerEstudiosComponent implements OnInit {


  public id: string;

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };


  public paciente = {

    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    curp:"",
    telefono:0,
    consultas: 0,
    _id:"",
    fechaRegistro:Date,
    genero:"",
      estadoPaciente:"",
      callePaciente:"",
      paisPaciente:"",
      cpPaciente:"",
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    correo: "",
    cp:"",
    edad:"",
    paquetes : [],
    membresiaActiva:false
  }

  public estudios:[] =[];
  
  public totalSinMembresia = 0;
  public totalConMembresia = 0;
  public totalCarritoMembresia: Number;

  constructor(
    private _recetaService: RecetasService,
    private _route: ActivatedRoute,
    ) {  }
    
    ngOnInit(): void {
      this.id = this._route.snapshot.paramMap.get('id');
      
      this.obtenerReceta();
  }


  
  obtenerCarrito(){

      
    let carritoSinMembresia = new Carrito();
    this.carrito = carritoSinMembresia.obtenerSotorageCarrito();

      // console.log( this.carrito );
    this.totalSinMembresia = this.carrito.totalSin;
    this.totalCarritoMembresia= this.carrito.totalCon;

  }


  obtenerReceta(){
    this._recetaService.verRecetaConConsulta(  this.id )
    .subscribe(  data => {

         
         this.paciente.nombrePaciente = data['data']['idPaciente'].nombrePaciente;
        this.paciente.apellidoPaterno = data['data']['idPaciente'].apellidoPaterno;
        this.paciente.apellidoMaterno = data['data']['idPaciente'].apellidoMaterno;
        this.paciente.cp = data['data']['idPaciente'].cpPaciente;
        this.paciente.estadoPaciente = data['data']['idPaciente'].estadoPaciente;
        this.paciente.paisPaciente = data['data']['idPaciente'].paisPaciente;
        this.paciente.callePaciente = data['data']['idPaciente'].callePaciente;
        this.paciente.cpPaciente = data['data']['idPaciente'].cpPaciente;
        this.paciente.edad = data['data']['idPaciente'].edad;
        this.paciente.correo = data['data']['idPaciente'].correoPaciente;
        this.paciente.curp = data['data']['idPaciente'].curp;
        this.paciente._id = data['data']['idPaciente']._id;
        this.paciente.paquetes = data['data']['idPaciente'].paquetes;
        this.paciente.contactoEmergencia1Nombre = data['data']['idPaciente'].contactoEmergencia1Nombre;
        this.paciente.contactoEmergencia1Edad = data['data']['idPaciente'].contactoEmergencia1Edad;
        this.paciente.genero = data['data']['idPaciente'].genero;
        this.paciente.fechaRegistro = data['data']['idPaciente'].fechaRegistro;
        this.estudios = data['data']['estudios'];
        // console.log( this.paciente );
        /* this.setMembresia( data['data']['idPaciente']['membresiaActiva'] );
        this.setLocalStoragePaciente( data['data']['idPaciente'] ); */
        this.guardarLocalStorage();

    });
  }

  setMembresia( status ){
    this.paciente.membresiaActiva = status;
    this.obtenerCarrito();
  }

  guardarLocalStorage(){

    if(  localStorage.getItem('paciente') ){
  
      localStorage.removeItem('paciente');
    }
    if(  this.paciente.paquetes.length >= 1 || this.paciente.membresiaActiva == true ){
        this.paciente.membresiaActiva = true;
    }
    
    localStorage.setItem('paciente', JSON.stringify(this.paciente));
    console.log(this.paciente);
    
  }

  setLocalStoragePaciente( paciente ){
    /* this.paciente = paciente
    this.guardarLocalStorage(this.paciente); */
    /* localStorage.setItem('idPaciente', JSON.stringify(paciente)) */
    /* const pacienteStorage = new PacienteStorage();
    const result  =  pacienteStorage.setPacienteStorage( idPaciente ); */
    // if( result  ){
    //   console.log("paciente agregado");
    // }
  }



  alertcomparasion( ev, precioPublicoOtros, precioMembresia, item2:any ){
    let precioSinTrim  =  precioPublicoOtros.replace('$', '');
     let precioSinComaPublico = precioSinTrim.replace(',', '');
     let precioMemTrim  =  precioMembresia.replace('$', '');
     let precioMemComaMembresia = precioMemTrim.replace(',', '');

     swal("Con membresia ahorras:"+(precioSinComaPublico - precioMemComaMembresia),{icon:"success"});
     this.agregarCarrito(ev, item2);
  }
  
  
  //carrito

  agregarCarrito( event, item: any ) {

    
    let carrito = new Carrito();
    /* swal("Con membrecia ahorras:" +(this.carrito.totalSin - this.carrito.totalCon) ,{icon:"success"}); */
    this.carrito = carrito.agregarItem(  event, item );
    this.obtenerCarrito()
  }

  eliminar( id ) {

    let carrito = new Carrito();
    carrito.eliminar( id );

    this.obtenerCarrito();

    // this.obtenerCarritoStorage();
    
  }


  
}
