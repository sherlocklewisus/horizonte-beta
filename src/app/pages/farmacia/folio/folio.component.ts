import { Component, OnInit } from '@angular/core';
import { MedicamentosService } from 'src/app/services/farmacia/medicamentos.service';
import{ ActivatedRoute, Router} from '@angular/router';
import { subscribeOn } from 'rxjs/operators';
import swal from 'sweetalert'

@Component({
  selector: 'app-folio',
  templateUrl: './folio.component.html',
  styleUrls: ['./folio.component.css']
})
export class FolioComponent implements OnInit {

  
public btnGuardar = true;

  
  public medicamento = {
    nombreComercial:"",
    nombreDeSalOsustanciaActiva:"",
    presentacio:"",
    contenidoFrasco:"",
    viaDeAdministracion:"",
    lote:"",
    fechaCaducidad:"",
    laboratorio:"",
  }


  constructor( public _agregarmedicamentoservice:MedicamentosService,
    public _router:Router){

   }

  ngOnInit(): void {
  }

  

enviar(){

  console.log(this.medicamento)
  this.medicamento.nombreComercial.trim()
  this.medicamento.contenidoFrasco.trim()
  this.medicamento.fechaCaducidad.trim()
  this.medicamento.laboratorio.trim()
  this.medicamento.lote.trim()
  this.medicamento.nombreDeSalOsustanciaActiva.trim()
  this.medicamento.presentacio.trim()
  this.medicamento.viaDeAdministracion.trim()
  if (this.medicamento.nombreComercial=="") {
    swal("Completa los campos",{icon:"warning"}) 
    /* alert('Completa los campos'); */

  return }

  this._agregarmedicamentoservice.agragarmedicamentos(this.medicamento).subscribe (
    (data:any) => {console.log(data)
      if(data.ok){
        swal("Medicamento guardado",{icon:"success"})
        /* alert('Medicamento guardado'); */
        this._router.navigateByUrl('/dashboard');
      }
    }
  )
}


validarNombre(){

  this._agregarmedicamentoservice.validarMedicamento(this.medicamento.nombreComercial)
  .subscribe((data) => {
    console.log(data)
    if (data['data'][0].length > 0){
      swal("Ese medicamento ya existe",{icon:"error"})
      /* alert('Ese medicamento ya existe'); */
    }else {
      this.btnGuardar = false;
    }
    })
    }
    
}

