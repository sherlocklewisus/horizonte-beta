import { Component, OnInit } from '@angular/core';
import {MedicamentosService} from '../../../services/farmacia/medicamentos.service'
import  swal from 'sweetAlert';


@Component({
  selector: 'app-listado-medicamentos',
  templateUrl: './listado-medicamentos.component.html',
  styleUrls: ['./listado-medicamentos.component.css']
  
})
export class ListadoMedicamentosComponent implements OnInit {

  public medicamentos: any [];
  public pagina = 0;
  public total: string;
  public buscar;
  constructor( private _medicamentoService:MedicamentosService) { }

  ngOnInit(): void {
    this.listarMedicamentos()
    //this.typeSearchMedicamentos();
  }

  listarMedicamentos( ){
    this._medicamentoService.obtenermedicamento().subscribe(
      (data) => {
        this.medicamentos=data['data'];
        this.total=data['data'].results;
        console.log(data);
      } 
      ) 
     }

     eliminarMedicamento( id ){
      //  console.log( id)

      swal ('¿Estás seguro que deseas eliminar este medicamento?', '', 'error', { buttons: {
          cancel: {
            text: 'Cancelar',
            value: null,
            visible: true
          },
          confirm: {
            text: "Aceptar",
            value: true,
            visible: true
          }          
      }
        
      })
      .then( value => {
        if(value === null){
          return
        }else {
          this._medicamentoService.eliminarMedicamento(id).subscribe(
            data => console.log(data))
            this.listarMedicamentos()
          
        }
      })
        }
 
        typeSearchMedicamentos(){
    
          if( this.buscar.length > 3 ){
            this._medicamentoService.agragarmedicamentos( this.medicamentos )
            .subscribe(  data => {
              this.medicamentos= data['data'];
              // console.log(this.medicamentos);
              
            });

          }else if(this.medicamentos){
            this.listarMedicamentos();
            this.focusSearchMedicamentos();
          }
      
        }

        focusSearchMedicamentos(){
          const input: HTMLInputElement = document.querySelector('#busquedaMedicamento');
          input.select();
        }

}