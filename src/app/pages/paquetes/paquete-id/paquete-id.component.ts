import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaquetesService } from 'src/app/services/paquetes/paquetes.service';

@Component({
  selector: 'app-paquete-id',
  templateUrl: './paquete-id.component.html',
  styleUrls: ['./paquete-id.component.css']
})
export class PaqueteIDComponent implements OnInit {

  public id: String;
  public paquete= {
    nombrePaquete: "",
    CitasIncluidas: [],
    consultasGinecologia: 0,
    ultrasonidos: [],
    costoTotal:0,
    examenesLaboratorio: [],
    descuentos: [],
    farmacia: [],
    extras: [],
    estudios: [],
    rayosX: [],
    icon: "",
    _id: ""
  };
  constructor(
    private _paqueteService: PaquetesService,
    public router: ActivatedRoute, 
  ) { }

  ngOnInit(): void {
    this.id = this.router.snapshot.paramMap.get('id');
    
    this.obtenerPaquete( );

  }

  

  obtenerPaquete( ){
    this._paqueteService.getPaqueById( this.id )
    .subscribe( (data:any) => {
      console.log( data );
      this.paquete = data['paquete'];
    } )
    
  }

}
