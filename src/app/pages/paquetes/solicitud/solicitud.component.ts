import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { PaquetesService } from 'src/app/services/paquetes/paquetes.service';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import Dates from 'src/app/classes/dates/date.class';
import swal from 'sweetAlert';
import Tickets from '../../../classes/tickets/ticket.class'
import { PagoServiciosService } from '../../../services/pagos/pago-servicios.service';
import { eliminarStorage } from 'src/app/functions/pacienteIntegrados';
import PacienteStorage from 'src/app/classes/pacientes/pacientesStorage.class';
@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.css'],
})
export class SolicitudComponent implements OnInit {

  public maxDay : any;
  pdfFilePath = "";
  public paciente={
    apellidoMaterno: "",
    apellidoPaterno: "",
    callePaciente: "",
    consultas:"",
    contactoEmergencia1ApellidoMaterno: "",
    contactoEmergencia1ApellidoPaterno:"",
    correoPaciente: "",
    estadoPaciente:"",
    colonia:"",
    curp: "",
    edad:0,
    municipioPaciente:"",
    entidadFederativa: "",
    fechaNacimientoPaciente: "",
    fechaRegistro: "",
    genero: "",
    cpPaciente: 0,
    localidadFiscal: "",
    nombrePaciente: "",
    paisNacimiento: "",
    paisPaciente: "",
    poblacion: "",
    referenciaPaciente: "",
    telefono:"" ,
    

    
    // razon social o fiscal
    razonSocial1: "",
    razoncocial1RFC: "",
    razonSocial1Municipio:"",
    razonSocial1Calle:"",
    razonSocial1colonia:"",
    cpRazonSocial: "",
    razonSocial1Estado:"",
    
    contactoEmergencia1Nombre: "",
    contactoEmergencia1Telefono: "",
    contactoEmergencia1Parentesco:"",
    // razon social 2
    contactoEmergencia2Nombre:"",
    contactoEmergencia2Telefono:"",
    contactoEmergencia2Parentesco:"",
    correoRazonSocial1:"",
    razonsocial1Telefono:""
  };


  public infoVenta2 = {
    paciente: "",
    atendio:"",
    fecha: "",
    hora:"",
    paquete:"",
    anticipo:0,
    totalCompra:0,
    estudios:[],
    sede:"",
  }

  public infoVenta = {
    idPaciente: "",
    atendio:"",
    fecha: "",
    hora:"",
    paquete:"",
    anticipo:0,
  }

  public paquetesPacientes;

  public usuarioMaq:any;
  
  public paquetesDB= [{
      CitasIncluidas: [],
      costoTotal:0,
      examenesLaboratorio: [],
      icon: "",
      nombrePaquete: "",
      _id: ""
    }];


    
  public paqueteSelected = {
    nombrePaquete: "",
    CitasIncluidas: [],
    costoTotal:0,
    examenesLaboratorio: [],
    icon: "",
    _id: ""
  };  

  public anticipo = 0;
  public parentesco1: string;
  public parentesco2: string;
  public celular:string;
  public folio;
  
  public fecha = moment().format('l');
  public hora = moment().format('hh:mm');
  public id: string;
  membresiavali = 0;

  constructor(
    private _pagoServicios:PagoServiciosService  ,
    public router: ActivatedRoute,
    public _router2: Router,
    public paquetesService: PaquetesService,
    public _pacientesServices: PacientesService,

  ) { }

  ngOnInit(): void {
    // el id del usuario
    this.id = this.router.snapshot.paramMap.get('id');
    this.getPacieteByID( this.id );

    this.getUsuarioLocalStorage();
    this.getPaquetes();
    this.setDate();

  }

  setDate( ){
    const today = new Dates();
    this.maxDay = today.getDate();
    // console.log( this.maxDay );
    this.infoVenta.fecha = this.maxDay;
  }


  getPacieteByID( id: string  ){

    this._pacientesServices.getPacienteBtID( id )
    .subscribe( (data:any) => {
      this.paciente = data.paciente;
    } );

    }


    
    // peticion para la solicitud
    getPaquetes(){
      this.paquetesService.getPaquetesSolicitud()
      .subscribe(  (data: any ) => this.paquetesDB = data.paquetes);
    }



    
    // OBTENEMOS DEL LOCAL STORAGE EL NOMBRE DEL USUARIO QUIEN ATENDIO
    getUsuarioLocalStorage(){
      this.usuarioMaq =  JSON.parse( localStorage.getItem('usuario') );
    }



      // obtiene los paquetes para el select
      getPaquete( id: string  ){

        this.paquetesService.getPaqueById( id )
        .subscribe( (data )  => {


          
            this.paqueteSelected = data['paquete'];
              console.log(this.paqueteSelected);
              
            // el anticipo varia de acuerdo al paquete
            if(this.paqueteSelected.nombrePaquete === 'PAQUETE DE CONTROL PRENATAL') {
              this.membresiavali = 0;
              this.anticipo = 1500;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteMaternidad.pdf";

            }else if( this.paqueteSelected.nombrePaquete === 'PAQUETE MÉDICO LABORAL') {
              this.membresiavali = 0;
              this.anticipo = 175;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteMedicoLaboral.pdf";

            }else if(this.paqueteSelected.nombrePaquete === 'SERVICIOS DE LA MEMBRESÍA'){
              this.membresiavali = 1;
              this.anticipo = 580;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ANEXO_PAQUETE_DE_MATERNIDAD.pdf";

            } else if( this.paqueteSelected.nombrePaquete === 'PAQUETE NEONATAL (DE 0 A 12 MESES)' ){
              this.membresiavali = 0;
              this.anticipo = 1000;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteNeonatal.pdf";

            }else if( this.paqueteSelected.nombrePaquete ===  "PAQUETE VIDA PLENA"  ){
              this.membresiavali = 0;
              this.anticipo= 2800;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ContratoPaqueteVidaPlena.pdf";

            }else if(this.paqueteSelected.nombrePaquete === 'PAQUETE DE CONTROL PRENATAL DE ALTO RIESGO'){
              this.membresiavali = 0;
              this.anticipo = 1500;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/ANEXO_PAQUETE_DE_MATERNIDAD.pdf";

            }else if(this.paqueteSelected.nombrePaquete === 'PAQUETE PEDIÁTRICO (APARTIR DE LOS 12 MESES)'){
              this.membresiavali = 0;
              this.anticipo = 1500;
              this.pdfFilePath = "http://tlayacapan.grupomedicohorizonte.com.mx/contratos/CONTRATODEPAQUETEPEDIATRICO.pdf";
            }
            console.log(this.membresiavali);
            
        });
    }


    setInfoVenta(){

      this.infoVenta.atendio = this.usuarioMaq;
      /* this.infoVenta.paciente = this.id; */
      this.infoVenta.idPaciente = this.id;
      this.infoVenta.paquete = this.paqueteSelected._id;
      this.infoVenta.hora = this.hora;
      this.infoVenta.anticipo = this.anticipo;
      console.log( this.infoVenta );
    }

    setInfoVenta2(){

      this.infoVenta2.atendio = this.usuarioMaq;
      /* this.infoVenta.paciente = this.id; */
      this.infoVenta2.paciente = this.id;
      this.infoVenta2.paquete = this.paqueteSelected._id;
      this.infoVenta2.hora = this.hora;
      this.infoVenta2.anticipo = this.anticipo;
      console.log( this.infoVenta );
    }


    actualizarEstadoMembresia(){

      const body = {
        membresiaActiva: true
      }

      this.paquetesService.actualizarEstadoMembresia( this.id, body  )
      .subscribe( data => console.log(data)  );
    }

    agregarPaqueteAlUsuario(){

      console.log( this.infoVenta  );

      this.paquetesService.agregarPaquete( this.paqueteSelected, this.id )
      .subscribe(data => {
        
        if(  data['ok'] ){
          this._router2.navigateByUrl('/paciente/'+ this.id);
          //this.crearPdf();
          this.actualizarEstadoMembresia();
        }
        
      }); 
    } 
     
    

    crearTablaVenta(){
      this.paquetesService.setPaquete( this.infoVenta  )
      .subscribe( data => console.log(data));
    }


    validarPaqueteSeeccionado(){
      if( this.paqueteSelected._id == ""){
        swal("Selcciona un paquete",{icon:"warning"})
        /* alert('Selcciona un paquete'); */
        return false;
      }else{
        return true;
      }

    }

    crearPdf(){
      
      if( this.paqueteSelected.nombrePaquete ===  "PAQUETE DE CONTROL PRENATAL"){
                
        this._router2.navigateByUrl('/contrato-maternidad');

      }else if( this.paqueteSelected.nombrePaquete === "PAQUETE MÉDICO LABORAL"  ){
        this._router2.navigateByUrl('/contrato-medico-laboral')
      
      }else if(  this.paqueteSelected.nombrePaquete === 'PAQUETE PEDIATRICO (APARTIR DE LOS 12 MESES)' ){
        this._router2.navigateByUrl('/contrato/pediatrico');
      }
      else if( this.paqueteSelected.nombrePaquete === 'PAQUETE VIDA PLENA'  ){

        this._router2.navigateByUrl('/contrato/vida/plena');
      
      }else if( this.paqueteSelected.nombrePaquete === 'PAQUETE NEONATAL (DE 0 12 MESES)' ){ 
        this._router2.navigateByUrl('/contrato/neonatal') 
      }else if( this.paqueteSelected.nombrePaquete === 'PAQUETE DE CONTROL PRENATAL DE ALTO RIESGO'){
          this._router2.navigateByUrl('/anexo/alto/riesgo');
      }
    }

    
    enviar( f: NgForm  ){

      if(  !this.validarPaqueteSeeccionado() ){
        return
      }else {

        this.setInfoVenta();
        this.setInfoVenta2();
        this._pacientesServices.updatePaceinte(  f.value, this.id )
        .subscribe(  data => 
          {
            console.log(data);
            
            /* if(this.paqueteSelected.nombrePaquete === 'SERVICIOS DE LA MEMBRESÍA'){ */
              const tickets = new Tickets();
              const carrito={
                totalSin:this.anticipo,
                totalCon:this.anticipo,
                items:[
                  {
                    nombreEstudio: this.paqueteSelected.nombrePaquete,
                    precioSin:this.anticipo,
                    precioCon:this.anticipo,
                    name:this.paqueteSelected.nombrePaquete
                  }
                ]
              }
              this.infoVenta2.totalCompra=carrito.totalSin;
              this.infoVenta2.estudios= carrito.items;
              this.infoVenta2.sede = "TLYC01"
              /* console.log(this.infoVenta); */
              
              
              this._pagoServicios.agregarPedido( this.infoVenta2 ).subscribe( (data) => {
      
                console.log(  data )
                // console.log( data );
                if(  data['ok'] ){
                  tickets.printTicketSinMembresia(this.paciente,carrito, this.infoVenta2, this.folio);
                  // se crea la tabla de las ventas 
                  swal('Se genero la venta','Se agrego el paquete al paciente ','success');
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas
                  eliminarStorage();
                  /* const eliminarPaciente = new PacienteStorage();
                  eliminarPaciente.removePacienteStorage();  
                  this._router2.navigateByUrl('/');  */
                }
              });
          }
          );
        
        this.crearTablaVenta();
        this.agregarPaqueteAlUsuario(); 


      }

    


      // this.paquetesService.setPaquete( dataForm, this.paciente, this.paqueteSelected, this.fecha, this.anticipo, 1, this.usuarioMaq.nombre, this.usuarioMaq.nombre  )
      // .subscribe( (data:any) => {

      //     this.paquetesPacientes = data.paquete;

      //     console.log(  this.paciente );
      //     console.log(  this.paqueteSelected );
      //     console.log( dataForm );
      //     console.log(  this.paqueteSelected );

      //     this._pacientesServices.addPaquete( this.paciente, this.paqueteSelected, dataForm , this.paquetesPacientes )
      //     .subscribe( (data: any) => {
            
      //       console.log(data);
      //       if( data.ok ){

      //         // swal('Paquete agregado', 'Se agrego el paquete', 'success'); 
      //         alert('Paquete agregado');
              




              }
            

              // });
 
            
    // });
  }
  

