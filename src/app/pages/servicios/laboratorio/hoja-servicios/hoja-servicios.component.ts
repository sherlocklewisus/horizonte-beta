import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf';
import { ConsultaService } from 'src/app/services/consultas/consulta/consulta.service';
import { LaboratorioService } from 'src/app/services/consultasLab/laboratorio.service';


@Component({
  selector: 'app-hoja-servicios',
  templateUrl: './hoja-servicios.component.html',
  styleUrls: ['./hoja-servicios.component.css']
})
export class HojaServiciosComponent implements OnInit {

public id: string
public pedido  = {
  estadoPedido: "",
  estudios: [{ idEstudio:""}], 
  _id: "", 
  idPaciente:{_id :""}, 
  fecha: "", 
  hora: "",
};
public paciente  = {
  nombre: '',
  apellidoPaterno: '',
  apellidoMaterno: '',
  estadoPaciente: '',
  fechaNacimiento: '',
  telefono: '',
  edad: 0,
  genero: '',
  curp:'',
  callePaciente:'',
  cpPaciente:'',
  paisPaciente:'',
  idMedicinaPreventiva: '',
  idAntecedentesHeredoFam: '',
  idPaciente:'',
  _id:""
};





public resultado =[{
  idEstudio:{
    ELEMENTOS:[{
      elementos:"",
      referencia:""
    }
    ]
  },
  obtenidos:{
   
},
  idPaciente:[],
  idPedido:""
}]


public obtenido=[];

  constructor( private _laboratorio: LaboratorioService, public _consultaService: ConsultaService,private activatedRoute: ActivatedRoute) { 
   
  }

  ngOnInit(): void {

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.obtenerPedidoLab()
    setTimeout(() => {
      // this.obtenerResultados()
    }, 2000);
   
    
    // this.grabaridPaciente()
    // this.grabarLocalStorage();
  }

  grabarLocalStorage(){

    localStorage.setItem('estudio',JSON.stringify( this.id));
    localStorage.setItem('idEstudioU',JSON.stringify(this.pedido.estudios))
  }

  grabaridPaciente(){
localStorage.setItem('idPedido',JSON.stringify(this.pedido._id))
    localStorage.setItem('idPaciente',JSON.stringify(this.pedido.idPaciente))
  }

enviarId(){

}

  obtenerPedidoLab(){
    this._consultaService.verPedidosLaboratorio(this.id)
    .subscribe( (data) =>   {
        this.pedido = data['data']
      this.datosPaciente(data ['data']['idPaciente'])
      this.estadoActualizado(data ['data']['estudios']);
      console.log(this.pedido);
      // localStorage.getItem( JSON.stringify( this.grabaridPaciente) )
      this.grabaridPaciente()
      
    });
  }

  estadoActualizado(estudios){
    console.log(estudios);
    let contador= 0;
    estudios.forEach(element => {
      if(element.estado == 'finalizado'){
        contador += 1;
      }      
    });

    if(estudios.length == contador){
      const priori={
        prioridad:'FINALIZADO'
      }
      console.log(this.id);
      
      this._laboratorio.actualizarPrioridad(this.id, priori).subscribe((data:any)=>{
        if(data.ok){
          console.log(data);
          
        }
      })
      
    }else{
      console.log('no entro');
      
    }
  }
 
  datosPaciente(paciente ){
 this.paciente=paciente
 console.log(this.paciente)
  }



//   obtenerResultados(){
//     // console.log(this.paciente.idPaciente)
//     this._laboratorio.obtenerLaboratorioEvolucion(this.pedido.idPaciente._id)
//     .subscribe(( data ) =>{ this.setEstudio( data ['data'])
//    console.log(data)
//    })
    
//   }
//  setEstudio(estudio){
//  this.resultado= estudio
 
//  if( !this.resultado[0]  ){
//    return;
//  }
//  console.log(Object.keys(this.resultado[0].obtenidos));
//  for(let item in this.resultado[0].obtenidos[0]){
   
//    console.log();
   
//    this.obtenido.push(this.resultado[0].obtenidos[0][item])
   
//  }
//  console.log(this.obtenido);
 
 
//  console.log(this.resultado)
//  }


}
