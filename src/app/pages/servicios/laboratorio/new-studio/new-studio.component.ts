import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LaboratorioService } from 'src/app/services/consultasLab/laboratorio.service';
import swal from 'sweetalert/dist/sweetalert.min.js';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-studio',
  templateUrl: './new-studio.component.html',
  styleUrls: ['./new-studio.component.css']
})
export class NewStudioComponent implements OnInit {
  forma: FormGroup; 

public id;
public elementos = {
  ELEMENTOS: [],
ESTUDIO: "",
PRECIO_MEMBRESIA: "",
PRECIO_MEMBRESIA_HOSPITALIZACION: "",
PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA: "",
PRECIO_MEMBRESIA_URGENCIA: "",
PRECIO_PUBLICO: "",
PRECIO_PUBLICO_HOSPITALIZACION: "",
PRECIO_PUBLICO_HOSPITALIZACIO_URGENCIA: "",
PRECIO_PUBLICO_URGENCIA: "",
name: "",
}

  constructor(private _routers: Router, private _laboratorio:  LaboratorioService , private fb: FormBuilder, private _route: ActivatedRoute, private consumoIntegrado : IntegradosService) { 
    console.log(this.crearFormulario());
    /* console.log(this.crearValoresRef()); */
    
    this.crearFormulario();
    /* this.crearValoresRef(); */
  }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerIdEstudio();
  }

 
 
   
  get grupos(){
    return this.forma.get('grupos') as FormArray;
  }

  get referencia(){
    return this.forma.get('referencia') as FormArray;
  }
  // get referencia(){
  //   return this.forma.get('referencia') as FormArray;
  // }
  // get tipo(){
  //   return this.forma.get('tipo') as FormArray;
  // }

  crearFormulario(){
    const valores =  this.fb.group({
      referencia:""
    });

 this.forma = this.fb.group({
  grupos:this.fb.array([]),
  // tipo:this.fb.array([[]])
 });

  this.agregarCampo()
  //this.agregarRefer2();
  //this.grupos.controls['0'].controls['referencia'].push(valores)


  }
  
  //fin metodo crearformulario()

  /* crearValoresRef(){
    this.formas=this.fb.group({
      referencias:this.fb.array([]),
    });
    this.agregarRefer()
  } */

  agregarCampo(){

 const elemento =  this.fb.group({
      elementos:"",
      referencia:this.fb.array([this.fb.group({
        referencia:''
      })]),
      tipo:"" 
    });
    this.grupos.push(elemento)
    // console.log(this.grupos)
  }

  /* agregarRefer2(){
    const valores =  this.fb.group({
      referencia:""
    });
    console.log(this.forma)
    this.grupos.controls['0'].controls['referencia'].push(valores)
  } */

  agregarRefer(index){
    const valores =  this.fb.group({
      referencia:""
    });
    // console.log(this.forma)
    this.grupos.controls[''+index].controls['referencia'].push(valores)
  }

  borrarCampo(i :  number){
    this.grupos.removeAt(i);
  }

    vista(){
      console.log(this.forma)
    }

    obtenerIdEstudio(){
       this.consumoIntegrado.getServicioById(this.id)
       .subscribe((data:any) => {
        console.log(data); 
        this.elementos=data ['data']
      
      })
    }


    enviarSave(){
      console.log(this.forma.value)
      this.consumoIntegrado.actualizarElemtos(this.id,this.forma.value)
      .subscribe ((data:any)=> {this.elementos =data ['data']
      console.log(data)
    }) 

    swal ( 
      "DATOS ENVIADOS",
       "Información enviada correctamente", 
      "success",{ button:"Entendido"}
      
     )
     this._routers.navigate(['/historico-estudio'])

    }

}
