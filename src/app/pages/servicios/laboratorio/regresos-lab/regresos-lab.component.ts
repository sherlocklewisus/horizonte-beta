import { Component, OnInit } from '@angular/core';
import { ConsultaService } from 'src/app/services/consultas/consulta/consulta.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import jsPDF from 'jspdf';
@Component({
  selector: 'app-regresos-lab',
  templateUrl: './regresos-lab.component.html',
  styleUrls: ['./regresos-lab.component.css']
})
export class RegresosLabComponent implements OnInit {

  public pagina = 0;
  public consultas:any=[];
  public totalAmbulancia: string;
  noSe: any;

  constructor(  public _consultaService: ConsultaService,public _consultaPaciente: PacientesService) { }

  ngOnInit(): void {

    this.obtenerCosnultaLab();

  }


  obtenerCosnultaLab(){
    this._consultaService.verListaLaboratorio()
    .subscribe( (data) =>   {
        this.consultas = data['data'].reverse();
        // console.log(data);
        this.totalAmbulancia = this.consultas.results;  
    });
  }

  imp(){
    let values: any;
    values = this.noSe.map((elemento) => Object.values(elemento));
    
    console.log(values);
    
    const doc:any = new jsPDF();
    doc.text(12, 9, "BITÁCORA DE RAYOS X");
    doc.autoTable({
      head: [['#', 'Fecha', 'Nombre', 'Edad', 'Sexo', 'Sede', 'Estudio']],
      body: values
    })
    doc.save('Bitácora_De_Rayos_X.pdf')
  }
 



}
