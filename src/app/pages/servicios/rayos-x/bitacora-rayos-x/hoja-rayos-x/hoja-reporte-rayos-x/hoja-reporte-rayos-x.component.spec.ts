import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaReporteRayosXComponent } from './hoja-reporte-rayos-x.component';

describe('HojaReporteRayosXComponent', () => {
  let component: HojaReporteRayosXComponent;
  let fixture: ComponentFixture<HojaReporteRayosXComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HojaReporteRayosXComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaReporteRayosXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
