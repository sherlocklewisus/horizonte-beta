import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { XRAYService } from 'src/app/services/Rayos-X/xray.service';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';
import swal from 'sweetalert/dist/sweetalert.min.js';

@Component({
  selector: 'app-hoja-reporte-rayos-x',
  templateUrl: './hoja-reporte-rayos-x.component.html',
  styleUrls: ['./hoja-reporte-rayos-x.component.css']
})
export class HojaReporteRayosXComponent implements OnInit {

  public id_: string;
  // public fechas;
  public paciente  = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    estadoPaciente: '',
    fechaNacimiento: '',
    telefono: '',
    edad: 0,
    genero: '',
    curp:'',
    callePaciente:'',
    cpPaciente:'',
    paisPaciente:'',
    idMedicinaPreventiva: '',
    idAntecedentesHeredoFam: '',
    idPaciente:'',
    _id:""
  };


  public xray ={
    ESTUDIO:"",
    ELEMENTOS:[{machote:""}],
    _id:"",
machote:""
  }

  public usgObtenido={
    usuario:"",
    idPaciente:"",
    idPedido:"",
    idEstudio:"",
    machoteEdit:"",
    fecha:"",
    observaciones:"",
    diagnostico:"",
    img: []
  }
  public imageUrl;
  public estudio;
  public img;
  public machote:""
  public macho:""
  public diagnostico:""
  public holiwis;
  public obs:"";
  public fechas:""
public IDxray:""
  public userStorage:{
    nombre:""
  }
  public navegateIdXray :""

  constructor(
    private activatedRoute: ActivatedRoute,
    private _integrateServicios : IntegradosService,
    private _xrayService : XRAYService,
    private _routers : Router

  ) { }

  ngOnInit(): void {
    this.id_ = this.activatedRoute.snapshot.paramMap.get('id');
    this.obtenerPedidosXray();
    this.obtenerDatosLocalStorage();
  }

  // ARCHIVOS DROPZONE
  files: File[] = [];

  onSelect(event){
    console.log(event);
    this.files.push(...event.addedFiles);
    // console.log(this.files);
  }

  onRemove(event){
    // console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
    
  }
  
 // METODOS OBTER PEDIDO
 setPeidoXray(pedido){
  this.xray =pedido;
  //  console.log(pedido);
 // console.log(this.ultrasonidos.ELEMENTOS[0]);
  //  this.holiwis = this.xray.ELEMENTOS[0].machote;
   if (this.xray.ELEMENTOS[0].machote == undefined)
 {
  //  console.log("no hay nada");  
}else {this.holiwis = this.xray.ELEMENTOS[0].machote;} 
   
  }
    obtenerPedidosXray(){
     this._integrateServicios.buscarServicio (this.id_)
     .subscribe((data) => this.setPeidoXray(data ['data']))
    }
 
   obtenerDatosLocalStorage(){
     this.estudio=JSON.parse(localStorage.getItem('ultrasonido'))
     this.xray=JSON.parse(localStorage.getItem('idPedidoXray'))
     this.IDxray =JSON.parse(localStorage.getItem('idPedidoXray'))
      this.paciente = JSON.parse(localStorage.getItem('idPaciente'));
      this.fechas = JSON.parse(localStorage.getItem('fechaXray'))
      this.userStorage = JSON.parse(localStorage.getItem('usuario'))
      this.navegateIdXray = JSON.parse(localStorage.getItem('idPedidoXray'))
     //  console.log(this.fechas);
      
   }

   // enviar reporte a hc y recepcion
    
  enviarReporteXray(){

// console.log(this.holiwis);

    // if((this.diagnostico === undefined && this.obs === undefined && this.holiwis === undefined) || (this.diagnostico === undefined && this.holiwis != undefined))
    if((this.holiwis == undefined && this.diagnostico == undefined && this.obs == undefined) || (this.obs != undefined && this.diagnostico != undefined && this.holiwis == undefined) || (this.holiwis != undefined && this.diagnostico != undefined && this.obs == undefined) || (this.obs != undefined && this.diagnostico != undefined && this.holiwis == "")  || (this.obs == undefined && this.diagnostico == undefined && this.holiwis != undefined) || (this.obs == "" && this.diagnostico == "" && this.holiwis != undefined))
    // if((this.obs!= "" && this.diagnostico !="" && this.holiwis != "") || (this.obs!= undefined && this.diagnostico !=undefined && this.holiwis != undefined) )
    {
      swal ( 
              "DATOS INCOMPLETOS",
               "INFORMACION INCOMPLETA, POR FAVOR LLENAR INTERPRETACIÓN (IR A EDITAR EL MACHOTE), DIAGNOSTICO Y OBSERVACIONES  ", 
              "warning",{ button:"OK"}
              
             )
     
    }else {


      // swal ( 
      //   "DATOS COMPLETOS",
      //    "INFORMACION ICOMPLETA", 
      //   "success",{ button:"OK"}
        
      //  )

    this.usgObtenido.idPaciente = this.paciente._id
 this.usgObtenido.idEstudio =   this.id_
 this.usgObtenido.idPedido =   this.IDxray
 this.usgObtenido.diagnostico= this.diagnostico
 this.usgObtenido.machoteEdit = this.holiwis
this.usgObtenido.observaciones = this.obs
this.usgObtenido.fecha = this.fechas
this.usgObtenido.usuario = this.userStorage.nombre

 
    

const formData = new FormData();
for (var i = 0; i < this.files.length; i++) { 
    formData.append("imageUrl", this.files[i]);
}
formData.append('idPaciente', this.usgObtenido.idPaciente)
formData.append('idEstudio', this.usgObtenido.idEstudio)
formData.append('idPedido', this.usgObtenido.idPedido)
formData.append('diagnostico', this.usgObtenido.diagnostico)
formData.append('machoteEdit', this.usgObtenido.machoteEdit)
formData.append('observaciones', this.usgObtenido.observaciones)
formData.append('fecha', this.usgObtenido.fecha)
formData.append('usuario',this.usgObtenido.usuario)
//console.log(this.usgObtenido.fecha);



 this._xrayService.enviarXrayRegreso(formData)

 .subscribe((data :any)=>{
   if(data.ok){
    // console.log(data)

    swal ( 
      "DATOS ENVIADOS",
       "Información enviada correctamente", 
      "success",{ button:"Entendido"}
      
     )
   }
  

   this._routers.navigate(['/hoja/rayos/x/'+ this.navegateIdXray])
})
    
} // final else
 
  }
}
