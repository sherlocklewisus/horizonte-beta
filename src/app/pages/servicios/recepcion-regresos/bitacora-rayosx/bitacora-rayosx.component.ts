import { Component, OnInit } from '@angular/core';
import { XRAYService } from 'src/app/services/Rayos-X/xray.service';

@Component({
  selector: 'app-bitacora-rayosx',
  templateUrl: './bitacora-rayosx.component.html',
  styleUrls: ['./bitacora-rayosx.component.css']
})
export class BitacoraRayosxComponent implements OnInit {

  public consultas: any =[]
  public pagina = 0;
  public totalAmbulancia: string;
  constructor(  private _xrayService :XRAYService) { }

  ngOnInit(): void {
    this.obtenerCosnultaXray();
  }

  obtenerCosnultaXray(){
    this._xrayService.getXray()
    .subscribe( (data) =>   {
      
        this.consultas = data['data'].reverse();
        //console.log(this.consultas);
        
    });
  }

}
