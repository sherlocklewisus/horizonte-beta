import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteUltrasonidoComponent } from './reporte-ultrasonido.component';

describe('ReporteUltrasonidoComponent', () => {
  let component: ReporteUltrasonidoComponent;
  let fixture: ComponentFixture<ReporteUltrasonidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteUltrasonidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteUltrasonidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
