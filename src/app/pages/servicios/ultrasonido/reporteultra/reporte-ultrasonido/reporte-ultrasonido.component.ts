import { Component, OnInit } from '@angular/core';
import { USGService } from 'src/app/services/usg/usg.service';
import { ActivatedRoute,Router } from '@angular/router';
import { IntegradosService } from 'src/app/services/servicios/integrados.service';
import swal from 'sweetalert/dist/sweetalert.min.js';
import { NgxDropzoneComponent } from 'ngx-dropzone';


@Component({
  selector: 'app-reporte-ultrasonido',
  templateUrl: './reporte-ultrasonido.component.html',
  styleUrls: ['./reporte-ultrasonido.component.css']
})
export class ReporteUltrasonidoComponent implements OnInit {



  public id_: string;
  // public fechas;
  public paciente  = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    estadoPaciente: '',
    fechaNacimiento: '',
    telefono: '',
    edad: 0,
    genero: '',
    curp:'',
    callePaciente:'',
    cpPaciente:'',
    paisPaciente:'',
    idMedicinaPreventiva: '',
    idAntecedentesHeredoFam: '',
    idPaciente:'',
    _id:""
  };


  public ultrasonidos ={
    ESTUDIO:"",
    ELEMENTOS:[{machote:""}],
    _id:"",
    machote:""
  }

  public usgObtenido={
    idPaciente:"",
    idPedido:"",
    idEstudio:"",
    machoteEdit:"",
    fecha:"",
    observaciones:"",
    diagnostico:"",
    img: [],
    usuario:""
  }
  public imageUrl;
  public estudio;
  public img;
  public machote:""
  public macho:""
  public diagnostico:"";
  public holiwis:any;
  public obs:"";
  public fechas:""
  public userStorage:{
    nombre:""
  }


  public activar = false;
  // public imageUrl = [];
  constructor(

    private activatedRoute: ActivatedRoute,
    private _usgService: USGService,
    private _integrateServicios : IntegradosService,
    private _routers : Router
  ) { }

  ngOnInit(): void {

    this.id_ = this.activatedRoute.snapshot.paramMap.get('id');
   
  this.paciente = JSON.parse(localStorage.getItem('idPaciente'))

    // this.ultrasonidos = this._usgService.getUltrasonidos();
    // console.log(this.ultrasonidos);

    this.obtenerPedidosUltra();
    this.obtenerDatosLocalStorage()
  }

  //Archivos del DROPZONE
  files: File[] = [];

  onSelect(event){
    // console.log(event);
    this.files.push(...event.addedFiles);
    // console.log(this.files);
    
    // const formData = new FormData();
    // this.files.forEach(img => {
    //   console.log(img);

    //     formData.append("name", img, img.name)
    // });

    //     console.log(formData);
        
    
    //     for (var i = 0; i < this.files.length; i++) { 
    //       formData.append("name", this.files[i],this.files[i].name);
    //       console.log(formData);
          
    //     }
  }

  onRemove(event){
    // console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
    
  }
  //Fin Archivos DROPZONE


 setPeidoUltra(pedido){
 this.ultrasonidos =pedido;

 if (this.ultrasonidos.ELEMENTOS[0].machote == undefined)
 {
  //  console.log("no hay nada");  
}else {this.holiwis = this.ultrasonidos.ELEMENTOS[0].machote;} 
  // console.log(pedido);
// console.log(this.ultrasonidos.ELEMENTOS[0]);
  
  
 }
   obtenerPedidosUltra(){
    this._integrateServicios.buscarServicio (this.id_)
    .subscribe((data) => this.setPeidoUltra(data ['data']))
   }

  obtenerDatosLocalStorage(){
    this.estudio=JSON.parse(localStorage.getItem('ultrasonido'))
    this.ultrasonidos=JSON.parse(localStorage.getItem('idPedidoUltra'))
     this.paciente = JSON.parse(localStorage.getItem('idPaciente'));
     this.fechas = JSON.parse(localStorage.getItem('fechaUsg'))
     this.userStorage = JSON.parse(localStorage.getItem('usuario'))
    //  console.log(this.userStorage.nombre);
     
  }
  
  enviarReporteUsg(){
    // console.log(this.holiwis,this.obs,this.diagnostico);
    
    // if((this.diagnostico === undefined && this.obs === undefined && this.holiwis === undefined) || (this.diagnostico === undefined && this.holiwis != undefined))
    if((this.holiwis == undefined && this.diagnostico == undefined && this.obs == undefined) || (this.obs != undefined && this.diagnostico != undefined && this.holiwis == undefined) || (this.holiwis != undefined && this.diagnostico != undefined && this.obs == undefined) || (this.obs != undefined && this.diagnostico != undefined && this.holiwis == "")  || (this.obs == undefined && this.diagnostico == undefined && this.holiwis != undefined) || (this.obs == "" && this.diagnostico == "" && this.holiwis != undefined)) 
    {
      swal ( 
              "DATOS INCOMPLETOS",
               "INFORMACION INCOMPLETA, POR FAVOR LLENAR INTERPRETACIÓN (IR A EDITAR EL MACHOTE), DIAGNOSTICO Y OBSERVACIONES  ", 
              "warning",{ button:"OK"}
              
             )
     
    }else {
   

    
    this.usgObtenido.idPaciente = this.paciente._id
 this.usgObtenido.idEstudio = this.ultrasonidos._id
 this.usgObtenido.idPedido = this.estudio
 this.usgObtenido.diagnostico= this.diagnostico
 this.usgObtenido.machoteEdit = this.holiwis
this.usgObtenido.observaciones = this.obs
this.usgObtenido.fecha = this.fechas
this.usgObtenido.usuario = this.userStorage.nombre

 



const formData = new FormData();
for (var i = 0; i < this.files.length; i++) { 
    formData.append("imageUrl", this.files[i]);
}
formData.append('idPaciente', this.usgObtenido.idPaciente)
formData.append('idEstudio', this.usgObtenido.idEstudio)
formData.append('idPedido', this.usgObtenido.idPedido)
formData.append('diagnostico', this.usgObtenido.diagnostico)
formData.append('machoteEdit', this.usgObtenido.machoteEdit)
formData.append('observaciones', this.usgObtenido.observaciones)
formData.append('fecha', this.usgObtenido.fecha)
formData.append('usuario',this.usgObtenido.usuario)



 this._usgService.enviarUsgRegreso(formData)

 .subscribe((data :any)=>{
   if(data.ok){
    // console.log(data)

    swal ( 
      "DATOS ENVIADOS",
       "Información enviada correctamente", 
      "success",{ button:"Entendido"}
      
     )
   }
  
   

   this._routers.navigate(['/hoja-ultrasonido/'+ this.estudio])
})

} 

 
  }



  


}
