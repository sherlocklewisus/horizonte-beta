import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform( value: any, arg: String ): unknown {

  const productsValue = value.filter( producto => producto.nombre === arg );

  console.log(productsValue );
    return productsValue;

  }

}
