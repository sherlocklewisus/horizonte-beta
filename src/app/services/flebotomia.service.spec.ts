import { TestBed } from '@angular/core/testing';

import { FlebotomiaService } from './flebotomia.service';

describe('FlebotomiaService', () => {
  let service: FlebotomiaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlebotomiaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
