import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationServicesService {

  constructor(public http: HttpClient) { }

  getCountries() {
    const URLCountries = "https://restcountries.eu/rest/v2/all";
    return this.http.get(URLCountries)
      .pipe(map((countries) => {
        return countries;
      }))
  }

  getLocalidades(codigo: number) {

    const URLCODIGOPOSTAL = "https://api.copomex.com/query/info_cp/";
    // obtenemos las localidades por el Codigo postal
    let tipo = '?type=simplified';
    //TOKEN
    const token = "&token=8ae40e61-5616-438d-b6aa-31d7b9bc32df";

    let url = `${URLCODIGOPOSTAL}${codigo}${tipo}${token}`;

    return this.http.get(url)
      .pipe(map(res => res));

  }

  getEstado() {

    const url = "https://api.copomex.com/query/get_estados";
    //TOKEN
    const token = "?token=78e3eb6c-11f6-445f-ab91-8c22c89c6464";
    return this.http.get(url +  token);
  }


  getMunicipios(municipio) {
    const url = "https://api.copomex.com/query/get_municipio_por_estado/" + municipio;
    //TOKEN
    const token = "?token=78e3eb6c-11f6-445f-ab91-8c22c89c6464";
    return this.http.get(url + token);

  }

}
