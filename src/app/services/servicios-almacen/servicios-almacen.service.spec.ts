import { TestBed } from '@angular/core/testing';

import { ServiciosAlmacenService } from './servicios-almacen.service';

describe('ServiciosAlmacenService', () => {
  let service: ServiciosAlmacenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiciosAlmacenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
