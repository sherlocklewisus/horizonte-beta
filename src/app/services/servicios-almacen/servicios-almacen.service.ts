import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ServiciosAlmacenService {

  private url = 'http://localhost:3200'

  constructor( private http: HttpClient) { }

  obtenerServicios(){
    return this.http.get( `${ this.url }/ver/servicios/censur` );
  }

}


